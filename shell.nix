let
  sources = import ./nix/sources.nix {};
  nixpkgs = import sources.nixpkgs {};
  tango-controls = import ./tango-controls {};
  mkPoetryShell = import ./nix/poetry2nix-shell.nix;
  pyclockmakerArgs = mkPoetryShell {poetry2nixApplication = import ./pyclockmaker {};};
in
  nixpkgs.mkShell (pyclockmakerArgs
    // {
      packages = pyclockmakerArgs.packages ++ (builtins.attrValues tango-controls) ++ (with nixpkgs; [qt5.qtbase alejandra niv]);
    })
