#!/usr/bin/env bash

set -e

if [ -z "$1$2" ]; then
  printf "Usage: $0 <DATABASENAME> <DIRECTORY_TO_CREATE_AND_DUMP_FILES>\n"
  exit 1
fi

# For some reason, 
# on some mariadb installs the directory mysql dumps to must be writable by 
# the mysql user.
mkdir -m a=rwx "$2"
for tablename in $(mysql "$1" -sN -e "SHOW TABLES;"); do 
  printf "Dumping $tablename\n"; 
  mysqldump -T "$2" --fields-terminated-by="," "$1" "$tablename"
done

printf "Restoring ownership and permissions of created files.\n"
sudo chown $USER:$(id -gn) -R "$2" 
chmod "-$(umask)" -R "$2" 

printf "Renaming .txt files to .csv\n"

for file in "$2"/*.txt; do
  [ -f "$file" ] || continue

  mv "$file" "${file%.txt}.csv"
done

