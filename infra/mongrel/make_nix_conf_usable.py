"""Add nix-command and flakes as well as clocktower's nix store key."""

from collections.abc import Iterable
from dataclasses import dataclass
from os import getlogin
import sys
from typing import NewType, Self

CLOCKTOWER_PUB_KEY = "clocktower.service.nixcache.nix-store-private-key:LTT4DuwIzdOeN8SCZGvPRUG2NqXAtGd6hW9w/4YmJqc="
# When we explicitly set trusted-public-keys, we must specify the default as well.
NIXCACHE_PUB_KEY = "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="


@dataclass
class Assignment:
    left_hand_side: str
    right_hand_side: list[str]


Comment = NewType("Comment", str)
"""Includes leading #"""


@dataclass
class NixConfLine:
    assignment: Assignment | None
    comment: Comment | None

    @classmethod
    def parse(cls, line: str) -> Self:
        pass
        if (comment_start := line.find("#")) >= 0:
            comment = Comment(line[comment_start:].strip())
            line = line[:comment_start]
        else:
            comment = None

        if "=" in line:
            left, right = line.split("=", 1)
            assignment = Assignment(
                left_hand_side=left.strip(), right_hand_side=right.split()
            )
        else:
            assignment = None

        return cls(assignment=assignment, comment=comment)

    def unparse(self) -> str:
        line = ""
        if self.assignment is not None:
            line += self.assignment.left_hand_side
            line += " = "
            line += " ".join(self.assignment.right_hand_side)

        if self.comment is not None:
            if line:
                line += " "
            line += self.comment

        return line



def add_values(
    lines: Iterable[NixConfLine], left_hand_side: str, extra_vals: list[str]
) -> Iterable[NixConfLine]:
    inserted = False
    for line in lines:
        if (line.assignment is None) or (
            line.assignment.left_hand_side != left_hand_side
        ):
            yield line
            continue


        inserted = True

        new_assignment = Assignment(
                    left_hand_side=left_hand_side,
                    right_hand_side=list(line.assignment.right_hand_side),
                    )


        for extra_val in extra_vals:
            if extra_val not in new_assignment.right_hand_side:
                new_assignment.right_hand_side.append(extra_val)

        yield NixConfLine(assignment=new_assignment, comment=line.comment)

    if not inserted:
        yield NixConfLine(
            assignment=Assignment(
                left_hand_side=left_hand_side, right_hand_side=extra_vals
            ),
            comment=None,
        )


def main():
    lines = map(NixConfLine.parse, sys.stdin.readlines())
    lines = add_values(lines, "trusted-public-keys", [NIXCACHE_PUB_KEY, CLOCKTOWER_PUB_KEY])
    lines = add_values(lines, "experimental-features", ["nix-command", "flakes"])
    lines = add_values(lines, "trusted-users", [getlogin()])
    lines_str = map(NixConfLine.unparse, lines)
    print('\n'.join(lines_str).lstrip())


if __name__ == "__main__":
    main()
