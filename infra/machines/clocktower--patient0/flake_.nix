# So this was a failed experiment to set up a client machine
# such that the installer automatically installed the full machine configuration.
# But I'm a bozo and ended up creating a machine
# where the installation ISO has the config,
# rather than the installed machine...
# The command used was
# nix-build ".#packages.x86_64-linux.myinstall" --impure -o /tmp/result
# and result found in /tmp/result/iso/nixos-*.iso
{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nixos-generators = {
      url = "github:nix-community/nixos-generators";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };
  outputs = {
    self,
    nixpkgs,
    nixos-generators,
    ...
  }: {
    packages.x86_64-linux = {
      myinstall = nixos-generators.nixosGenerate {
        system = "x86_64-linux";
        # you can include your own nixos configuration here, i.e.
        modules = [
          ./configuration.nix
        ];
        format = "myFormat";

        # optional arguments:
        # explicit nixpkgs and lib:
        # pkgs = nixpkgs.legacyPackages.x86_64-linux;
        # lib = nixpkgs.legacyPackages.x86_64-linux.lib;
        # additional arguments to pass to modules:
        # specialArgs = { myExtraArg = "foobar"; };

        # you can also define your own custom formats
        # customFormats = { "myFormat" = <myFormatModule>; ... };
        customFormats = {
          myFormat = (
            {
              config,
              lib,
              modulesPath,
              ...
            }: {
              imports = [
                "${toString modulesPath}/installer/cd-dvd/installation-cd-graphical-calamares-gnome.nix"
              ];

              # override installation-cd-base and enable wpa and sshd start at boot
              systemd.services.sshd.wantedBy = lib.mkForce ["multi-user.target"];
              virtualisation.hypervGuest.enable = true;

              formatAttr = "isoImage";
              fileExtension = ".iso";
            }
          );
        };
        # format = "myFormat";
      };
    };
  };
}
