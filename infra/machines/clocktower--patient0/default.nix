{
  pkgs,
  ...
}: let
  tango-controls = import ../../../tango-controls;

  users = {
    mutableUsers = false;
    users = {
      # clockmakeruser = {
      #   description = "Clockmaker user, controller of all clockmaker-related daemons";
      #   extraGroups = ["dialout"];
      #   # TODO: Instead derive password from a passwd file.
      #   # used `mkpasswd` command. Below the password is just "clockmaker"
      #   initialHashedPassword = "$y$j9T$LXbtfgvxp62M/aFHZ5tF./$i03twvGrhw68XTNzTUD2cddzXg//ZmzmZVUdg92zed3";
      #   isNormalUser = true;
      #   openssh.authorizedKeys.keys = [
      #     "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop"
      #   ];
      # };
      clockmaker = {
        description = "Clockmaker dev account, for on-site debuggery.";
        extraGroups = ["dialout" "wheel"];
        isNormalUser = true;
        openssh.authorizedKeys.keys = [
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop"
        ];
        packages = with pkgs; [vscode];
      };
    };
  };
in {
  imports = [
    ./hardware-configuration.nix
    ../../modules/clockmaker-rescuers
    #../../modules/tango-controls/TangoDatabase
    #../../modules/tango-controls/hdbpp
    ../../modules/clockmaker-cinnamon
    ../../modules/clockmaker-secrets
    ../../modules/clockmaker-base
  ];

  # BELOW FOR GPT
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;
  # Use the GRUB 2 boot loader (MBR)
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only

  clockmaker = {
    clientName = "clocktower";
    machineName = "patient0";
    secrets = {
      host.enableSecrets = "none";
    };
  };

  # networking.hostName = "clockmaker-patient0"; # Set by clockmaker-base module.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  # networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # # Configure keymap in X11
  # services.xserver.xkb = {
  #   layout = "us";
  #   variant = "";
  # };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  inherit users;

  nix.settings.experimental-features = [
    "flakes"
    "nix-command"
  ];

  # Allow unfree packages
  # nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages =
    [
      pkgs.htop
      pkgs.ripgrep
    ]
    ++ (builtins.attrValues tango-controls)
    ;

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  services = {
    #hdbpp.enable = true;
    #TangoDatabase.enable = true;
    mysql = {
      enable = true;
      package = pkgs.mariadb;
    };
    openssh = {
      enable = true;
      ports = [22];
      settings = {
        PasswordAuthentication = false;
      };
    };
    tailscale = {
      enable = true;
      #authKeyFile = /tmp/config.age.secrets."clockmaker..tailscale-auth-key.spoke".path;
    };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
