# TODO (daniel): Write tests, perhaps running this in a vm,
# to ensure that a non-empty tango db is created.
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  ...
}: let
  users = {
    mutableUsers = false;
  };
  getUserName = (
    name:
      assert builtins.hasAttr name users.users; users.users."${name}".name or name
  );
in {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ../../rescuers/configuration.nix
    ../../bitbucket-runner-linux-shell
  ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "clockmaker-nixnucdan"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/New_York";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # Configure keymap in X11
  services.xserver.xkb = {
    layout = "us";
    variant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  inherit users;

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs;
    [
      htop
      ripgrep
    ]
    ++ (builtins.attrValues tango-controls);

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  services = {
    hydra = {
      enable = true;
      notificationSender = "hydra@localhost";
      useSubstitutes = true;
    };

    openssh = {
      enable = true;
      ports = [22];
      settings = {
        PasswordAuthentication = false;
      };
    };
    tailscale = {
      enable = true;
    };

    bitbucket-runner-linux-shell = {
      # Enable Bitbucket Runner Linux Shell Service
      enable = false;

      user = "bitbucket-runner-linux-shell";
      group = "bitbucket-runner-linux-shell";

      # Flags for the Bitbucket runner
      # TODO: Fill these in securely as secrets...
      flags = {
        accountUuid = "{00000000-0000-0000-0000-000000000000}";
        repositoryUuid = "{00000000-0000-0000-0000-000000000000}";
        runnerUuid = "{00000000-0000-0000-0000-000000000000}";
        OAuthClientId = "this-was-a-triumph";
        OAuthClientSecret = "im_making_a_note_here___huge_success";
        workingDirectory = "/tmp";
        runtime = "linux-shell";
      };
    };
    services.nginx = {
      enable = true;
      virtualHosts = {
        "clockmaker-nixnucdan.tailc9ae5.ts.net" = {
          addSSL = true;
          # got these with `tailscale cert`.
          # Be sure to chown them to nginx:nginx
          sslCertificate = "/var/ssl/host.crt";
          sslCertificateKey = "/var/ssl/host.key";
          default = true;
          locations = {
            "/" = {
              proxyPass = "http://unix:/run/nix-serve/nix-serve.sock";
              # root = "/var/www/default";
              # tryFiles = "$uri $uri/index.html $uri.html =404";
              # index = "/index.html";
            };
          };
        };
      };
    };
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "24.05"; # Did you read the comment?
}
