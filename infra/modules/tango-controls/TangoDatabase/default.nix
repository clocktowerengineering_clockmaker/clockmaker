# TODO (daniel): Write tests, perhaps running this in a vm,
# to ensure that a non-empty tango db is created.
# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  ...
}: let
  tango-controls = import ../../../../tango-controls;
  create_tango_db = let
    workingDir = "${tango-controls.TangoDatabase}/share/tango/db";
    workingDirEsc = (lib.strings.escape ["/"]) (lib.strings.escapeRegex workingDir);
  in
    pkgs.runCommand
    "create_tango_db.sql"
    {}
    ''
      # Default db creation script that x builds
      # has "CREATE DATABASE tango;"
      # lines, but services.mysql.initialDatabases.schema
      # is run after the db is already created, so we comment those lines out.
      sed -E "s/CREATE DATABASE.*;/-- \\0/" "${workingDir}/create_db.sql" > $out

      # Make "source" directives reference by absolute path.
      sed -i -E "s/source (.*\\.sql)$/source ${workingDirEsc}\/\\1/" $out
    '';
in {
  options = let
    inherit (lib) mkOption types;
  in {
    services.TangoDatabase = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Enable the Bitbucket runner service";
      };

      users = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Users who should have access to Tango Database";
      };
    };
  };

  config.services = let
    inherit (lib) mkIf;
  in {
    mysql = mkIf config.services.TangoDatabase.enable {
      initialDatabases = [
        {
          name = "tango";
          # stringified to tell Nix it's a dependency.
          schema = "${create_tango_db}";
        }
      ];
      ensureDatabases = ["tango"];
      ensureUsers =
        builtins.map
        (
          name: {
            inherit name;
            ensurePermissions = {
              "tango.*" = "ALL PRIVILEGES";
            };
          }
        )
        config.services.TangoDatabase.users;
    };
  };
}
