{
  config,
  lib,
  ...
}: let
  tango-controls = import ../../../../tango-controls;
in {
  options = let
    inherit (lib) mkOption types;
  in {
    services.hdbpp = {
      enable = mkOption {
        type = types.bool;
        default = false;
        description = "Require the hdb++ Tango server is built on startup.";
      };

      users = mkOption {
        type = types.listOf types.str;
        default = [];
        description = "Users who should have full access to hdbpp.";
      };
    };
  };
  config.services = let
    inherit (lib) mkIf;
  in {
    mysql = mkIf config.services.hdbpp.enable {
      initialDatabases = [
        {
          name = "hdbpp";

          schema = "${tango-controls.libhdbpp-mysql}/share/libhdb++mysql/create_hdb++_mysql.sql";
        }
      ];
      ensureDatabases = ["hdbpp"];
      ensureUsers =
        builtins.map
        (
          name: {
            inherit name;
            ensurePermissions = {
              "hdbpp.*" = "ALL PRIVILEGES";
            };
          }
        )
        config.services.hdbpp.users;
    };
  };
}
