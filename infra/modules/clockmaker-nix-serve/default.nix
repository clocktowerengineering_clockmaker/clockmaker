{
  config,
  pkgs,
  lib,
  ...
}: let
  inherit (lib) mkDefault mkOption optionalAttrs optionalString setAttrsByPath types mkIf;
  inherit (lib.options) mkEnableOption;
  inherit (lib.attrsets) nameValuePair;

  serviceName = "clockmaker-nix-serve";
  serviceConfig = builtins.getAttr serviceName config.services;
  runDir = "/run/${serviceName}";
  sockPath = "${runDir}/clockmaker-nix-serve.sock";
in {
  options.services.${serviceName} = {
    description = "nix-serve using sockets instead";
    enable = mkEnableOption serviceName;
    nixSecretFile = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "nix store secret file";
    };
    virtualHosts = mkOption {
      type = types.listOf types.str;
      default = [];
      description = "Which Nginx virt hosts to pass.";
    };
    systemdService = mkOption {
      type = types.str;
      description = "Readonly, always reads ${serviceName}.service";
    };
  };
  config = {
    services = {
      ${serviceName}.systemdService = "${serviceName}.service";
      nginx = {
        enable = mkIf (serviceConfig.enable && (serviceConfig.virtualHosts != [])) (mkDefault true);
        virtualHosts = builtins.listToAttrs (
          builtins.map (name:
            nameValuePair name
            {
              locations."/" = {
                proxyPass = "http://unix:${sockPath}";
              };
            })
        );
      };
    };
    systemd.services.${serviceName} = {
      description = "nix-serve binary cache server";
      after = ["network.target"];
      wantedBy = ["multi-user.target"];

      # add to service's path
      # path = [ config.nix.package.out pkgs.bzip2.bin ];
      environment.NIX_REMOTE = "daemon";

      script =
        optionalString (builtins.isString serviceConfig.nixSecretFile) ''
          export NIX_SECRET_KEY_FILE="$CREDENTIALS_DIRECTORY/NIX_SECRET_KEY_FILE"
        ''
        + ''
          umask g+w  # Nginx needs to be able to read it.
          exec ${pkgs.nix-serve-ng}/bin/nix-serve --socket "${sockPath}"
        '';

      serviceConfig =
        {
          Restart = "on-failure";
          RestartSec = "10s";
          Group = "nginx";
          DynamicUser = true;
          ReadWritePaths = runDir;
          RuntimeDirectory = serviceName;
        }
        // (optionalAttrs (builtins.isString serviceConfig.nixSecretFile) {
          LoadCredential = "NIX_SECRET_KEY_FILE:${serviceConfig.nixSecretFile}";
        });
    };
  };
}
