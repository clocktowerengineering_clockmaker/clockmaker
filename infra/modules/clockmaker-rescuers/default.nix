{pkgs, ...}: let
  users = {
    nicholas = {
      description = "Nicholas";
      extraGroups = ["dialout" "networkmanager" "wheel"];
      # used `mkpasswd` command. Below the password is just "clockmaker"
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop"
      ];
    };
    mark = {
      description = "Mark";
      extraGroups = ["dialout" "networkmanager" "wheel"];
      # used `mkpasswd` command. Below the password is just "clockmaker"
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop"
      ];
    };
    daniel = {
      description = "Dan";
      extraGroups = ["dialout" "networkmanager" "wheel"];
      # used `mkpasswd` command. Below the password is just "clockmaker"
      isNormalUser = true;
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop"
      ];
      packages = with pkgs; [neovim ripgrep];
    };
  };
in {
  imports = [../../modules/clockmaker-secrets];
  config.users = {users = users // {root.openssh.authorizedKeys.keys = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAlNPXhTBWLs0M8JdXbIWV1vhDHy6qb6gRbB0lAUSa8N daniel@daniel-debian-desktop" ];};};
  # Blanket add all possible secrets for these users.
  #config.clockmaker.secrets.users = {daniel = {"enableSecrets" = "all";};}; #builtins.mapAttrs (userName: _: {enableSecrets = "all";});
  config.clockmaker.secrets.users = builtins.mapAttrs (userName: _: {enableSecrets = "all";}) users;
  # For nixos-rebuild remote deployment. Needs better organization.
  config.nix.settings.trusted-users = [ "root" ];
}
