# Reasonable defaults for the graphical install for our client machines.
{...}: {
  services.xserver.enable = true;
  services.xserver.desktopManager.cinnamon.enable = true;
  services.xserver.displayManager.lightdm = {
    enable = true;
    background = ../../../desktop/wallpapers/mk0.png;
  };
  services.displayManager.defaultSession = "cinnamon";
}
