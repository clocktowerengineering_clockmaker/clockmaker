{
  config,
  lib,
  ...
}: let
  inherit (lib) mergeEqualOption mkDefault mkOptionType mkOption;
  kebabTokenType = mkOptionType {
    name = "kebab-token-token-type";
    description = ''
      Only lowercase letters, numbers, and nonconsecutive hyphens (ex. a--b not allowed).
      Must start with lowercase letter.
      May not be empty.
      May not end with hyphen.
    '';
    merge = mergeEqualOption;
    check = s: (builtins.match "[a-z](([a-z0-9]+-?)*[a-z0-9])?" s) != null;
  };
in {
  options.clockmaker = {
    clientName = mkOption {
      description = "Client for whom this machine is built. Internal machines use \"clocktower\".";
      type = kebabTokenType;
    };
    machineName = mkOption {
      description = "ex. hephaestion";
      type = kebabTokenType;
    };
  };

  config = {
    networking.hostName = mkDefault (config.clockmaker.clientName + "--" + config.clockmaker.machineName);
  };
}
