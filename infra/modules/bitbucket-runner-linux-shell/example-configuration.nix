{...}: {
  imports = [
    ./default.nix
  ];
  services = {
    # Enable Bitbucket Runner Linux Shell Service
    bitbucket-runner-linux-shell.enable = true;

    bitbucket-runner-linux-shell.user = "bitbucket-runner-linux-shell";
    bitbucket-runner-linux-shell.group = "bitbucket-runner-linux-shell";

    # Flags for the Bitbucket runner
    bitbucket-runner-linux-shell.flags = {
      accountUuid = "{00000000-0000-0000-0000-000000000000}";
      repositoryUuid = "{00000000-0000-0000-0000-000000000000}";
      runnerUuid = "{00000000-0000-0000-0000-000000000000}";
      OAuthClientId = "this-was-a-triumph";
      OAuthClientSecret = "im_making_a_note_here___huge_success";
      workingDirectory = "/tmp";
      runtime = "linux-shell";
    };
  };
}
