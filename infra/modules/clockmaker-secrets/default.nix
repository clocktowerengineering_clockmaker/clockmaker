{
  config,
  lib,
  ...
}: let
  # inherit ((import (import ../nix/sources.nix {}).nixpkgs) {}) age;
  inherit (lib) attrsets mkIf mkOption types;

  inherit (import ../../../nix/sources.nix) agenix;
  mkSelectionType = validSelections: (
    types.either (types.enum ["all" "none"])
    (types.attrTag {
      only = types.listOf (types.enum validSelections);
      exceptFor = types.listOf (types.enum validSelections);
    })
  );

  checkSelection = selection: value: (
    (selection == "all")
    || (builtins.elem value (selection.only or [])
      || (!(builtins.elem value (selection.except or [value]))))
  );

  inherit (config.clockmaker) clientName machineName;

  # TODO: clean this section up.
  # Consider lib.makeOverridable.
  mkSecretOpts = {
    defaultSecretSourceDir ? config.clockmaker.secrets.secretSourceDir,
    defaultSecretsDir ? config.age.secretsDir,
    secretName,
    defaultOwner ? "0",
    defaultPath ? "${defaultSecretsDir}/${secretName}",
  }: (builtins.mapAttrs (_: mkOption) {
    name = {
      type = types.str;
      default = secretName;
      description = "Name of the file used in {option}`age.secretsDir`";
    };
    file = {
      type = types.path;
      default = defaultSecretSourceDir + /${secretName}.age;
      description = "Age file the secret is loaded from.";
    };
    path = {
      type = types.str;
      default = defaultPath;
      description = "Path where the decrypted secret is installed.";
    };
    mode = {
      type = types.str;
      default = "0400";
      description = "Permissions mode of the decrypted secret in a format understood by chmod.";
    };
    owner = {
      type = types.str;
      default = defaultOwner;
      description = "User of the decrypted secret.";
    };
    group = {
      type = types.str;
      default = config.users.${defaultOwner}.group or "0";
      defaultText = types.literalExpression ''
        users.''${config.owner}.group or "0"
      '';
      description = "Group of the decrypted secret.";
    };
    symlink = {
      description = "symlinking secrets to their destination";
      type = types.bool;
      default = true;
    };
  });

  mkClockmakerUserSecrets = submoduleArgs: let
    userName = submoduleArgs.name;
  in {
    account-password-hash = mkSecretOpts {
      defaultOwner = submoduleArgs.name;
      secretName = "${clientName}.user.${userName}.account-password-hash";
    };
    ssh-private-key = mkSecretOpts {
      defaultOwner = submoduleArgs.name;
      secretName = "${clientName}.user.${userName}.ssh-private-key";
    };
  };

  effectiveUserSecrets =
    builtins.mapAttrs (
      userName: userSecretsConfig:
        userSecretsConfig
        // {
          secrets =
            attrsets.filterAttrs (
              secretSuffix: _: checkSelection userSecretsConfig.enableSecrets secretSuffix
            )
            userSecretsConfig.secrets;
        }
    )
    config.clockmaker.secrets.users;

  hostSecrets = {
    tailscale-auth-key = mkSecretOpts {
      secretName = "${clientName}.machine.${machineName}.account-password-hash";
    };
  };
  effectiveHostSecrets =
    attrsets.filterAttrs (
      secretSuffix: _: checkSelection config.clockmaker.secrets.host.enableSecrets secretSuffix
    )
    config.clockmaker.secrets.host.secrets;
in {
  options.clockmaker.secrets = {
    secretSourceDir = mkOption {
      type = types.path;
      default = /tmp/clockmaker/secrets/${machineName}/${clientName};
    };

    host = {
      secrets = hostSecrets;
      enableSecrets = mkOption {
        description = "Which clockmaker host secrets to supply?";
        type = mkSelectionType (builtins.attrNames hostSecrets);
      };
    };

    users = mkOption {
      default = {};
      type = types.attrsOf (
        types.submodule (
          # `@{name,...}` seems to be mandatory; submoduleArgs.name isn't defined otherwise.
          # This is bad design on Nix's part.
          submoduleArgs @ {name, ...}: let
            clockmakerUserSecrets = mkClockmakerUserSecrets submoduleArgs;
          in {
            options = {
              enableSecrets = mkOption {
                description = "Which clockmaker user secrets to supply?";
                type = mkSelectionType (builtins.attrNames clockmakerUserSecrets);
              };
              secrets = clockmakerUserSecrets;
            };
          }
        )
      );
    };
  };

  imports = [
    "${agenix}/modules/age.nix"
    ../clockmaker-base
  ];
  # Ugly transpose from config.clockmaker.secrets to config.age.secrets
  config.age.secrets = let
    toSingletonSecretAttrset = (
      secret: attrsets.setAttrByPath [secret.name] secret
    );

    userSecrets = (
      builtins.concatMap builtins.attrValues
      (
        builtins.map (builtins.getAttr "secrets")
        (
          builtins.attrValues effectiveUserSecrets
        )
      )
    );
  in
    builtins.foldl' attrsets.unionOfDisjoint {}
    (
      builtins.map toSingletonSecretAttrset userSecrets
    );

  config.users.users =
    builtins.mapAttrs (
      userName: userSecretsConfig:
        attrsets.setAttrByPath ["hashedPasswordFile"]
        (
          mkIf
          (builtins.hasAttr "account-password-hash" userSecretsConfig.secrets)
          userSecretsConfig.secrets.account-password-hash.path
        )
    )
    effectiveUserSecrets;

  config.services.tailscale.authKeyFile =
    mkIf
    (builtins.hasAttr "tailscale-auth-key" effectiveHostSecrets)
    effectiveHostSecrets.tailscale-auth-key.path;
}
