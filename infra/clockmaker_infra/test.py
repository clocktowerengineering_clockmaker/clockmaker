from bitwarden_sdk import BitwardenClient
import os

client = BitwardenClient()
resp = client.auth().login_access_token(os.environ["BWS_ACCESS_TOKEN"])
assert resp.success, resp
