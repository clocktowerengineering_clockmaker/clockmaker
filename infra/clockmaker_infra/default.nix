let
  # Since the nixpkgs used here is independent of the one sourced through
  # the nixOS configuration, we need to specify allowUnfree in the nixpkgs
  # configuration here instead of the systemwide nixpkgs.
  # The solution here is to switch to flakes and get rid of niv to allow a
  # single nixpkgs to be used across our systems.
  # The allowUnfree specification should be in the module using this package,
  # not here.
  sources = import ../../nix/sources.nix {};
  nixpkgs = import sources.nixpkgs {};
  poetry2nix = import sources.poetry2nix {pkgs = nixpkgs;};
in
  poetry2nix.mkPoetryApplication {
    projectDir = ./.;
    preferWheels = true;
  }
