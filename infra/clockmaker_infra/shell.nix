let
  inherit (import (import ../../nix/sources.nix {}).nixpkgs {}) mkShell;
  provisioner = import ./.;
  mkPoetryShell = import ../../nix/poetry2nix-shell.nix;
  provisionerShellArgs = mkPoetryShell {poetry2nixApplication = provisioner;};
in
  mkShell provisionerShellArgs
