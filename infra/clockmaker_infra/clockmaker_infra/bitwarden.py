from os import environ
from pathlib import Path
from asyncio import (
    Lock,
    get_running_loop,
)
from typing import (
    Any,
    Iterable,
    Self,
    Sequence,
)
from uuid import UUID
from bitwarden_sdk import (
    BitwardenClient,
    ProjectResponse,
    SecretResponse,
)
from pydantic import (
    TypeAdapter,
    ValidationError,
)

from clockmaker_infra.models import (
    ClockmakerSecret,
    ClockmakerClientName,
    ClockmakerSecretKey,
    KeyT,
    NonEmptyStr,
)
import logging

logger = logging.getLogger(__name__)


def project_name_for_client(client_name: ClockmakerClientName) -> str:
    return "clockmaker-" + client_name


async def create_bitwarden_client(bws_access_token: NonEmptyStr) -> BitwardenClient:
    """Returns authenticated client."""
    client = BitwardenClient()
    auth_resp = await get_running_loop().run_in_executor(
        None, client.auth().login_access_token, bws_access_token
    )
    if not auth_resp.success:
        raise ValueError(auth_resp.error_message, auth_resp)

    return client


class ClockmakerKeyStore:
    """
    Makes keys for a specific clockmaker client.
    Keys are stored in client-specific projects.
    """

    @classmethod
    def _secret_response_to_clockmaker_secret(
        cls, bws_secret: SecretResponse, key: KeyT
    ) -> ClockmakerSecret[KeyT]:
        return ClockmakerSecret(
            id=bws_secret.id,
            project_id=bws_secret.project_id,
            organization_id=bws_secret.organization_id,
            key=key,
            value=bws_secret.value,
            note=bws_secret.note,
            creation_date=bws_secret.creation_date,
            revision_date=bws_secret.revision_date,
        )

    async def _get_or_create_project(
        self,
        project_suffix: str,
    ) -> ProjectResponse:
        """Use authenticated client to find or create Bitwarden Secrets project for a client."""
        if (project := self._projects.get(project_suffix)) is not None:
            return project

        project_names = await get_running_loop().run_in_executor(
            None, self._bitwarden_client.projects().list, str(self._organization_id)
        )
        if project_names.data is None:
            raise ValueError(project_names.error_message, project_names)
        projects_by_name = {
            project.name: project for project in project_names.data.data
        }
        client_project_name = project_name_for_client(project_suffix)
        client_project = projects_by_name.get(client_project_name)
        if client_project is not None:
            logger.debug(
                "Found project %s:%s", client_project_name, client_project.name
            )
            return client_project

        logger.info("Could not find project %s, creating...", client_project_name)
        get_project = await get_running_loop().run_in_executor(
            None,
            self._bitwarden_client.projects().create,
            str(self._organization_id),
            client_project_name,
        )
        if get_project.data is None:
            raise ValueError(get_project.error_message, get_project)
        self._projects[project_suffix] = get_project.data
        return get_project.data

    @classmethod
    async def _find_clockmaker_secrets(
        cls,
        bitwarden_client: BitwardenClient,
        organization_id: UUID,
    ) -> Sequence[ClockmakerSecret]:
        """Use authenticated client to find all Bitwarden Secrets we recognize for a project id"""
        # bitwarden_client.secrets().list gives secret "identifiers",
        # including the "key" (human readable name of secret) and "id" (uuid).
        list_client_secrets = await get_running_loop().run_in_executor(
            None, bitwarden_client.secrets().list, str(organization_id)
        )
        if list_client_secrets.data is None:
            raise ValueError(list_client_secrets.error_message, list_client_secrets)

        keys_by_id = dict[UUID, ClockmakerSecretKey]()
        _clockmaker_secret_key_type_adapter = TypeAdapter(ClockmakerSecretKey)
        # Narrow down the secret identifiers to ones we recognize.
        for secret_identifier in list_client_secrets.data.data:
            try:
                key = _clockmaker_secret_key_type_adapter.validate_python(
                    secret_identifier.key
                )
            except ValidationError as err:
                logger.warning(
                    "Ignoring unrecognized key %s:%s",
                    secret_identifier.key,
                    secret_identifier.id,
                    # err.errors(),
                )
                continue
            if (
                existing_secret_identifier_id := keys_by_id.get(secret_identifier.id)
            ) is not None:
                raise ValueError(
                    f"Encountered duplicate key {secret_identifier.key}:"
                    + f" {existing_secret_identifier_id} and {secret_identifier.id}."
                    + " Please resolve."
                )
            keys_by_id[secret_identifier.id] = key

        if len(keys_by_id) == 0:
            return []

        get_by_id_client_secrets = await get_running_loop().run_in_executor(
            None, bitwarden_client.secrets().get_by_ids, list(keys_by_id.keys())
        )
        if get_by_id_client_secrets.data is None:
            raise ValueError(
                get_by_id_client_secrets.error_message, get_by_id_client_secrets
            )

        return [
            cls._secret_response_to_clockmaker_secret(
                bws_secret, keys_by_id[bws_secret.id]
            )
            for bws_secret in get_by_id_client_secrets.data.data
        ]

    @classmethod
    async def construct(
        cls,
        bitwarden_client: BitwardenClient,
        organization_id: UUID,
    ) -> Self:
        """Create a keymaker for a specific Clocktower client.
        Don't confuse bitwarden_client (provides bitwarden secrets)
        with client_name (business logic).
        """
        secrets = await cls._find_clockmaker_secrets(bitwarden_client, organization_id)

        return cls(
            bitwarden_client=bitwarden_client,
            organization_id=organization_id,
            secrets=secrets,
        )

    def __init__(
        self,
        bitwarden_client: BitwardenClient,
        organization_id: UUID,
        secrets: Iterable[ClockmakerSecret],
    ) -> None:
        self._bitwarden_client = bitwarden_client
        self._organization_id = organization_id
        self._projects = dict[str, ProjectResponse]()
        self._secrets = {secret.key: secret for secret in secrets}
        self._lock = Lock()

    async def _create_secret(
        self, key: KeyT, value: str, note: str | None
    ) -> ClockmakerSecret[KeyT]:
        project = await self._get_or_create_project(key.client)

        create_secret = await get_running_loop().run_in_executor(
            None,
            self._bitwarden_client.secrets().create,
            self._organization_id,
            str(key),
            value,
            note,
            [project.id],
        )
        if create_secret.data is None:
            raise ValueError(create_secret.error_message, create_secret)

        return self._secret_response_to_clockmaker_secret(create_secret.data, key)

    async def get_secret(self, key: KeyT) -> ClockmakerSecret[KeyT] | None:
        async with self._lock:
            if (secret := self._secrets.get(key)) is not None:
                return secret

    async def get_or_create_secret(
        self, key: KeyT, value: str, note: str | None
    ) -> ClockmakerSecret[KeyT]:
        async with self._lock:
            # key may have been set between our check above
            # and the time we got the lock.
            if (secret := self._secrets.get(key)) is not None:
                return secret
            logging.info("Creating %s", key)
            secret = await self._create_secret(key, value, note)
            self._secrets[key] = secret
            return secret


def env_or_file_contents(varname: NonEmptyStr, path: Any):
    if path is None:
        if (access_token := environ.get(varname)) is not None:
            return access_token
        raise ValueError(varname + " not in env; you must provide arg")

    if not isinstance(path, str):
        return path

    path_ = Path(path)
    if not path_.is_file():
        raise ValueError("Is not file")

    return path_.read_text().strip()
