from base64 import b64encode
from cryptography.hazmat.primitives.asymmetric.ed25519 import (
    Ed25519PrivateKey,
    Ed25519PublicKey,
)
from cryptography.hazmat.primitives.serialization import (
    Encoding,
    PublicFormat,
    load_ssh_private_key,
    load_ssh_public_key,
)


def private_ssh_to_public_key(private_key: str | bytes) -> str:
    if isinstance(private_key, str):
        private_key = private_key.encode(encoding="ascii")
    return (
        load_ssh_private_key(private_key, password=None)
        .public_key()
        .public_bytes(encoding=Encoding.OpenSSH, format=PublicFormat.OpenSSH)
        .decode()
    )


def private_ssh_to_nix_store_private_key(private_key: str | bytes, name: str) -> str:
    if isinstance(private_key, str):
        private_key = private_key.encode(encoding="ascii")

    secret_key = load_ssh_private_key(private_key, password=None)
    if not isinstance(secret_key, Ed25519PrivateKey):
        raise ValueError("Nix store keys must be ed25519")
    catted = secret_key.private_bytes_raw() + secret_key.public_key().public_bytes_raw()
    return name + ":" + b64encode(catted).decode(encoding="ascii")


def public_ssh_to_nix_store_public_key(public_key: str | bytes, name: str) -> str:
    if isinstance(public_key, str):
        public_key = public_key.encode(encoding="ascii")
    pubkey = load_ssh_public_key(public_key)
    if not isinstance(pubkey, Ed25519PublicKey):
        raise ValueError("Nix store keys must be ed25519")
    return name + ":" + b64encode(pubkey.public_bytes_raw()).decode(encoding="ascii")
