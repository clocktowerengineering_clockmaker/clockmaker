from itertools import filterfalse, tee
from typing import Callable, Iterable, TypeVar

T = TypeVar("T")


def partition(
    predicate: Callable[[T], bool], iterable: Iterable[T]
) -> tuple[Iterable[T], Iterable[T]]:
    """Partition entries into false (left) entries and true entries (right).

    If *predicate* is slow, consider wrapping it with functools.lru_cache().
    """
    # partition(is_odd, range(10)) → 0 2 4 6 8   and  1 3 5 7 9
    t1, t2 = tee(iterable)
    return filterfalse(predicate, t1), filter(predicate, t2)
