from typing import TypeVar


T = TypeVar("T")


def identity(t: T) -> T:
    return t


def join(t: T) -> T:
    return t
