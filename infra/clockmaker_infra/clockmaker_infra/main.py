from functools import partial
import logging
from pathlib import Path
from uuid import UUID
import pyrage
import pyrage.ssh  # pyright: ignore[reportMissingImports]
from clipstick import parse
from asyncio import run
from typing import (
    TYPE_CHECKING,
    Annotated,
    AsyncIterable,
    Iterable,
    cast,
)
from pydantic import (
    AfterValidator,
    BaseModel,
    BeforeValidator,
    Field,
    TypeAdapter,
    ValidationError,
)

from clockmaker_infra.bitwarden import (
    ClockmakerKeyStore,
    create_bitwarden_client,
    env_or_file_contents,
)
from clockmaker_infra.conversion import (
    private_ssh_to_nix_store_private_key,
    private_ssh_to_public_key,
    public_ssh_to_nix_store_public_key,
)
from clockmaker_infra.generation import gen_password_and_hash, gen_private_ssh_key
from clockmaker_infra.models import (
    AccountPasswordHash,
    ClockmakerClientName,
    ClockmakerMachineScopedKey,
    ClockmakerSecret,
    ClockmakerSecretKey,
    ClockmakerUserScopedKey,
    NixStorePrivateKey,
    NixStoreTrustedPublicKeys,
    NonEmptyStr,
    SshPrivateKey,
    SshPublicKey,
)
from clockmaker_infra.nix import AgenixSecretSpec, NixOSConfigurationReader

logger = logging.getLogger(__name__)


class Ensure(BaseModel):
    """Create secrets"""

    what: ClockmakerSecretKey


class GenerateForConfig(BaseModel):
    """Generate secrets where configuration file expects them."""

    config_file: Path
    """configuration.nix"""


class ProvisionerArgs(BaseModel):
    bws_access_token_file: Annotated[
        NonEmptyStr,
        AfterValidator(str.strip),
        BeforeValidator(partial(env_or_file_contents, "BWS_ACCESS_TOKEN")),
    ] = Field(default=None, validate_default=True)
    """Manually specify bitwarden secret access token
AS PATH TO FILE WHOSE CONTENTS ARE THIS TOKEN.
If $BWS_ACCESS_TOKEN not set, reads from this flag.
"""
    bws_organization_id_file: Annotated[
        UUID,
        BeforeValidator(partial(env_or_file_contents, "BWS_ORGANIZATION_ID")),
    ] = Field(default=None, validate_default=True)
    """Manually specify bitwarden organization name
AS PATH TO FILE WHOSE CONTENTS ARE THIS TOKEN.
If $BWS_ORGANIZATION_ID not set, reads from this flag.
"""
    #     host: list[ClockmakerHostName] = []
    #     """Hosts to add as recipients to this secret.
    # Missing hosts have ssh key pairs generated for them.
    #     """
    #     user: list[ClockmakerUserName] = []
    #     """Users to add as recipients to this secret.
    # Missing users have ssh key pairs generated for them.
    #     """

    what: Ensure | GenerateForConfig


async def build(
    key_store: ClockmakerKeyStore,
    key: ClockmakerSecretKey,
) -> tuple[ClockmakerSecret, str]:
    if (direct_match := (await key_store.get_secret(key))) is not None:
        return direct_match, direct_match.value

    match key.kind:
        case SshPrivateKey():
            private_key = gen_private_ssh_key()
            secret = await key_store.get_or_create_secret(
                key=key,
                value=private_key.decode("ascii", errors="strict"),
                note=private_ssh_to_public_key(private_key),
            )
            return secret, secret.value
        case SshPublicKey():
            match key:
                case ClockmakerUserScopedKey() | ClockmakerMachineScopedKey():
                    private_secret_key = type(key)(
                        client=key.client, name=key.name, kind=SshPrivateKey()
                    )
                case other:
                    raise ValueError(other)

            secret, value = await build(key_store, private_secret_key)
            return secret, private_ssh_to_public_key(value)

        case AccountPasswordHash():
            password, hashed = gen_password_and_hash()
            secret = await key_store.get_or_create_secret(
                key,
                value=hashed,
                note=f'Password: "{password}"',
            )
            return secret, secret.value

        case NixStorePrivateKey():
            secret, value = await build(
                key_store,
                ClockmakerMachineScopedKey(
                    client="clocktower", name="nixnucdan", kind=SshPrivateKey()
                ),
            )
            return (
                secret,
                private_ssh_to_nix_store_private_key(
                    value, name="clocktower-nixnucdan"
                ),
            )

        case NixStoreTrustedPublicKeys():
            secret, value = await build(
                key_store,
                ClockmakerMachineScopedKey(
                    client="clocktower", name="nixnucdan", kind=SshPublicKey()
                ),
            )
            return (
                secret,
                public_ssh_to_nix_store_public_key(value, name="clocktower-nixnucdan"),
            )


def _valid_secret_key(
    agenix_secret_spec: AgenixSecretSpec,
) -> ClockmakerSecretKey | None:
    try:
        return cast(
            ClockmakerSecretKey,
            TypeAdapter(ClockmakerSecretKey).validate_python(agenix_secret_spec.name),
        )
    except ValidationError as err:
        logger.warning(
            "Not sure what to do with %s", agenix_secret_spec.name, exc_info=err
        )


async def generate_for_config(
    key_store: ClockmakerKeyStore, target: GenerateForConfig
) -> int:
    reader = NixOSConfigurationReader()
    # TODO: Make this program check that a configuration can only read
    # PUBLIC + client name.

    secrets = await reader.get_agenix_secrets(target.config_file)
    client_name = await reader.get_clockmaker_client_name(target.config_file)
    machine_name = await reader.get_clockmaker_machine_name(target.config_file)
    has_errs = False
    for agenix_secret_spec in secrets.values():
        if (requested_key := _valid_secret_key(agenix_secret_spec)) is None:
            has_errs = True
            continue

        logger.debug("Ensuring %s", requested_key)
        secret, value = await build(key_store, requested_key)
        logger.debug("Evaluating recipients for %s", requested_key)
        _, machine_public_key = await build(
            key_store,
            ClockmakerMachineScopedKey(
                client=client_name, name=machine_name, kind=SshPublicKey()
            ),
        )
        # TODO: add client key if necessary. Not sure yet.
        # if isinstance(requested_key, 123) or isinstance(requested_key)

        logger.debug("Encrypting contents of %s", requested_key)
        age_contents: bytes = pyrage.encrypt(  # pyright: ignore[reportAttributeAccessIssue]
            value.encode("ascii"),
            [pyrage.ssh.Recipient.from_str(machine_public_key)],
        )
        logger.info(
            "Writing encrypted contents of %s to %s",
            requested_key,
            agenix_secret_spec.file,
        )
        agenix_secret_spec.file.parent.mkdir(mode=0o700, parents=True, exist_ok=True)
        agenix_secret_spec.file.write_bytes(age_contents)

    return int(has_errs)


async def main_() -> int:
    args = parse(ProvisionerArgs)

    bitwarden_client = await create_bitwarden_client(args.bws_access_token_file)

    key_store = await ClockmakerKeyStore.construct(
        bitwarden_client, args.bws_organization_id_file
    )

    if isinstance(args.what, GenerateForConfig):
        return await generate_for_config(key_store, args.what)
    else:
        secret, value = await build(key_store, args.what.what)
        logger.info("Computed %s from %s", str(args.what), str(secret.key))
        print(value)
    return 0


def main() -> None:
    logging.basicConfig(level=logging.INFO)
    import sys

    sys.exit(run(main_()))
