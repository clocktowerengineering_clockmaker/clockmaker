from abc import ABC, abstractmethod
from dataclasses import dataclass
from pathlib import Path
import re
from functools import cache
from uuid import UUID
from pydantic import ConfigDict, validator
from pydantic.json_schema import JsonSchemaValue
from pydantic_core import CoreSchema
from pydantic_core.core_schema import literal_schema, str_schema
from datetime import datetime
from typing import (
    TYPE_CHECKING,
    Annotated,
    Any,
    Callable,
    ClassVar,
    Generic,
    Literal,
    Pattern,
    TypeAlias,
    TypeVar,
)
from annotated_types import MinLen
from pydantic import (
    BaseModel,
    Field,
    GetCoreSchemaHandler,
    GetJsonSchemaHandler,
    model_serializer,
    model_validator,
)

NonEmptyStr: TypeAlias = Annotated[str, MinLen(1)]
StrPath: TypeAlias = str | Path

LOWER_KEBAB_TOKEN_PATTERN = r"[a-z](([a-z0-9]+-?)*[a-z0-9])?"
_LowerKebabTokenStr: TypeAlias = Annotated[
    str, Field(pattern=LOWER_KEBAB_TOKEN_PATTERN)
]

ClockmakerClientName: TypeAlias = _LowerKebabTokenStr
ClockmakerHostName: TypeAlias = _LowerKebabTokenStr
ClockmakerMachineName: TypeAlias = _LowerKebabTokenStr
ClockmakerUserName: TypeAlias = _LowerKebabTokenStr


class StrLiteral(BaseModel):
    value: ClassVar[_LowerKebabTokenStr]

    def __hash__(self) -> int:
        return hash(self.__str__())

    @classmethod
    def __get_pydantic_json_schema__(
        cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler, /
    ) -> JsonSchemaValue:
        if not hasattr(cls, "_value"):
            return super().__get_pydantic_json_schema__(core_schema, handler)
        return handler(literal_schema([cls.value]))

    @model_validator(mode="before")
    @classmethod
    def try_validate(cls, data: Any) -> Any:
        if not isinstance(data, str):
            return data

        if data == cls.value:
            # Pydantic is so strange sometimes.
            return {"value": cls.value}

        raise ValueError(f"didn't get {cls.value}")

    @model_serializer
    def __str__(self) -> str:
        return self.value


class NixStoreSubstituters(StrLiteral):
    """newline-separated."""

    value = "nix-store-substituters"


class NixStoreTrustedPublicKeys(StrLiteral):
    """newline-separated."""

    value = "nix-store-trusted-public-keys"


class NixStorePrivateKey(StrLiteral):
    value = "nix-store-private-key"


class SshPrivateKey(StrLiteral):
    value = "ssh-private-key"


class SshPublicKey(StrLiteral):
    value = "ssh-public-key"


class AccountPasswordHash(StrLiteral):
    value = "account-password-hash"


class TemplatedStrModel(ABC, BaseModel):
    def __hash__(self) -> int:
        return hash(self.__str__())

    @classmethod
    @abstractmethod
    def format(cls) -> str:
        """Something like 'account-password.{fieldname1}.{fieldname2}'"""
        raise NotImplementedError

    @classmethod
    def pattern(cls) -> str:
        """ex. 'account-password\\.(?P<fieldname1>.*)\\.(?P<fieldname2>.*)'"""
        if TYPE_CHECKING:
            assert issubclass(cls, BaseModel)
        return (
            cls.format()
            .replace(".", r"\.")
            .format_map({key: rf"(?P<{key}>.*)" for key in cls.model_fields.keys()})
        )

    @classmethod
    @cache
    def pattern_compiled(cls) -> Pattern[str]:
        """ex. 'account-password\\.(?P<fieldname1>.*)\\.(?P<fieldname2>.*)'"""
        return re.compile(cls.pattern())

    @classmethod
    def __get_pydantic_json_schema__(
        cls, core_schema: CoreSchema, handler: GetJsonSchemaHandler, /
    ) -> JsonSchemaValue:
        return handler(
            str_schema(pattern=cls.pattern_compiled(), regex_engine="python-re")
        )

    @model_validator(mode="before")
    @classmethod
    def try_validate(cls, data: Any) -> Any:
        if not isinstance(data, str):
            return data
        matches = cls.pattern_compiled().fullmatch(data)
        if matches is None:
            raise ValueError("Got no match")
        return matches.groupdict()

    @model_serializer
    def __str__(self) -> str:
        # this is inefficient but hey it's Python
        if TYPE_CHECKING:
            assert isinstance(self, BaseModel)
        return self.format().format_map(
            {key: getattr(self, key) for key in self.model_fields.keys()}
        )


class ClockmakerPublicKey(TemplatedStrModel, BaseModel):
    """Keys for everyone!"""

    client: Literal["PUBLIC"] = "PUBLIC"
    # kind: NixStoreTrustedPublicKeys | NixStoreSubstituters
    kind: NixStoreTrustedPublicKeys

    @classmethod
    def format(cls) -> str:
        return "PUBLIC.{kind}"


class ClocktowerPrivateKey(TemplatedStrModel, BaseModel):
    """Keys for everyone!"""

    client: Literal["clocktower"] = "clocktower"
    kind: NixStorePrivateKey

    @classmethod
    def format(cls) -> str:
        return "clocktower.{kind}"


class ClockmakerClientScopedKey(TemplatedStrModel, BaseModel):
    """Client wide keys."""

    client: ClockmakerClientName
    kind: NixStorePrivateKey

    @classmethod
    def format(cls) -> str:
        return "{client}.{kind}"


class ClockmakerMachineScopedKey(TemplatedStrModel, BaseModel):
    client: ClockmakerClientName
    name: ClockmakerMachineName
    kind: SshPrivateKey | SshPublicKey

    @classmethod
    def format(cls) -> str:
        return "{client}.machine.{name}.{kind}"


class ClockmakerUserScopedKey(TemplatedStrModel, BaseModel):
    client: ClockmakerClientName
    name: ClockmakerHostName
    kind: SshPrivateKey | SshPublicKey | AccountPasswordHash

    @classmethod
    def format(cls) -> str:
        return "{client}.user.{name}.{kind}"


ClockmakerSecretKey = (
    ClockmakerPublicKey
    | ClockmakerClientScopedKey
    | ClockmakerMachineScopedKey
    | ClockmakerUserScopedKey
)


KeyT = TypeVar("KeyT", bound=ClockmakerSecretKey)


@dataclass(frozen=True)
class ClockmakerSecret(Generic[KeyT]):
    id: UUID
    project_id: UUID | None
    """1-1 correspondence between project_id and client name"""
    organization_id: UUID
    """Currently, all Clockmaker secrets are in a single org."""
    key: KeyT
    value: str
    note: str
    creation_date: datetime
    revision_date: datetime
