"""Tools to automatically introspects nix machine configurations."""

from asyncio.subprocess import create_subprocess_exec, PIPE
from io import BytesIO
from pathlib import Path
from sys import stderr
from typing import TypeAlias

from pydantic import BaseModel, NonNegativeInt, ConfigDict, TypeAdapter, ValidationError

from clockmaker_infra.models import (
    ClockmakerClientName,
    ClockmakerHostName,
    ClockmakerMachineName,
    NonEmptyStr,
    StrPath,
)


class AgenixSecretSpec(BaseModel):
    """Small selection, not exhaustive."""

    model_config = ConfigDict(extra="ignore")
    file: Path
    owner: NonNegativeInt | NonEmptyStr
    """Either UID or string. We capture both but only handle UID 0 + string cases."""
    group: NonNegativeInt | NonEmptyStr
    mode: str
    """ex. "0400", an octal number. Will make this a validated octal if necessary."""
    name: NonEmptyStr
    """Name of the file when decrypted/mounted. 
    We ignore this and use the declared name.
    """


AgenixSecrets: TypeAlias = dict[NonEmptyStr, AgenixSecretSpec]


class NixOSConfigurationReader:
    """Reads NixOS configurations.
    Setting this up as a class in case we later want to parameterize
    the nixpkgs we use later.
    """

    async def _evaluate_configuration(
        self, configuration_path: StrPath, attr: str
    ) -> bytes:
        """
        ``configuration_path`` interpreted by "nix eval" program.
        Passed unquoted, so provide quotes if you need.

        ``attr`` can be "config.age.secrets", "config.networking.hostName", etc.
        """
        args = [
            "nix",
            "eval",
            "--no-update-lock-file",
            "--read-only",  # TODO: Make sure this doesn't have unintended effects.
            "--json",
            "--show-trace",
            "--file",
            # TODO: parameterize this if we want/need?
            # Would be good to use the same nixpkgs that runs on the target.
            "<nixpkgs/nixos>",
            "--arg",
            "configuration",
            str(configuration_path),
            attr,
        ]
        proc = await create_subprocess_exec(*args, stdout=PIPE, stderr=PIPE)
        stdout_, stderr_ = await proc.communicate()
        if proc.returncode != 0:
            stderr.write(stderr_.decode())
            raise ValueError(
                f"{args} returned {proc.returncode}\nDid you import the clockmaker-secrets module?"
            )

        return stdout_

    async def get_agenix_secrets(self, configuration_path: StrPath) -> AgenixSecrets:
        return TypeAdapter(AgenixSecrets).validate_json(
            await self._evaluate_configuration(configuration_path, "config.age.secrets")
        )

    async def get_clockmaker_machine_name(
        self, configuration_path: StrPath
    ) -> ClockmakerMachineName:
        return TypeAdapter(ClockmakerMachineName).validate_json(
            await self._evaluate_configuration(
                configuration_path, "config.clockmaker.machineName"
            )
        )

    async def get_clockmaker_client_name(
        self, configuration_path: StrPath
    ) -> ClockmakerClientName:
        try:
            return TypeAdapter(ClockmakerClientName).validate_json(
                await self._evaluate_configuration(
                    configuration_path, "config.clockmaker.clientName"
                )
            )
        except (ValueError, ValidationError) as err:
            raise ValueError(
                "Error evaluating config.clockmaker.clientName for {configurationPath}. Are you sure the configurtion exists & imports the clockmaker-base module?"
            ) from err
