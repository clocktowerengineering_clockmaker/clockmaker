from base64 import urlsafe_b64encode
from os import urandom
from passlib.hosts import linux_context
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
from typing import (
    cast,
)
from xkcdpass.xkcd_password import (
    locate_wordfile,
    generate_wordlist,
    generate_xkcdpassword,
)
from cryptography.hazmat.primitives.serialization import (
    PrivateFormat,
    Encoding,
    NoEncryption,
)


def gen_private_ssh_key() -> bytes:
    """Returns
    -----BEGIN OPENSSH PRIVATE KEY-----
    HIHIHIHI
    LOTSOFTEXT
    -----END OPENSSH PRIVATE KEY-----
    """
    return Ed25519PrivateKey.generate().private_bytes(
        encoding=Encoding.PEM,
        format=PrivateFormat.OpenSSH,
        encryption_algorithm=NoEncryption(),
    )


_wordfile = locate_wordfile()
_wordlist = generate_wordlist(_wordfile)


def gen_password() -> str:
    """Generate friendly xkcd-style password."""
    return (
        cast(str, generate_xkcdpassword(_wordlist, case="random", delimiter="-"))
        + "-"
        + urlsafe_b64encode(urandom(3)).decode()
    )


def gen_password_and_hash() -> tuple[str, str]:
    """Returns (pasword, mkpasswd passwd)."""
    password = gen_password()
    # Equivalent to linux mkpasswd command.
    hashed = linux_context.hash(password)
    return password, hashed
