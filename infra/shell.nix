let
  inherit (import ../nix/sources.nix) agenix nixpkgs;
  inherit (import nixpkgs {}) age alejandra bitwarden-cli bws callPackage mkShell;
in
  mkShell {
    packages = [
      (callPackage "${agenix}/pkgs/agenix.nix" {})
      age
      bitwarden-cli
      bws # bitwarden secret cli
      alejandra
    ];
  }
