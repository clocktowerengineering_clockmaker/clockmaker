let
  inherit (import (import ../nix/sources.nix {}).nixpkgs {}) mariadb mkShell;
  pyhdbpp = import ./.;
  mkPoetryShell = import ../nix/poetry2nix-shell.nix;
  pyhdbppShellArgs = mkPoetryShell {poetry2nixApplication = pyhdbpp;};
in
  mkShell (pyhdbppShellArgs // {packages = pyhdbppShellArgs.packages ++ [mariadb];})
