from typing import TypeAlias

from sqlalchemy.ext.asyncio.session import async_sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession


AsyncSessionMaker: TypeAlias = async_sessionmaker[AsyncSession]
