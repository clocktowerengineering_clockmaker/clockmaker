from datetime import datetime
from typing import Generic, Literal, Type, TypeVar

from sqlalchemy import PrimaryKeyConstraint, func
from sqlalchemy.orm import Mapped, declarative_base, declared_attr, mapped_column
from sqlalchemy.dialects.mysql import TIMESTAMP, TINYINT, INTEGER
from sqlalchemy.sql.schema import SchemaEventTarget
from sqlalchemy.sql.type_api import TypeEngine

T = TypeVar("T")


class _AttScalarMixin(Generic[T]):
    @classmethod
    def scalar_str(cls) -> str:
        raise NotImplementedError

    @classmethod
    def scalar_col(cls) -> Type[TypeEngine[T]] | TypeEngine[T] | SchemaEventTarget:
        raise NotImplementedError

    @classmethod
    def read_mode(cls) -> Literal["rw", "ro"]:
        raise NotImplementedError

    @declared_attr.directive
    def __tablename__(cls) -> str:
        return f"att_scalar_dev{cls.scalar_str()}_{cls.read_mode()}"

    @declared_attr
    def att_conf_id(cls) -> Mapped[int]:
        return mapped_column(INTEGER(10))

    @declared_attr
    def data_time(cls) -> Mapped[datetime | None]:
        return mapped_column(
            TIMESTAMP(fsp=6), nullable=True, server_default="0000-00-00 00:00:00.000000"
        )

    @declared_attr
    def recv_time(cls) -> Mapped[datetime | None]:
        return mapped_column(
            TIMESTAMP(fsp=6), nullable=True, server_default="0000-00-00 00:00:00.000000"
        )

    @declared_attr
    def insert_time(cls) -> Mapped[datetime | None]:
        return mapped_column(
            TIMESTAMP(fsp=6),
            nullable=True,
            server_default="0000-00-00 00:00:00.000000",
            default=func.now(),
        )

    @declared_attr
    def quality(cls) -> Mapped[int]:
        return mapped_column(TINYINT(1), nullable=False, server_default=None)

    @declared_attr
    def att_error_desc_id(cls) -> Mapped[str]:
        return mapped_column(
            INTEGER(10, unsigned=True), nullable=False, server_default=None
        )

    @declared_attr.directive
    def __table_args__(cls) -> tuple:
        old_args = getattr(super(), "__table_args__", ())
        return (*old_args, PrimaryKeyConstraint("att_conf_id", "data_time"))


class AttScalarMixinRO(_AttScalarMixin[T]):
    @classmethod
    def read_mode(cls) -> Literal["ro"]:
        return "ro"

    @declared_attr
    def value_r(cls) -> Mapped[T]:
        return mapped_column(cls.scalar_col(), nullable=False, server_default=None)


class AttScalarMixinRW(_AttScalarMixin[T]):
    @classmethod
    def read_mode(cls) -> Literal["rw"]:
        return "rw"

    @declared_attr
    def value_r(cls) -> Mapped[T]:
        return mapped_column(cls.scalar_col(), nullable=False, server_default=None)

    @declared_attr
    def value_w(cls) -> Mapped[T]:
        return mapped_column(cls.scalar_col(), nullable=False, server_default=None)


Base = declarative_base()


class AttScalarLongRO(AttScalarMixinRO[int], Base):
    @classmethod
    def scalar_col(cls) -> INTEGER:
        return INTEGER(11)

    @classmethod
    def scalar_str(cls) -> Literal["long"]:
        return "long"
