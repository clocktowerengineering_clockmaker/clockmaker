from collections.abc import AsyncGenerator
from datetime import datetime, timedelta
import pytest_asyncio
from functools import cache
from os import chmod
from typing import Any
from pytest_mysql.config import get_config
from pytest import FixtureRequest, fixture
from shutil import which
from pytest import mark
from getpass import getuser
from pathlib import Path

from pytest_mysql.executor import MySQLExecutor
from pytest_mysql.factories import mysql_proc
from sqlalchemy import text
from sqlalchemy.engine import URL
from sqlalchemy.ext.asyncio import AsyncEngine, create_async_engine
from tempfile import NamedTemporaryFile

from sqlalchemy.ext.asyncio.session import async_sessionmaker

from pyhdbpp.db import AttScalarLongRO
from pyhdbpp.types import AsyncSessionMaker


schema_path = Path(__file__).parent / "../create_hdb++_mysql.sql"
create_hdbpp_schema = text(schema_path.read_text())


def ss(*args, **kwargs):
    print(args)
    print(kwargs)


class MyMySQLExecutor(MySQLExecutor):
    """MySQL Executor for running MySQL server."""

    pass


if (
    mariadb_admin_loc := which("mariadb-admin")
) is not None and mariadb_admin_loc.startswith("/nix/store"):
    # shutils.which('mariadbd')
    basedir = Path(mariadb_admin_loc).parent.parent

    @cache
    def _wrap(progname: str) -> Path:
        wrappee_path = basedir / "bin" / progname
        with NamedTemporaryFile(
            mode="w", delete=False, prefix="wrapped", suffix=progname
        ) as wrapped:
            chmod(wrapped.fileno(), 0o700)
            wrapped.write(
                "#!/usr/bin/env bash\n"
                f'exec {wrappee_path} --no-defaults --basedir {basedir} "$@"'
            )
        return Path(wrapped.name)

    mysql_my_proc = mysql_proc(
        mysqld_exec=_wrap(progname="mariadbd"),
        admin_executable=str(mariadb_admin_loc),
        mysqld_safe=_wrap(progname="mariadbd-safe"),
        install_db=str(_wrap(progname="mariadb-install-db")),
        port=None,
        user=getuser(),
    )
else:
    mysql_my_proc = mysql_proc(port=None, user=getuser())


@pytest_asyncio.fixture(scope="function")
async def hdbpp_engine(
    mysql_my_proc: MySQLExecutor, request: FixtureRequest
) -> AsyncGenerator[AsyncEngine]:
    if not mysql_my_proc.running():
        mysql_my_proc.start()

    config = get_config(request)

    print(config)
    print(request)

    mysql_user = mysql_my_proc.user
    mysql_passwd = config["passwd"]
    mysql_dbname = "blah"

    connection_kwargs: dict[str, Any] = {
        "host": mysql_my_proc.host,
        "user": mysql_user,
        "passwd": mysql_passwd,
    }

    if mysql_my_proc.unixsocket:
        connection_kwargs["unix_socket"] = mysql_my_proc.unixsocket

        engine = create_async_engine(
            URL.create(
                drivername="mysql+asyncmy",
                query={"unix_socket": mysql_my_proc.unixsocket},
            )
        )
    else:
        engine = create_async_engine(
            URL.create(drivername="mysql+asyncmy", port=mysql_my_proc.port)
        )

    create_db_str = f"CREATE DATABASE `{mysql_dbname}`;" f"USE `{mysql_dbname}`;"
    async with engine.connect() as conn:
        await conn.execute(text(create_db_str))
        await conn.execute(create_hdbpp_schema)

    yield engine
    query_drop_database = f"DROP DATABASE IF EXISTS `{mysql_dbname}`"
    async with engine.connect() as conn:
        await conn.execute(text(query_drop_database))


@fixture
def hdbpp_sessionmaker(hdbpp_engine: AsyncEngine) -> AsyncSessionMaker:
    return async_sessionmaker(hdbpp_engine)


# @mark.asyncio
# async def test_mydb(hdbpp_engine: AsyncEngine):
#
#     async with hdbpp_engine.connect() as conn:
#         x = await conn.execute(text('show tables;'))
#         list_x = list(x)
#     breakpoint()
#     print(hdbpp_engine)


@mark.asyncio
async def test_record_insertion(hdbpp_sessionmaker: AsyncSessionMaker):
    record = AttScalarLongRO()
    now = datetime.now()
    record.att_conf_id = 99  # making things up
    record.data_time = now
    record.recv_time = now + timedelta(seconds=11)
    record.quality = 10  # making things up
    record.att_error_desc_id = 12  # making things up

    async with hdbpp_sessionmaker() as session:
        session.add(record)
