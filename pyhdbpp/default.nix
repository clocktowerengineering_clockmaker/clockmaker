let
  sources = import ../nix/sources.nix {};
  nixpkgs = import sources.nixpkgs {};
  poetry2nix = import sources.poetry2nix {pkgs = nixpkgs;};
in
  poetry2nix.mkPoetryApplication {
    projectDir = ./.;
    preferWheels = true;
  }
