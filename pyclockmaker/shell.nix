let
  inherit (import (import ../nix/sources.nix {}).nixpkgs {}) mkShell;
  pyclockmaker = import ./. {};
  mkPoetryShellArgs = import ../nix/poetry2nix-shell.nix;
in
  mkShell (mkPoetryShellArgs {poetry2nixApplication = pyclockmaker;})
