# Modifying behavior in /NixOS/nixpkgs/master/pkgs/build-support/setup-hooks/make-wrapper.sh
# Starter/Astor seems to require that device server like "GalilRioxxx" 
# shows up in ps -ax as "GalilRioxxx" rather than ".GalilRioxxx.wrapped",
# the default behavior of wrapProgram.
# Thus, this is a fix to make that happen.
wrapProgramShell() {
    local prog="$1"
    local hidden

    assertExecutable "$prog"

    # THIS IS THE LINE WE MODIFY
    # hidden="$(dirname "$prog")/.$(basename "$prog")"-wrapped
    mkdir -p "$(dirname "$prog")/.wrapped"
    hidden="$(dirname "$prog")/.wrapped/$(basename "$prog")"
    while [ -e "$hidden" ]; do
      hidden="{hidden}_"
    done
    mv "$prog" "$hidden"
    makeShellWrapper "$hidden" "$prog" --inherit-argv0 "${@:2}"
}

