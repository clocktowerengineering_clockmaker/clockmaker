import itertools

def assign(lst, idx, value, fill=None):
    diff = len(lst) - idx
    if diff >= 0:
        lst[idx] = value
    else:
        lst.extend(itertools.repeat(fill, -diff))
        lst.append(value)

class rlist(list):
  def __init__(self, default):
    self._default = default
  def __setitem__(self, key, value):
    if key >= len(self):
      self += [self._default] * (key - len(self) + 1)
    super(rlist, self).__setitem__(key, value)


if __name__ == "__main__":
   
    listData = [rlist(0),rlist(0)]
    #listData[1][1]=1
    assign[1]
    for i in range(len(listData)):
        print(listData[i])
    #listData[2][2]=2
    for i in range(len(listData)):
        print(listData[i])    