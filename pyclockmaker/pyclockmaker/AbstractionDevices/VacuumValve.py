#!/usr/bin/env python
# -*- coding:utf-8 -*-


# ############################################################################
#  license :
# ============================================================================
#
#  File :        VacuumValve.py
#
#  Project :     
#
# This file is part of Tango device class.
# 
# Tango is free software: you can redistribute it and/or modify
# it under the terms of the MIT licence.
# 
#
#  $Author :      mark.amato$
#
#  $Revision :    $
#
#  $Date :        $
#
#  $HeadUrl :     $
# ============================================================================
#            This file is generated by POGO
#     (Program Obviously used to Generate tango Object)
# ############################################################################

__all__ = ["VacuumValve", "VacuumValveClass", "main"]

__docformat__ = 'restructuredtext'

import PyTango
import sys
# Add additional import
#----- PROTECTED REGION ID(VacuumValve.additionnal_import) ENABLED START -----#
from scipy import interpolate
from datetime import datetime
import traceback
import re
import time
import threading
#----- PROTECTED REGION END -----#	//	VacuumValve.additionnal_import

# Device States Description
# ON : 
# OFF : 
# OPEN : 
# CLOSE : 
# ALARM : 


class VacuumValve (PyTango.LatestDeviceImpl):
    """Simple vacuumvalve. Depending on configuration:
    
    Outputs:
    
    Open (boolean)
    
    Inputs:
    
    Open (boolean)"""
    
    # -------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(VacuumValve.global_variables) ENABLED START -----#
    queueAction = False
    queueActionData = False
    updateInterval = 0.25 # update interval
    updateTimeout = 0.1 # timeout for threading
    #----- PROTECTED REGION END -----#	//	VacuumValve.global_variables

    def __init__(self, cl, name):
        PyTango.LatestDeviceImpl.__init__(self,cl,name)
        self.debug_stream("In __init__()")
        VacuumValve.init_device(self)
        #----- PROTECTED REGION ID(VacuumValve.__init__) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(VacuumValve.delete_device) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_openValve_read = False
        self.attr_valveOpened_read = False
        self.attr_valveLocked_read = False
        #----- PROTECTED REGION ID(VacuumValve.init_device) ENABLED START -----#
        self.set_state(PyTango.DevState.INIT)
        self.e1 = threading.Event()
        self.e2 = threading.Event()
        self.ioThread = threading.Thread(target = self.IOMethod)
        self.ioThread.setDaemon(True)
        self.ioThread.start()
        #----- PROTECTED REGION END -----#	//	VacuumValve.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(VacuumValve.always_executed_hook) ENABLED START -----#
        
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.always_executed_hook

    # -------------------------------------------------------------------------
    #    VacuumValve read/write attribute methods
    # -------------------------------------------------------------------------
    
    def read_openValve(self, attr):
        self.debug_stream("In read_openValve()")
        #----- PROTECTED REGION ID(VacuumValve.openValve_read) ENABLED START -----#
        attr.set_value(self.attr_openValve_read)
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.openValve_read
        
    def write_openValve(self, attr):
        self.debug_stream("In write_openValve()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(VacuumValve.openValve_write) ENABLED START -----#
        
        self.queueActionData = data

        if not self.attr_valveLocked_read:
            self.queueAction = True

        self.e1.set()
        self.e2.wait(self.updateInterval)
        self.e2.clear()
               
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.openValve_write
        
    def read_valveOpened(self, attr):
        self.debug_stream("In read_valveOpened()")
        #----- PROTECTED REGION ID(VacuumValve.valveOpened_read) ENABLED START -----#
        self.e1.set()
        self.e2.wait(self.updateInterval)
        self.e2.clear()
        attr.set_value(self.attr_valveOpened_read)
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.valveOpened_read
        
    def read_valveLocked(self, attr):
        self.debug_stream("In read_valveLocked()")
        #----- PROTECTED REGION ID(VacuumValve.valveLocked_read) ENABLED START -----#
        self.e1.set()
        self.e2.wait(self.updateInterval)
        self.e2.clear()
        attr.set_value(self.attr_valveLocked_read)
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.valveLocked_read
        
    
    
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(VacuumValve.read_attr_hardware) ENABLED START -----#


        #----- PROTECTED REGION END -----#	//	VacuumValve.read_attr_hardware


    # -------------------------------------------------------------------------
    #    VacuumValve command methods
    # -------------------------------------------------------------------------
    

    #----- PROTECTED REGION ID(VacuumValve.programmer_methods) ENABLED START -----#
    def openValve(self):
        
        PyTango.DeviceProxy(self.valveOpenConfig[0]).write_attribute(self.valveOpenConfig[1],True)
        self.attr_openValve_read = True
        print("openValve")
        
    def closeValve(self):
        
        PyTango.DeviceProxy(self.valveOpenConfig[0]).write_attribute(self.valveOpenConfig[1],False)
        self.attr_openValve_read = False
        print("closeValve")

    def valveReadback(self):

        if self.valveType==1:
            
            self.attr_valveOpened_read = PyTango.DeviceProxy(self.valveReadbackConfig[0]).read_attribute(self.valveReadbackConfig[1]).value
            
        else:
        
            self.attr_valveOpened_read = self.attr_openValve_read
            
        if self.attr_valveOpened_read:
        
            self.set_state(PyTango.DevState.OPEN)
        
        else:
            self.set_state(PyTango.DevState.CLOSE)

    def IOMethod(self):

        while True:
            self.protection1 = False
            self.protection2 = False
            try:

                if len(self.protectionConfig)==4:

                    while True:
                        
                        self.e1.wait()
                        self.e1.clear()
                        self.valveReadback()
                    
                        protectionPointValue = PyTango.DeviceProxy(self.protectionConfig[0]).read_attribute(self.protectionConfig[1]).value
                        protectionPoint2Value = PyTango.DeviceProxy(self.protectionConfigIO2[0]).read_attribute(self.protectionConfigIO2[1]).value

                        #converting to floats for comparisons
                        protectionPointValueFloat = float(protectionPointValue)
                        protectionPoint2ValueFloat = float(protectionPoint2Value)

                        if str(self.protectionConfig[2]).lower()=="equals" or str(self.protectionConfigIO2[2]).lower()=="equals":

                            if protectionPointValue :
                                print("protecting1")
                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    self.protection1 = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.protection1 = False
                                if not self.protection2:
                                    self.attr_valveLocked_read = False

                        if str(self.protectionConfigIO2[2]).lower()=="equals":
                            

                            if protectionPoint2Value:
                                print("protecting2")
                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    self.protection2 = True
                                    print("locking BB")
                                    
                                    if self.protectionConfiguration2 == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration2 == 2:

                                        self.openValve()

                            else:
                                self.protection2 = False
                                if not self.protection1:
                                    self.attr_valveLocked_read = False
                        
                        if str(self.protectionConfig[2]).lower()=="notequals":

                            if not protectionPointValue:
                                print("protecting1")
                                if not self.attr_valveLocked_read:
                                    self.protection1 = True
                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.protection1 = False
                                if not self.protection2:
                                    self.attr_valveLocked_read = False

                        
                        
                        if str(self.protectionConfigIO2[2]).lower()=="notequals":

                            if not protectionPoint2Value:
                                
                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    self.protection2 = True

                                    if self.protectionConfiguration2 == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration2 == 2:

                                        self.openValve()

                            else:

                                self.protection2 = False
                                if not self.protection1:
                                    self.attr_valveLocked_read = False         

                        if str(self.protectionConfig[2]).lower()=="above":

                            if protectionPointValueFloat > float(self.protectionConfig[3]):

                                print("protecting1")
                                if not self.attr_valveLocked_read:
                                    self.protection1 = True
                                

                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.protection1 = False
                                if not self.protection2:
                                    self.attr_valveLocked_read = False

                        if str(self.protectionConfigIO2[2]).lower()=="above":

                            if protectionPoint2ValueFloat > float(self.protectionConfigIO2[3]):

                                print("protecting2")
                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    self.protection2 = True

                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration2 == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration2 == 2:

                                        self.openValve()

                            else:
                                
                                self.protection2 = False
                                if not self.protection1:
                                    self.attr_valveLocked_read = False  

                        if str(self.protectionConfig[2]).lower()=="below":

                            if protectionPointValueFloat < float(self.protectionConfig[3]):

                                print("protecting1")
                                if not self.attr_valveLocked_read:

                                    self.protection1 = True
                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.protection1 = False
                                if not self.protection2:
                                    self.attr_valveLocked_read = False


                        if str(self.protectionConfigIO2[2]).lower()=="below":

                            if protectionPoint2ValueFloat < float(self.protectionConfigIO2[3]):

                                print("protecting2")
                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    print("locking A")
                                    self.protection2 = True
                                    print(self.protectionConfiguration2)

                                    #removing these 3 because why repeated? this isn't going into executing below
                                    #self.attr_valveLocked_read = True
                                    #self.protection2 = True

                                    #self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration2 == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration2 == 2:

                                        self.openValve()

                            else:

                                self.protection2 = False
                                if not self.protection1:
                                    self.attr_valveLocked_read = False  

                        if self.queueAction:
                            
                            self.queueAction = False

                            if self.queueActionData:
                                self.openValve()
                            
                            else:
                                self.closeValve()

                        self.e2.set()
                                

                elif len(self.protectionConfig)==5:

                    while True:

                        self.e1.wait()
                        self.e1.clear()

                        protectionPointValue = PyTango.DeviceProxy(self.protectionConfig[0]).read_attribute(self.protectionConfig[1]).value
                        comparisonPointValue = PyTango.DeviceProxy(self.protectionConfig[3]).read_attribute(self.protectionConfig[4]).value

                        #float conversions
                        protectionPointValueFloat = float(protectionPointValue)
                        comparisonPointValueFloat = float(comparisonPointValue)

                        print("len=5, protect, comp")
                        print(protectionPointValueFloat)
                        print(comparisonPointValueFloat)
                        
                        # TODO: this is a hack to deal with a slightly miscalculated valve.

                        comparisonPointValueFloat = .8 * comparisonPointValueFloat 

                        self.valveReadback()

                        if str(self.protectionConfig[2]).lower()=="equals":

                            if protectionPointValue == comparisonPointValue:

                                self.attr_valveLocked_read = True
                                
                                if self.protectionConfiguration == 1:

                                    self.closeValve()

                                elif self.protectionConfiguration == 2:

                                    self.openValve()

                            else:

                                self.attr_valveLocked_read = False
                        
                        if str(self.protectionConfig[2]).lower()=="notequals":

                            if not protectionPointValue == comparisonPointValue:

                                self.attr_valveLocked_read = True
                                
                                if self.protectionConfiguration == 1:

                                    self.closeValve()

                                elif self.protectionConfiguration == 2:

                                    self.openValve()

                            else:

                                self.attr_valveLocked_read = False                

                        if str(self.protectionConfig[2]).lower()=="above":
                            print("above test")
                            
                            # added in an "if both gauges are below 1 Torr don't give a shit"
                            if protectionPointValueFloat > comparisonPointValueFloat and (protectionPointValueFloat >.5 or comparisonPointValueFloat>.5):
                                print("above protecting1")
                                
                                if not self.attr_valveLocked_read :

                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.attr_valveLocked_read = False

                        if str(self.protectionConfig[2]).lower()=="below":

                            if protectionPointValueFloat <  comparisonPointValueFloat:

                                if not self.attr_valveLocked_read:

                                    self.attr_valveLocked_read = True
                                    
                                    if self.protectionConfiguration == 1:

                                        self.closeValve()

                                    elif self.protectionConfiguration == 2:

                                        self.openValve()

                            else:

                                self.attr_valveLocked_read = False
                        
                        if self.queueAction:
                            
                            self.queueAction = False
                            
                            if self.queueActionData:
                                self.openValve()
                            
                            else:
                                self.closeValve()

                        self.e2.set()

                else:

                    while True:

                        
                        self.e1.wait()
                        self.e1.clear()

                        self.valveReadback()

                        if self.queueAction:
                            
                            self.queueAction = False
                            
                            if self.queueActionData:
                                self.openValve()
                            
                            else:
                                self.closeValve()

                        self.e2.set()

            except:

                print('Device failure at ' + str(datetime.now()))
                traceback.print_exc()
                
                self.set_state(PyTango.DevState.FAULT)
                time.sleep(5)

                
        
        
        
    #----- PROTECTED REGION END -----#	//	VacuumValve.programmer_methods

class VacuumValveClass(PyTango.DeviceClass):
    # -------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(VacuumValve.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	VacuumValve.global_class_variables


    #    Class Properties
    class_property_list = {
        }


    #    Device Properties
    device_property_list = {
        'valveOpenConfig':
            [PyTango.DevVarStringArray, 
            "This defines the io point being pointed to.\n\nFormat:\n\n[\ndeviceProxy\nattribute\n]",
            [] ],
        'valveType':
            [PyTango.DevLong, 
            "0 - Feedbackless valve\n1 - Valve with readback",
            [] ],
        'valveReadbackConfig':
            [PyTango.DevVarStringArray, 
            "This defines the io point being pointed to.\n\nFormat:\n\n[\ndeviceProxy\nattribute\n]",
            [] ],
        'protectionConfiguration':
            [PyTango.DevLong, 
            "Defines the protection scheme.\n\n0 or does not exist - No automatic protection.\n1 - Automatically closes based on criteria.\n2 - Automatically opens based on criteria.",
            [0]],
        'protectionConfig':
            [PyTango.DevVarStringArray, 
            "Defines the protection control point. Overloaded.\n[\nTango Device\nTango Attribute\nEquals/NotEquals/Above/Below (Defines Trigger Behavior)\nValue (if above/below) OR (if overloaded!) comparison device\ncomparison attribute\n]",
            [] ],
        'protectionConfiguration2':
            [PyTango.DevLong, 
            "Defines the protection scheme.\n\n0 or does not exist - No automatic protection.\n1 - Automatically closes based on criteria.\n2 - Automatically opens based on criteria.",
            [0]],
        'protectionConfigIO2':
            [PyTango.DevVarStringArray, 
            "Defines the protection control point. Overloaded.\n[\nTango Device\nTango Attribute\nEquals/NotEquals/Above/Below (Defines Trigger Behavior)\nValue (if above/below) OR (if overloaded!) comparison device\ncomparison attribute\n]",
            [] ],
        }


    #    Command definitions
    cmd_list = {
        }


    #    Attribute definitions
    attr_list = {
        'openValve':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'valveOpened':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ]],
        'valveLocked':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'description': "valveLocked defines that the valve is under automatic protection mode and cannot be changed.",
            } ],
        }


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(VacuumValveClass, VacuumValve, 'VacuumValve')
        #----- PROTECTED REGION ID(VacuumValve.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	VacuumValve.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print ('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print ('-------> An unforeseen exception occured....', e)

if __name__ == '__main__':
    main()
