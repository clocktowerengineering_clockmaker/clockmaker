import serial
import time
from datetime import datetime

class Wenglor():

    defaultBaud = 38400
    defaultPortID = "/dev/ttyUBS10"
    defaultTimeout = .1
    defaultModel = "OCP352H0180"
    #defaultModel = "CP25QXVT80"

    def __init__(self, model = defaultModel):
        self.model = model
        self.WenglorSerial = None
        print(str(datetime.now()) + ': Initializing Wenglor Sensor')
        

    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout,parity=serial.PARITY_NONE):
        try:
            self.WenglorSerial = serial.Serial()
            self.WenglorSerial.timeout=timeout
            self.WenglorSerial.port=serialPort
            self.WenglorSerial.baudrate=baud
            self.WenglorSerial.parity=parity
            self.WenglorSerial.open()
        except serial.SerialException as e:
            print(f"Error opening serial port: {e}")
        else:
            print(f"Serial port {serialPort} opened successfully.")


    def StopSerial(self):
        if self.WenglorSerial and self.WenglorSerial.is_open:
            self.WenglorSerial.close()
            print("Serial port closed.")


    def getObjectDistanceBinary(self):
        self.WenglorSerial.write('#'.encode())
        binaryresponse = self.WenglorSerial.read_all()
        # binary response is a 3 byte value, # + 2 bytes for response in 10um
        return int.from_bytes(binaryresponse[1:2],"big")

    def getObjectDistance(self):
        if self.model == "CP25QXVT80":
            return self._get_ocp352h0180_distance()
        elif self.model == "OCP352H0180":
            return self._get_cp25qxvt80_distance()
        else:
            raise ValueError("Unsupported sensor model")


    def _get_cp25qxvt80_distance(self):
        self.WenglorSerial.write('/020D0e0C.'.encode())
        time.sleep(0.5)  # Add a delay to allow the sensor to process
        response = self.WenglorSerial.read_until(b".")
        print(f"CP25QXVT80 response: {response}")  # Log the response for debugging
        if len(response) != 14:
            return 0
        um = int(response[5:10].decode())
        return um * 10


    def _get_ocp352h0180_distance(self):
        self.WenglorSerial.write('/020D0059.'.encode())
        time.sleep(0.005)   # Add a delay to allow the sensor to process
        response = self.WenglorSerial.read_until(".".encode())
        # check that the response is the correct length
        print(f"CP25QXVT80 response: {response}")  # Log the response for debugging
        if len(response)!=19:
            print("length issue, length is "+ str(len(response)))
            um = 0
        else:
            um = int(response[8:14].decode())

        return um

    def setFilter(self,samples=0):
        self.WenglorSerial.write(f'/030FS{samples:02d}qq.'.encode())
        response = self.WenglorSerial.read_until(b".")
        response = response[1:-3].decode()
        if response == '030MF00':
            return 'no filter'
        if response == f'030MF{samples:02d}':
            return f'Set filter to {samples} samples'
        return 'Unable to set filter'

        #TODO: need to implement setFilter. Or not!
        pass

    def setLaserEnabled(self, command = True):

        if command:
            self.WenglorSerial.write('/020L0150.'.encode())
        
        else:
            self.WenglorSerial.write('/020L0151.'.encode())

        response = self.WenglorSerial.read_until(".".encode())
        time.sleep(1)
        response = self.WenglorSerial.read_until(b".")
        response = response[1:-1].decode()
        if response == '020L0150':
            return 'Laser light Activated'
        elif response == '020L0051':
            return 'Laser light Deactivated'
        else:
            return 'Unable to change Laser Light Status'

        # TODO: probably should parse this properly or some shit

        return command

    def setLaserDisabled(self):

        # alias method
        self.setLaserEnabled(False)

    def setExposureMode(self,mode = 0):

        # mode list = 0 = "DCM", 1 = "SCM", 2 = "LCM". not one of those? Do nothing and respond -1 instead
        mode_commands = {
                0: b'/020eCD7F.',
                1: b'/020eCL77.',
                2: b'/020eCS68.'
            }
        if mode not in mode_commands:
            return -1
        self.WenglorSerial.write(mode_commands[mode])
        time.sleep(1)
        response = self.WenglorSerial.read_until(b".")
        response = response[1:-1].decode()
        mode_responses = {
            '030MeCD33': 'DCM-Mode set',
            '030MeCL3B': 'LCM-Mode Set',
            '030MeCS24': 'SCM-Mode set'
        }
        return mode_responses.get(response, "Unable to change Exposure Mode")

        response = self.WenglorSerial.read_until(".".encode())
        # TODO: probably should parse this properly or some shit
        return mode 

    def setExposureTime(self, ex_time = 8000):
        if self.model != 'CP25QXVT80':
            raise ValueError("Exposure mode not supported for this model")
        self.WenglorSerial.write(f'/060cr{ex_time:05d}qq.'.encode())
        response = self.WenglorSerial.read_until(b".")
        time.sleep(1)
        response = response.decode()
        return f"Exposure time set to {ex_time}"
    

    def setHighSpeedModeEnabled(self, mode = True):
        command = b'/020er13B.' if mode else b'/020er238.'
        self.WenglorSerial.write(command)
        time.sleep(1)
        response = self.WenglorSerial.read_until(b".")
        response = response[1:-1].decode()
        mode_responses = {
            '030Mer177': 'Speed-Mode set',
            '030Mer274': 'Resolution-Mode set'
        }
        return mode_responses.get(response, 'Unable to change Mode')
 

    def setHighSpeedModeDisabled(self):
        return self.setHighSpeedModeEnabled(False) 
