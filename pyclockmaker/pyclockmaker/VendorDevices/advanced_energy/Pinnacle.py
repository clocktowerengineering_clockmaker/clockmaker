
from pyclockmaker.VendorDevices.advanced_energy.AESerial import AESerial


class Pinnacle(AESerial):

      
    defaultBaud = 19200
    defaultTimeout = .1
    defaultPortID = '/dev/tty_Extraction_Pulser'
    defaultAddress = 1
    
    # parameter dictionary

    # sets for DC Pinnacle
    param_set_DC_off = 1  
    param_set_DC_on  = 2
    param_set_regulation_select = 3 # values to pass 6 =  power, 7 = voltage, 8 = current)
    param_set_joule_setpoint = 4 # 0 to 9,999,999 in joules
    param_set_joule_mode_enable = 5 # 0 = disable, other = enable
    param_set_setpoint = 6 # 16 bit value for setpoint of regulation mode in W, 1/100ths of amp, or V
    param_set_arc_detect_shutdown_time = 8 # sets amount of time constituting an arc and amount of time to shutdown the supply
    param_set_hard_arc_count_limit = 9 # sets number of hard arcs to trigger the arc bit
    param_set_voltage_arc_trip_level = 10 #specifies voltage level for arc triggering (16 bit)
    param_set_active_target = 11 # values 1 to 8
    param_set_target_life = 12 # target life, byte 0 is target number, 1-4 is target life in kwh, 1kwh = 100
    param_set_target_enable = 13 # 0 = disable, other = enable
    param_set_control_mode = 14 # select control, 2 = rs232, 4 = analog
    param_set_ramp_enable = 15 # 0 = disable, other = enable
    param_set_ramp_time = 16 # ramp time in 10mS increments, min 50mS max 600 sec (16 bit)
    param_set_program_source = 17 # sets where the program source is coming from, see datasheet
    param_set_side_enable = 18 # not implemented.
    param_set_number_of_recipe_steps = 19 # sets number of recipe steps from 0 to 8. 0 disables recipes.
    param_set_recipe_step_regulation_mode = 20 # sets recipe regulation mode for that step, 1st byte is step # 2nd is type (see param_set_regulation_select)
    param_set_recipe_step_ramp_time = 21 # sets ramp time, 1st byte is step # 2nd byte is ramptime in ms (see param_set_ramp_time) 
    param_set_recipe_step_setpoint = 22 # byte 0 is step; byte 1 and 2 is setpoint, lsb first
    param_set_recipe_step_run_time = 23  # byte 0 is step; byte 1 and 2 is runtime in hundredths of seconds, lsb first
    param_set_joule_threshold = 26 # byte 0 is step, 1 thru 7; byte 1 and 2 is runtime in hundredths of seconds or joules, lsb first
    param_set_arc_handling_type= 30 # 0 = 50µS, other = 500µS. NOT USED FOR PINNACLE PLUS+. Not implemented.
    param_set_out_of_setpoint_timer = 31 # timeout in seconds between 1 and 600, 16 bit
    param_set_ramp_start_point = 32 # sets the start point of a ramp at % of setpoint when first turning on, range of 0 to 95
    param_set_overvoltage_timer = 34 # sets how long overvoltage can be available in 1/10ths of seconds, 16 bits. NOT USED FOR PINNACLE.
    param_set_AEbus_watchdog_timer = 39 # not super useful but maybe it's an issue? it's the serial watchdog
    param_set_host_port_timeout_value = 40 # host port timeout
    param_set_process_voltage_limit_enable = 47 # 0 = disable, other = enable
    param_set_process_voltage_lower_limit = 48 # sets the minimum voltage in V. If voltage falls below this, supply turns off and errors out. 16 bits. 
    param_set_user_power_limit = 49 # sets the maximum power limit in W. 16 bits.
    param_set_user_voltage_limit = 50 # sets the maximum voltage limit in V. 16 bits.
    param_set_user_current_limit = 51 # sets the maximum current limit in 1/100ths of A. 16 bits.
    param_set_user_strike_voltage_limit = 52 # sets the strike voltage behavior. 0 = low, 1 = medium, 2 = high. Might be able to be used for playing with ripple and boosting during on-phase.
    param_set_arc_handling_enable = 61 # 0 = disable, other = enable
    param_set_TCC_enable = 79 # 0 = disable, other = enable. Can be used for target/grid conditioning. when enabled, if supply is turned on, always goes through grid conditioning. In IBAD should never be turned on except for cleaning cycle.
    param_set_TCC_timer = 80 # off time in seconds that doesn't require TCC on enable. For IBAD applications, this can be ignored.
    param_set_master_reset = 119 # clears fault indications when a non-recoverable (explicit clear) fault occurs.
    param_set_reset_defaults = 126 # resets all user-defined values to defaults and saves. THIS IS THE BIG ONE, DO NOT DO THIS LIGHTLY.

    # reads and gets for DC Pinnacle
    param_read_supply_type = 128 # not implemented
    param_read_supply_size = 129 # not implemented
    param_read_maximum_limits = 130 # not implemented, reads power, voltage, current limits (6 bytes)
    param_read_joule_threshold = 136 # reads joule threshold
    param_read_global_enable = 137 # not implemented
    param_read_AEbus_watchdog_timer_value = 139 # not implemented, PINNACLE ONLY
    param_read_host_timeout_value = 140 # not implemented
    param_read_user_power_limit = 141
    param_read_user_voltage_limit = 142
    param_read_user_current_limit = 143
    param_read_user_strike_voltage_limit = 144
    param_read_ramp_start_point = 152
    param_read_joule_mode_status = 153
    param_read_regulation_mode = 154
    param_read_control_method = 155
    param_read_active_target = 156
    param_read_target_life = 157
    param_read_ramp_time = 158
    param_read_ramp_time_remaining = 159 # how much ramp time is left in 1/100ths of a sec
    param_read_status = 160 # gets logic board values, not implemented
    param_read_output_on_status = 161 # kind of a shitty version of 162, not implemented, just use 162.
    param_read_process_status = 162 # the big one! this gets pretty much all current process values and supply statuses.
    param_read_config_status = 163 # gets a lot of configuration values in one shot.
    param_read_setpoint_regulation_mode = 164
    param_read_actual_power = 165 # not implemented in favor of 168
    param_read_actual_voltage = 166 # not implemented in favor of 168
    param_read_actual_current = 167 # not implemented in favor of 168
    param_read_actual_PVI = 168 # gets actual power, voltage, current in one command
    param_read_PVI_setpoint = 169 # gets setpoints for power, voltage, current in one command
    param_read_arc_detect_shutdown_time = 170 # gets settings for param_set_arc_detect_shutdown_time.
    param_read_voltage_arc_trip_level = 171 # gets settings for param_set_voltage_arc_trip_level.
    param_read_joules_remaining = 172
    param_read_joules_setpoint = 173
    param_read_target_type = 174
    param_read_time_output_on = 175 # first byte = hours, 2nd byte = minutes, 3rd byte = seconds
    param_read_process_voltage_limit = 177
    param_read_hard_arc_count_limit = 178
    param_read_active_recipe_status = 179 # whole bunch of things, not implemented
    param_read_recipe_steps = 180 # how many steps? not implemented
    param_read_recipe_step_ramp_time = 181 # send step #, get ramp time. Not implemented
    param_read_recipe_step_setpoint = 182 #not implemented
    param_read_recipe_step_runtime = 183 #not implemented
    param_read_out_of_setpoint_interval = 184 # time remaining on the out of setpoint timer
    param_read_TCC_timer = 185 # not implemented
    param_read_out_of_setpoint_timer = 187 # reads param_set_out_of_setpoint_timer
    param_read_arc_density_last_second = 188 # gets # microarcs/hardarcs the last second, 2 bytes each
    param_read_microarc_density = 189 # gets microarcs from last run
    param_read_fault_list = 208

    # defines
    max_power = 6000
    max_current = 2000
    max_voltage = 600
    power_regulation = 6
    voltage_regulation = 7
    current_regulation = 8
    host_control_mode = 2
    user_control_mode = 4

    # process dict

    processdictionary = dict()

    def DCOff(self):

        sendresponse = self.Send(self.param_set_DC_off,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def DCOn(self):

        sendresponse = self.Send(self.param_set_DC_on,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def RegulationSelect(self,value=voltage_regulation):

        value = self.BoundInput(value,self.power_regulation,self.current_regulation)
        sendresponse = self.Send(self.param_set_regulation_select,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated
    
    def PowerLimit(self,value):

        value = self.BoundInput(value,0,self.max_power)
        sendresponse = self.Send(self.param_set_user_power_limit,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated
        
    def VoltageLimit(self,value):

        value = self.BoundInput(value,0,self.max_voltage)
        sendresponse = self.Send(self.param_set_user_voltage_limit,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def CurrentLimit(self,value):

        value = self.BoundInput(value,0,self.max_current)
        sendresponse = self.Send(self.param_set_user_current_limit,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def Setpoint(self,value):

        value = self.BoundInput(value,0,10000)
        sendresponse = self.Send(self.param_set_setpoint,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated
        
    def ArcDetectShutdownTime(self,value):

        value = self.BoundInput(value,0,255)
        sendresponse = self.Send(self.param_set_arc_detect_shutdown_time,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def HardArcCountLimit(self,value):

        value = self.BoundInput(value,0,255)
        sendresponse = self.Send(self.param_set_hard_arc_count_limit,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def VoltageArcTripLevel(self,value):
        value = self.BoundInput(value,0,10000)
        sendresponse = self.Send(self.param_set_hard_arc_count_limit,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def ControlMode(self,value=host_control_mode):

        value = self.BoundInput(value,0,10)
        sendresponse = self.Send(self.param_set_control_mode,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def OutOfSetpointTimer(self,value):

        value = self.BoundInput(value,0,9999)
        sendresponse = self.Send(self.param_set_out_of_setpoint_timer,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def OvervoltageTimer(self,value):

        value = self.BoundInput(value,0,9999)
        sendresponse = self.Send(self.param_set_overvoltage_timer,self.long_to_bytes(value,2),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def StrikeVoltageLimit(self,value):

        value = self.BoundInput(value,0,3)
        sendresponse = self.Send(self.param_set_user_strike_voltage_limit,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated
    
    def EnableTCCMode(self,value):

        value = self.BoundInput(value,0,3)
        sendresponse = self.Send(self.param_set_TCC_enable,self.long_to_bytes(value,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def MasterReset(self):

        sendresponse = self.Send(self.param_set_master_reset,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def ReadCurrentLimit(self):

        sendresponse = self.Send(self.param_read_user_current_limit,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def ReadVoltageLimit(self):

        sendresponse = self.Send(self.param_read_user_voltage_limit,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def ReadProcessStatus(self):

        sendresponse = self.Send(self.param_read_process_status,[],self.defaultAddress)
        print(sendresponse)
        translated = sendresponse.hex()
        mappedprocesses, faultactive = self.TranslateProcessStatus(translated)
        return translated, mappedprocesses, faultactive


    def ReadPVI(self):

        sendresponse = self.Send(self.param_read_actual_PVI,[],self.defaultAddress)
        translated = sendresponse.hex()
        power = self.hex_to_long(translated[0:4])
        voltage = self.hex_to_long(translated[4:8])
        current = self.hex_to_long(translated[8:12])/100
        return translated, power, voltage, current

    def ReadPVISetpoints(self):

        sendresponse = self.Send(self.param_read_PVI_setpoint,[],self.defaultAddress)
        translated = sendresponse.hex()
        power = self.hex_to_long(translated[0:4])
        voltage = self.hex_to_long(translated[4:8])
        current = self.hex_to_long(translated[8:12])/100
        return translated, power, voltage, current

    def ReadArcDensity(self):

        sendresponse = self.Send(self.param_read_arc_density_last_second,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse[2:].hex())
        return translated

    def ReadMicroArcDensity(self):

        sendresponse = self.Send(self.param_read_microarc_density,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def ReadFaultList(self):

       
        sendresponse = self.Send(self.param_read_fault_list,[0],self.defaultAddress)
        hexsendresponse = sendresponse.hex()
        if len(hexsendresponse)==0:

            return 0

        elif len(hexsendresponse)==4:

            if hexsendresponse == '1600':
                
                # contactor is open, that gets caught properly at the process status word
                return 0

            else:

                return self.hex_to_long(hexsendresponse[0:3]), hexsendresponse
        
        else:

            return self.hex_to_long(hexsendresponse[0:3]), hexsendresponse





        



    def TranslateProcessStatus(self,value):

        # translates the process statuses. Let's go!!!!
        # specific to Pinnacle Plus +. Won't be exactly right for the older units.
        
        # grab each byte
        firstbyte = int(value[0:1],16)
        
        secondbyte = int(value[2:3],16)
        
        thirdbyte = int(value[4:5],16)
        
        fourthbyte = int(value[6:7],16)
        
        faultactive = False

        if firstbyte & 2:
            self.processdictionary['RampActive'] = True
        else:
            self.processdictionary['RampActive'] = False

        if firstbyte & 4:
            self.processdictionary['RunActive'] = True
        else:
            self.processdictionary['RunActive'] = False

        if firstbyte & 8:
            self.processdictionary['OutputPowerOn'] = False
        else:
            self.processdictionary['OutputPowerOn'] = True

        if firstbyte & 16:
            self.processdictionary['IllegalRegulationMode'] = True
            faultactive = True
        else:
            self.processdictionary['IllegalRegulationMode'] = False

        if firstbyte & 32:
            self.processdictionary['CableInterlock'] = True
        else:
            self.processdictionary['CableInterlock'] = False

        if firstbyte & 64:
            self.processdictionary['EndOfTargetLife'] = True
        else:
            self.processdictionary['EndOfTargetLife'] = False

        if firstbyte & 128:
            self.processdictionary['SetpointOutOfTolerance'] = True
            
        else:
            self.processdictionary['SetpointOutOfTolerance'] = False

        if secondbyte & 1:
            self.processdictionary['OverVoltage'] = True
        else:
            self.processdictionary['OverVoltage'] = False

        if secondbyte & 2:
            self.processdictionary['OverCurrent'] = True
        else:
            self.processdictionary['OverCurrent'] = False

        if secondbyte & 8:
            self.processdictionary['ControlCircuitFault'] = True
            faultactive = True
        else:
            self.processdictionary['ControlCircuitFault'] = False

        if secondbyte & 16:
            self.processdictionary['HeatsinkOvertemperature'] = True
            faultactive = True
        else:
            self.processdictionary['HeatsinkOvertemperature'] = False

        if secondbyte & 32:
            self.processdictionary['UserInterlockOpen'] = True
        else:
            self.processdictionary['UserInterlockOpen'] = False

        if secondbyte & 64:
            self.processdictionary['UserPortResetActive'] = True
        else:
            self.processdictionary['UserPortResetActive'] = False

        if secondbyte & 128:
            self.processdictionary['MainContactorInterlockOpen'] = True
        else:
            self.processdictionary['MainContactorInterlockOpen'] = False

        if thirdbyte & 1:
            self.processdictionary['PowerFault'] = True
            faultactive = True
        else:
            self.processdictionary['PowerFault'] = False

        if thirdbyte & 4:
            self.processdictionary['NearEndJoule'] = True
        else:
            self.processdictionary['NearEndJoule'] = False

        if thirdbyte & 8:
            self.processdictionary['JouleThresholdReached'] = True
        else:
            self.processdictionary['JouleThresholdReached'] = False

        if thirdbyte & 16:
            self.processdictionary['UnitAInverterLow'] = True
        else:
            self.processdictionary['UnitAInverterLow'] = False

        if thirdbyte & 32:
            self.processdictionary['UnitBInverterLow'] = True
        else:
            self.processdictionary['UnitBInverterLow'] = False

        if thirdbyte & 128:
            self.processdictionary['PROFIBUSError'] = True
            faultactive = True
        else:
            self.processdictionary['PROFIBUSError'] = False

        if fourthbyte & 1:
            self.processdictionary['BusFault'] = True
            faultactive = True
        else:
            self.processdictionary['BusFault'] = False

        if fourthbyte & 2:
            self.processdictionary['FaultActive'] = True
            faultactive = True
        else:
            self.processdictionary['FaultActive'] = False
            
        if fourthbyte & 4:
            self.processdictionary['JoulesReached'] = True
        else:
            self.processdictionary['JoulesReached'] = False


        if fourthbyte & 16:
            self.processdictionary['OutOfSetpointTimerExpired'] = True
        else:
            self.processdictionary['OutOfSetpointTimerExpired'] = False

        if fourthbyte & 32:
            self.processdictionary['GroundFaultDetected'] = True
            faultactive = True
        else:
            self.processdictionary['GroundFaultDetected'] = False

        if fourthbyte & 64:
            self.processdictionary['ShortCircuitFaultDetected'] = True
            faultactive = True
        else:
            self.processdictionary['ShortCircuitFaultDetected'] = False

        if fourthbyte & 128:
            self.processdictionary['FatalShortCircuitDetected'] = True
            faultactive = True
        else:
            self.processdictionary['FatalShortCircuitDetected'] = False

        return self.processdictionary, faultactive


    def ReadConfigStatus(self):
        
        sendresponse = self.Send(self.param_read_config_status,[],self.defaultAddress)
        translated = sendresponse.hex()
        mappedconfigs = self.TranslateConfigStatus(translated)
        return translated, mappedconfigs
        

    def TranslateConfigStatus(self,value):

        firstbyte = int(value[0:1],16)
        secondbyte = int(value[2:3],16)
        thirdbyte = int(value[4:5],16)
        fourthbyte = int(value[6:7],16)


        if firstbyte & 1:
            self.configdictionary['HostExternalProgramSource'] = True
        else:
            self.configdictionary['HostExternalProgramSource'] = False

        if firstbyte & 2:
            self.configdictionary['LocalProgramSourceExternal'] = True
        else:
            self.configdictionary['LocalProgramSourceExternal'] = False

        if firstbyte & 4:
            self.configdictionary['UserProgramSourceExternal'] = True
        else:
            self.configdictionary['UserProgramSourceExternal'] = False

        if firstbyte & 8:
            self.configdictionary['MainUnit'] = True
        else:
            self.configdictionary['MainUnit'] = False

        if firstbyte & 16:
            self.configdictionary['SatelliteUnit'] = True
        else:
            self.configdictionary['SatelliteUnit'] = False

        if firstbyte & 32:
            self.configdictionary['DualUnitEnabled'] = True
        else:
            self.configdictionary['DualUnitEnabled'] = False

        if firstbyte & 64:
            self.configdictionary['ProgramSourceExternal'] = True
        else:
            self.configdictionary['ProgramSourceExternal'] = False

        if firstbyte & 128:
            self.configdictionary['TargetLifeActive'] = True
        else:
            self.configdictionary['TargetLifeActive'] = False

        if secondbyte & 1:
            self.configdictionary['JouleRecipe'] = True
        else:
            self.configdictionary['JouleRecipe'] = False

        if secondbyte & 2:
            self.configdictionary['ControlPanelAccess'] = True
        else:
            self.configdictionary['ControlPanelAccess'] = False

        if secondbyte & 4:
            self.configdictionary['JouleMode'] = True
        else:
            self.configdictionary['JouleMode'] = False

        if secondbyte & 8:
            self.configdictionary['InterlockOK'] = True
        else:
            self.configdictionary['InterlockOK'] = False

        if secondbyte & 32:
            self.configdictionary['RampModeEnabled'] = True
        else:
            self.configdictionary['RampModeEnabled'] = False

        if secondbyte & 128:
            self.configdictionary['PulsingEnabled'] = True
        else:
            self.configdictionary['PulsingEnabled'] = False


        if thirdbyte & 1:
            self.configdictionary['SyncPulseTransmitter'] = True
        else:
            self.configdictionary['SyncPulseTransmitter'] = False

        if thirdbyte & 2:
            self.configdictionary['OvervoltageTimerActive'] = True
        else:
            self.configdictionary['OvervoltageTimerActive'] = False

        if thirdbyte & 4:
            self.configdictionary['OvercurrentTimerActive'] = True
        else:
            self.configdictionary['OvercurrentTimerActive'] = False

        if thirdbyte & 8:
            self.configdictionary['ConstantDutyCycle'] = True
        else:
            self.configdictionary['ConstantDutyCycle'] = False

        if thirdbyte & 16:
            self.configdictionary['MicroArcHandling'] = True
        else:
            self.configdictionary['MicroArcHandling'] = False

        if thirdbyte & 32:
            self.configdictionary['PulsingMode'] = 'Always'
        else:
            self.configdictionary['PulsingMode'] = 'CurrentThreshold'

        if not (thirdbyte & 64) and not(thirdbyte & 128):
            self.configdictionary['JouleThreshold'] = 'Power'
        elif  (thirdbyte & 64) and not(thirdbyte & 128):
            self.configdictionary['JouleThreshold'] = 'Voltage'
        elif  not(thirdbyte & 64) and (thirdbyte & 128):
            self.configdictionary['JouleThreshold'] = 'Current'

        if fourthbyte & 1:
            self.configdictionary['HardArcBroadcast'] = True
        else:
            self.configdictionary['HardArcBroadcast'] = False

        if fourthbyte & 2:
            self.configdictionary['JouleCountdownHold'] = True
        else:
            self.configdictionary['JouleCountdownHold'] = False

        return self.configdictionary
