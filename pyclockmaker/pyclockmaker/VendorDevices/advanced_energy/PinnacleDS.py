#!/usr/bin/env python
# -*- coding:utf-8 -*-


# ############################################################################
#  license :
# ============================================================================
#
#  File :        PinnacleDS.py
#
#  Project :     
#
# This file is part of Tango device class.
# 
# Tango is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Tango is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Tango.  If not, see <http://www.gnu.org/licenses/>.
# 
#
#  $Author :      mark.amato$
#
#  $Revision :    $
#
#  $Date :        $
#
#  $HeadUrl :     $
# ============================================================================
#            This file is generated by POGO
#     (Program Obviously used to Generate tango Object)
# ############################################################################

__all__ = ["PinnacleDS", "PinnacleDSClass", "main"]

__docformat__ = 'restructuredtext'

from inspect import trace
import PyTango
import sys
# Add additional import
#----- PROTECTED REGION ID(PinnacleDS.additionnal_import) ENABLED START -----#
import threading

from pyclockmaker.VendorDevices.advanced_energy import Pinnacle
import traceback
import time
from datetime import datetime
#----- PROTECTED REGION END -----#	//	PinnacleDS.additionnal_import

# Device States Description
# ALARM : 
# DISABLE : 
# FAULT : Unit has a fault raised.
# OFF : 
# ON : Device Server Operational.


class PinnacleDS (PyTango.LatestDeviceImpl):
    """This class controls a Pinnacle, Pinnacle Plus, or Pinnacle Plus + unit"""
    
    # -------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(PinnacleDS.global_variables) ENABLED START -----#
    needsinit = True
    queueSetVoltage = False
    queueEnable = False
    queueVoltage = 0
    queueDisable = True
    queueReset = False
    queueSetReverseTime = False
    queueReverseTime = 0 
    queueSetDutyCycle = False
    queueDutyCycle  = False
    queueSetPulseFrequency = False
    queuePulseFrequency = False
    queueSend = False
    queueSendData = ''
    processStatusDict = {}

    #----- PROTECTED REGION END -----#	//	PinnacleDS.global_variables

    def __init__(self, cl, name):
        PyTango.LatestDeviceImpl.__init__(self,cl,name)
        self.debug_stream("In __init__()")
        PinnacleDS.init_device(self)
        #----- PROTECTED REGION ID(PinnacleDS.__init__) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(PinnacleDS.delete_device) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_arcCount_read = 0.0
        self.attr_arcRate_read = 0.0
        self.attr_currentReadback_read = 0.0
        self.attr_demandSetpoint_read = 0.0
        self.attr_dutyCycle_read = 0.0
        self.attr_enable_read = False
        self.attr_enabled_read = False
        self.attr_warningStatus_read = ""
        self.attr_faultStatus_read = ""
        self.attr_interlockActive_read = False
        self.attr_powerReadback_read = 0.0
        self.attr_processStatus_read = ""
        self.attr_pulseFrequency_read = 0.0
        self.attr_reverseTime_read = 0.0
        self.attr_voltageReadback_read = 0.0
        self.attr_currentLimit_read = 0.0
        self.attr_dutyCycleLimits_read = ""
        #----- PROTECTED REGION ID(PinnacleDS.init_device) ENABLED START -----#
        self.set_state(PyTango.DevState.INIT)
        
        self.ioThread = threading.Thread(target = self.IOMethod)
        self.ioThread.setDaemon(True)
        self.ioThread.start() 
        #----- PROTECTED REGION END -----#	//	PinnacleDS.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(PinnacleDS.always_executed_hook) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.always_executed_hook

    # -------------------------------------------------------------------------
    #    PinnacleDS read/write attribute methods
    # -------------------------------------------------------------------------
    
    def read_arcCount(self, attr):
        self.debug_stream("In read_arcCount()")
        #----- PROTECTED REGION ID(PinnacleDS.arcCount_read) ENABLED START -----#
        attr.set_value(self.attr_arcCount_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.arcCount_read
        
    def read_arcRate(self, attr):
        self.debug_stream("In read_arcRate()")
        #----- PROTECTED REGION ID(PinnacleDS.arcRate_read) ENABLED START -----#
        attr.set_value(self.attr_arcRate_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.arcRate_read
        
    def read_currentReadback(self, attr):
        self.debug_stream("In read_currentReadback()")
        #----- PROTECTED REGION ID(PinnacleDS.currentReadback_read) ENABLED START -----#
        attr.set_value(self.attr_currentReadback_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.currentReadback_read
        
    def read_demandSetpoint(self, attr):
        self.debug_stream("In read_demandSetpoint()")
        #----- PROTECTED REGION ID(PinnacleDS.demandSetpoint_read) ENABLED START -----#
        attr.set_value(self.attr_demandSetpoint_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.demandSetpoint_read
        
    def write_demandSetpoint(self, attr):
        self.debug_stream("In write_demandSetpoint()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(PinnacleDS.demandSetpoint_write) ENABLED START -----#
        voltagetoset = int(self.Supply.BoundInput(data,0,800))
        self.queueVoltage = voltagetoset
        self.queueSetVoltage = True
        #----- PROTECTED REGION END -----#	//	PinnacleDS.demandSetpoint_write
        
    def read_dutyCycle(self, attr):
        self.debug_stream("In read_dutyCycle()")
        #----- PROTECTED REGION ID(PinnacleDS.dutyCycle_read) ENABLED START -----#
        attr.set_value(self.attr_dutyCycle_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.dutyCycle_read
        
    def write_dutyCycle(self, attr):
        self.debug_stream("In write_dutyCycle()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(PinnacleDS.dutyCycle_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.dutyCycle_write
        
    def read_enable(self, attr):
        self.debug_stream("In read_enable()")
        #----- PROTECTED REGION ID(PinnacleDS.enable_read) ENABLED START -----#
        attr.set_value(self.attr_enable_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.enable_read
        
    def write_enable(self, attr):
        self.debug_stream("In write_enable()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(PinnacleDS.enable_write) ENABLED START -----#
        self.attr_enable_read = data
        if data:
            self.queueEnable = True
        else:
            self.queueDisable = True
            self.set_state(PyTango.DevState.OFF)
        #----- PROTECTED REGION END -----#	//	PinnacleDS.enable_write
        
    def read_enabled(self, attr):
        self.debug_stream("In read_enabled()")
        #----- PROTECTED REGION ID(PinnacleDS.enabled_read) ENABLED START -----#
        self.attr_enabled_read = self.processStatusDict['OutputPowerOn']
        
        if self.attr_enabled_read or self.attr_voltageReadback_read>50:
            # voltage readback captures the case where there's no current (no load condition) but the supply is still live
            self.set_state(PyTango.DevState.ON)


        attr.set_value(self.attr_enabled_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.enabled_read
        
    def read_warningStatus(self, attr):
        self.debug_stream("In read_warningStatus()")
        #----- PROTECTED REGION ID(PinnacleDS.warningStatus_read) ENABLED START -----#
        attr.set_value(self.attr_warningStatus_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.warningStatus_read
        
    def read_faultStatus(self, attr):
        self.debug_stream("In read_faultStatus()")
        #----- PROTECTED REGION ID(PinnacleDS.faultStatus_read) ENABLED START -----#
        self.attr_enabled_read = self.processStatusDict['FaultActive']
        attr.set_value(self.attr_faultStatus_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.faultStatus_read
        
    def read_interlockActive(self, attr):
        self.debug_stream("In read_interlockActive()")
        #----- PROTECTED REGION ID(PinnacleDS.interlockActive_read) ENABLED START -----#
        if self.processStatusDict['UserInterlockOpen'] or self.processStatusDict['MainContactorInterlockOpen']:
            self.attr_interlockActive_read = True
            self.set_state(PyTango.DevState.DISABLE)
        
        elif self.attr_interlockActive_read and not (self.processStatusDict['UserInterlockOpen'] or self.processStatusDict['MainContactorInterlockOpen']):
            self.attr_interlockActive_read = False
            self.set_state(PyTango.DevState.OFF)

        else:
            self.attr_interlockActive_read = False
        
        attr.set_value(self.attr_interlockActive_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.interlockActive_read
        
    def read_powerReadback(self, attr):
        self.debug_stream("In read_powerReadback()")
        #----- PROTECTED REGION ID(PinnacleDS.powerReadback_read) ENABLED START -----#
        attr.set_value(self.attr_powerReadback_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.powerReadback_read
        
    def read_processStatus(self, attr):
        self.debug_stream("In read_processStatus()")
        #----- PROTECTED REGION ID(PinnacleDS.processStatus_read) ENABLED START -----#
        # grab the dict, cast to string, replace the comma whitespace combo with /r/n

        #TODO: this needs to get broken out into invidual things; currently dumped into a command for the curious
        
        
        attr.set_value(self.attr_processStatus_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.processStatus_read
        
    def read_pulseFrequency(self, attr):
        self.debug_stream("In read_pulseFrequency()")
        #----- PROTECTED REGION ID(PinnacleDS.pulseFrequency_read) ENABLED START -----#
        attr.set_value(self.attr_pulseFrequency_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.pulseFrequency_read
        
    def write_pulseFrequency(self, attr):
        self.debug_stream("In write_pulseFrequency()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(PinnacleDS.pulseFrequency_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.pulseFrequency_write
        
    def read_reverseTime(self, attr):
        self.debug_stream("In read_reverseTime()")
        #----- PROTECTED REGION ID(PinnacleDS.reverseTime_read) ENABLED START -----#
        attr.set_value(self.attr_reverseTime_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.reverseTime_read
        
    def read_voltageReadback(self, attr):
        self.debug_stream("In read_voltageReadback()")
        #----- PROTECTED REGION ID(PinnacleDS.voltageReadback_read) ENABLED START -----#
        attr.set_value(self.attr_voltageReadback_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.voltageReadback_read
        
    def read_currentLimit(self, attr):
        self.debug_stream("In read_currentLimit()")
        #----- PROTECTED REGION ID(PinnacleDS.currentLimit_read) ENABLED START -----#
        attr.set_value(self.attr_currentLimit_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.currentLimit_read
        
    def write_currentLimit(self, attr):
        self.debug_stream("In write_currentLimit()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(PinnacleDS.currentLimit_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.currentLimit_write
        
    def read_dutyCycleLimits(self, attr):
        self.debug_stream("In read_dutyCycleLimits()")
        #----- PROTECTED REGION ID(PinnacleDS.dutyCycleLimits_read) ENABLED START -----#
        attr.set_value(self.attr_dutyCycleLimits_read)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.dutyCycleLimits_read
        
    
    
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(PinnacleDS.read_attr_hardware) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.read_attr_hardware


    # -------------------------------------------------------------------------
    #    PinnacleDS command methods
    # -------------------------------------------------------------------------
    
    def DCOff(self):
        """ turns off DC.
        """
        self.debug_stream("In DCOff()")
        #----- PROTECTED REGION ID(PinnacleDS.DCOff) ENABLED START -----#
        self.queueDisable = True
        #----- PROTECTED REGION END -----#	//	PinnacleDS.DCOff
        
    def DCOn(self):
        """ Turns on DC.
        """
        self.debug_stream("In DCOn()")
        #----- PROTECTED REGION ID(PinnacleDS.DCOn) ENABLED START -----#
        self.queueEnable = True
        #----- PROTECTED REGION END -----#	//	PinnacleDS.DCOn
        
    def Reset(self):
        """ Resets supply.
        """
        self.debug_stream("In Reset()")
        #----- PROTECTED REGION ID(PinnacleDS.Reset) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.Reset
        
    def Send(self, argin):
        """ Allows direct sending of pretty much anything via the send command.  Format is:
        Takes an array of strings, first is command number, second is data, third is address (if present).
        Send(command number, data)
        :param argin: 
        :type argin: PyTango.DevVarStringArray
        :rtype: PyTango.DevString
        """
        self.debug_stream("In Send()")
        argout = ""
        #----- PROTECTED REGION ID(PinnacleDS.Send) ENABLED START -----#
        self.queueSendData = []
        self.queueSendData = argin
        self.queueSend = True

        while self.queueSend:

            time.sleep(.1)

        argout = str(self.queueSendData)
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.Send
        return argout
        
    def SendConfig(self):
        """ Sends the configuration bits in the device properties.
        """
        self.debug_stream("In SendConfig()")
        #----- PROTECTED REGION ID(PinnacleDS.SendConfig) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.SendConfig
        
    def ReadProcessStatus(self):
        """ 
        :rtype: PyTango.DevString
        """
        self.debug_stream("In ReadProcessStatus()")
        argout = ""
        #----- PROTECTED REGION ID(PinnacleDS.ReadProcessStatus) ENABLED START -----#
        argout = str(self.processStatusDict).replace(', ','\r\n')
        #----- PROTECTED REGION END -----#	//	PinnacleDS.ReadProcessStatus
        return argout
        

    #----- PROTECTED REGION ID(PinnacleDS.programmer_methods) ENABLED START -----#
    def IOMethod(self):
        
        
        
        
        while(True):
            
            try:
                self.Supply = Pinnacle.Pinnacle()
                self.Supply.StartSerial(self.defaultBaud,self.defaultPortID,self.defaultTimeout)
                # read process status to set up dicts
                temp4, self.processStatusDict = self.Supply.ReadProcessStatus()
                self.Supply.ControlMode(self.Supply.host_control_mode)
                self.Supply.RegulationSelect(self.Supply.voltage_regulation)
                # queue a setpoint write
                self.queueSetVoltage = True
                
                self.set_state(PyTango.DevState.OFF)

            
            
                while True:
                    
                    try:
                    
                        if self.queueSend:

                            try:
                                # build the thing to send.
                                print(self.queueSendData)
                                
                                datalen = len(self.queueSendData)
                                print(datalen)

                                if datalen == 1:
                                    response = self.Supply.Send(int(self.queueSendData[0]),[],self.Supply.defaultAddress )
                                    print(response)
                                
                                if datalen == 2:
                                    response = self.Supply.Send(int(self.queueSendData[0]),self.Supply.long_to_bytes(int(self.queueSendData[1]),2),self.Supply.defaultAddress )
                                    print(response)


                                if datalen == 3:
                                    response = self.Supply.Send(int(self.queueSendData[0]),self.Supply.long_to_bytes(int(self.queueSendData[1]),2)  + self.Supply.long_to_bytes(int(self.queueSendData[2]),2),self.Supply.defaultAddress )
                                    print(response)

                                self.queueSendData = response
                            
                            except:

                                traceback.print_exc()
                                self.queueSendData = ''


                            self.queueSend = False

                        if self.queueSetVoltage:
                            self.queueSetVoltage = False
                            self.Supply.Setpoint(self.queueVoltage)

                        if self.queueEnable:
                            self.queueEnable = False
                            self.Supply.DCOn()

                        if self.queueDisable:
                            self.queueDisable = False
                            self.Supply.DCOff()

                        if self.queueReset:
                            self.queueReset = False
                            self.Supply.MasterReset()

                        if self.queueSetDutyCycle:
                            
                            # implement when pinnacle plus+ arrives
                            pass

                        if self.queueSetPulseFrequency:

                            # implement when pinnacle plus+ arrives
                            pass

                        if self.queueSetReverseTime:

                            # implement when pinnacle plus+ arrives
                            pass

                        # now get some readback values
                        temp, self.attr_powerReadback_read, self.attr_voltageReadback_read, self.attr_currentReadback_read = self.Supply.ReadPVI()
                        # this presently doesn't work on the old units.
                        temp, temp2, self.attr_demandSetpoint_read, temp3 = self.Supply.ReadPVISetpoints()
                        
                        temp4, self.processStatusDict = self.Supply.ReadProcessStatus()

                        self.attr_arcRate_read = self.Supply.ReadArcDensity()

                    except:

                        print(str(datetime.now()))
                        traceback.print_exc()
                        time.sleep(5)
                        self.set_state(PyTango.DevState.FAULT)
            
            except:

                print(str(datetime.now()))
                traceback.print_exc()
                time.sleep(5)
                self.set_state(PyTango.DevState.FAULT)

    #----- PROTECTED REGION END -----#	//	PinnacleDS.programmer_methods

class PinnacleDSClass(PyTango.DeviceClass):
    # -------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(PinnacleDS.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	PinnacleDS.global_class_variables


    #    Class Properties
    class_property_list = {
        }


    #    Device Properties
    device_property_list = {
        'arcShutdownTime':
            [PyTango.DevDouble, 
            "Defines the arc shutdown time in uSec.\n\n0 = 200\n1 = 500\n2 = 1000\n3 = 1500\n4 = 2000\n5 = 2500\n6 = 3000\n7 = 4000\n8 = 5000",
            [4]],
        'defaultAddress':
            [PyTango.DevShort, 
            "Defines the AEbus address.  Typically 2 for most devices.",
            [1]],
        'defaultBaud':
            [PyTango.DevLong, 
            "sets the baud rate used for communication.  Note that this device server is designed only for 19200 baud and other baud rates should be used with caution.",
            [19200]],
        'defaultPortID':
            [PyTango.DevString, 
            "Defines the port used for serial communication.\n\nExample: `/dev/tty_USB0`",
            ["/dev/tty_Extraction_Pulser"] ],
        'defaultTimeout':
            [PyTango.DevFloat, 
            "The timeout period for the pyserial code.  Should not have a large effect as all reads and writes have been refactored to be bytewise and not dependent on waiting for a EOL character.",
            [0.1]],
        'hardArcCountLimit':
            [PyTango.DevDouble, 
            "Defines the number of arcs required to indicate a hard arc.",
            [0]],
        'outOfSetpointTimer':
            [PyTango.DevDouble, 
            "Enables or disables an out-of-setpoint timer that will trigger if things go poorly.\n\n1 = .1 sec\n10 = 1 sec\n100 = 10 sec\n...\n9999 = 999.9 sec",
            [10]],
        'overCurrentTimer':
            [PyTango.DevDouble, 
            "Enables or disables an overvoltage timer that will trigger if things go poorly.\n\n1 = .1 sec\n10 = 1 sec\n100 = 10 sec\n...\n9999 = 999.9 sec",
            [5]],
        'overVoltageTimer':
            [PyTango.DevDouble, 
            "Enables or disables an overvoltage timer that will trigger if things go poorly.\n\n1 = .1 sec\n10 = 1 sec\n100 = 10 sec\n...\n9999 = 999.9 sec",
            [2]],
        'regulationMethod':
            [PyTango.DevDouble, 
            "Defines the regulation method. 6 = Power, 7 = Current, 8 = Voltage.\n\nDefaults to Voltage.",
            [8]],
        'vArcTripLevel':
            [PyTango.DevDouble, 
            "Defines the trip level for an arc.\n\n0 = 10\n1 = 20\n...\n9 = 100\n10 = 125\n....\n15 = 250",
            [5]],
        }


    #    Command definitions
    cmd_list = {
        'DCOff':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'DCOn':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Reset':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Send':
            [[PyTango.DevVarStringArray, "none"],
            [PyTango.DevString, "none"]],
        'SendConfig':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'ReadProcessStatus':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevString, "none"]],
        }


    #    Attribute definitions
    attr_list = {
        'arcCount':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ]],
        'arcRate':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ]],
        'currentReadback':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "A",
                'description': "Current readback of the supply.",
            } ],
        'demandSetpoint':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "VoltageSetpoint",
                'unit': "V",
                'standard unit': "V",
                'max value': "1000",
                'min value': "0",
                'description': "Voltage Setpoint.",
                'period': "1000",
                'rel_change': "1",
            } ],
        'dutyCycle':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'enable':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "EnableSupply",
                'Polling period': "1000",
                'period': "1000",
            } ],
        'enabled':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "SupplyEnabled",
                'Polling period': "1000",
                'period': "1000",
            } ],
        'warningStatus':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'period': "1000",
            } ],
        'faultStatus':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'period': "1000",
            } ],
        'interlockActive':
            [[PyTango.DevBoolean,
            PyTango.SCALAR,
            PyTango.READ]],
        'powerReadback':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "Power",
                'unit': "W",
                'standard unit': "W",
                'period': "1000",
                'rel_change': "5",
            } ],
        'processStatus':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'period': "1000",
            } ],
        'pulseFrequency':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'reverseTime':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ]],
        'voltageReadback':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "VoltageReadback",
                'unit': "V",
                'standard unit': "V",
                'period': "1000",
                'rel_change': "5",
            } ],
        'currentLimit':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'dutyCycleLimits':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'description': "Provides the user with a string of min/max duty cycle limits at a given pulse frequency.",
            } ],
        }


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(PinnacleDSClass, PinnacleDS, 'PinnacleDS')
        #----- PROTECTED REGION ID(PinnacleDS.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	PinnacleDS.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print ('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print ('-------> An unforeseen exception occured....', e)

if __name__ == '__main__':
    main()
