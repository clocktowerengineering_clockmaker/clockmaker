#!/usr/bin/env python
# -*- coding:utf-8 -*-


# ############################################################################
#  license :
# ============================================================================
#
#  File :        AEMatchboxDS.py
#
#  Project :     rareRF
#
# This file is part of Tango device class.
# 
# Tango is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Tango is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Tango.  If not, see <http://www.gnu.org/licenses/>.
# 
#
#  $Author :      mark.amato$
#
#  $Revision :    $
#
#  $Date :        $
#
#  $HeadUrl :     $
# ============================================================================
#            This file is generated by POGO
#     (Program Obviously used to Generate tango Object)
# ############################################################################

__all__ = ["AEMatchboxDS", "AEMatchboxDSClass", "main"]

__docformat__ = 'restructuredtext'

import PyTango
import sys

# Add additional import
#----- PROTECTED REGION ID(AEMatchboxDS.additionnal_import) ENABLED START -----#
from pyclockmaker.VendorDevices.advanced_energy import AEMatchbox
from datetime import datetime
import time
import threading
import traceback

#----- PROTECTED REGION END -----#	//	AEMatchboxDS.additionnal_import

# Device States Description
# FAULT : Unit has a fault raised.
# ON : Device Server Operational.


class AEMatchboxDS (PyTango.Device_4Impl):
    """This class controls any Advanced Energy matchbox via the AEBus protocol."""
    
    # -------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(AEMatchboxDS.global_variables) ENABLED START -----#
    capacitorDict = dict()
    loadDict = dict()
    lastStatusReading = datetime.now()
    queueRemote = False
    queueAutomatic = False
    queueLocal = False
    queueSend = False
    queueData = []
    queueLoad = False
    queueTune = False
    #----- PROTECTED REGION END -----#	//	AEMatchboxDS.global_variables

    def __init__(self, cl, name):
        PyTango.Device_4Impl.__init__(self,cl,name)
        self.debug_stream("In __init__()")
        AEMatchboxDS.init_device(self)
        #----- PROTECTED REGION ID(AEMatchboxDS.__init__) ENABLED START -----#
        print(str(datetime.now()) + " initializing ")
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(AEMatchboxDS.delete_device) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_LoadCapacitorPosition_read = 0.0
        self.attr_faultCode_read = ""
        self.attr_statusCode_read = ""
        self.attr_TuneCapacitorPosition_read = 0.0
        self.attr_loadOhms_read = 0.0
        self.attr_loadX_read = 0.0
        self.attr_loadGamma_read = 0.0
        self.attr_loadWatts_read = 0.0
        self.set_change_event("LoadCapacitorPosition", True, False)
        self.set_change_event("faultCode", True, False)
        self.set_change_event("statusCode", True, False)
        self.set_change_event("TuneCapacitorPosition", True, False)
        self.set_change_event("loadOhms", True, False)
        self.set_change_event("loadX", True, False)
        self.set_change_event("loadGamma", True, False)
        self.set_change_event("loadWatts", True, False)
        #----- PROTECTED REGION ID(AEMatchboxDS.init_device) ENABLED START -----#

        
        self.ioThread = threading.Thread(target = self.IOMethod)
        self.ioThread.setDaemon(True)
        self.ioThread.start() 

        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(AEMatchboxDS.always_executed_hook) ENABLED START -----#
        

        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.always_executed_hook

    # -------------------------------------------------------------------------
    #    AEMatchboxDS read/write attribute methods
    # -------------------------------------------------------------------------
    
    def read_LoadCapacitorPosition(self, attr):
        self.debug_stream("In read_LoadCapacitorPosition()")
        #----- PROTECTED REGION ID(AEMatchboxDS.LoadCapacitorPosition_read) ENABLED START -----#
        attr.set_value(self.attr_LoadCapacitorPosition_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.LoadCapacitorPosition_read
        
    def write_LoadCapacitorPosition(self, attr):
        self.debug_stream("In write_LoadCapacitorPosition()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(AEMatchboxDS.LoadCapacitorPosition_write) ENABLED START -----#
        self.queueData = int(data*100)
        self.queueLoad = True

        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.LoadCapacitorPosition_write
        
    def read_faultCode(self, attr):
        self.debug_stream("In read_faultCode()")
        #----- PROTECTED REGION ID(AEMatchboxDS.faultCode_read) ENABLED START -----#
        
        attr.set_value(self.attr_faultCode_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.faultCode_read
        
    def read_statusCode(self, attr):
        self.debug_stream("In read_statusCode()")
        #----- PROTECTED REGION ID(AEMatchboxDS.statusCode_read) ENABLED START -----#
        
        attr.set_value(self.attr_statusCode_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.statusCode_read
        
    def read_TuneCapacitorPosition(self, attr):
        self.debug_stream("In read_TuneCapacitorPosition()")
        #----- PROTECTED REGION ID(AEMatchboxDS.TuneCapacitorPosition_read) ENABLED START -----#
        attr.set_value(self.attr_TuneCapacitorPosition_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.TuneCapacitorPosition_read
        
    def write_TuneCapacitorPosition(self, attr):
        self.debug_stream("In write_TuneCapacitorPosition()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(AEMatchboxDS.TuneCapacitorPosition_write) ENABLED START -----#
        self.queueData = int(data*100)
        self.queueTune = True
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.TuneCapacitorPosition_write
        
    def read_loadOhms(self, attr):
        self.debug_stream("In read_loadOhms()")
        #----- PROTECTED REGION ID(AEMatchboxDS.loadOhms_read) ENABLED START -----#

        attr.set_value(self.attr_loadOhms_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.loadOhms_read
        
    def read_loadX(self, attr):
        self.debug_stream("In read_loadX()")
        #----- PROTECTED REGION ID(AEMatchboxDS.loadX_read) ENABLED START -----#
        
        attr.set_value(self.attr_loadX_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.loadX_read
        
    def read_loadGamma(self, attr):
        self.debug_stream("In read_loadGamma()")
        #----- PROTECTED REGION ID(AEMatchboxDS.loadGamma_read) ENABLED START -----#
        
        attr.set_value(self.attr_loadGamma_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.loadGamma_read
        
    def read_loadWatts(self, attr):
        self.debug_stream("In read_loadWatts()")
        #----- PROTECTED REGION ID(AEMatchboxDS.loadWatts_read) ENABLED START -----#
        
        attr.set_value(self.attr_loadWatts_read)
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.loadWatts_read
        
    
    
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(AEMatchboxDS.read_attr_hardware) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.read_attr_hardware


    # -------------------------------------------------------------------------
    #    AEMatchboxDS command methods
    # -------------------------------------------------------------------------
    
    def Automatic(self):
        """ Sets the matchbox into automatic mode.
        """
        self.debug_stream("In Automatic()")
        #----- PROTECTED REGION ID(AEMatchboxDS.Automatic) ENABLED START -----#
        self.queueAutomatic = True
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.Automatic
        
    def GetMode(self):
        """ Returns the current mode of the matchbox (local, remote, automatic)
        :rtype: PyTango.DevString
        """
        self.debug_stream("In GetMode()")
        argout = ""
        #----- PROTECTED REGION ID(AEMatchboxDS.GetMode) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.GetMode
        return argout
        
    def Local(self):
        """ Sets the matchbox to local communication.
        """
        self.debug_stream("In Local()")
        #----- PROTECTED REGION ID(AEMatchboxDS.Local) ENABLED START -----#
        self.queueLocal = True
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.Local
        
    def Remote(self):
        """ Sets the matchbox to remote (serial) communication.
        """
        self.debug_stream("In Remote()")
        #----- PROTECTED REGION ID(AEMatchboxDS.Remote) ENABLED START -----#
        self.queueRemote = True
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.Remote
        
    def Send(self, argin):
        """ Allows direct sending of pretty much anything via the send command.  Format is:
        
        Send(command number, data)
        :param argin: 
        :type argin: PyTango.DevString
        :rtype: PyTango.DevString
        """
        self.debug_stream("In Send()")
        argout = ""
        #----- PROTECTED REGION ID(AEMatchboxDS.Send) ENABLED START -----#

        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.Send
        return argout
        

    #----- PROTECTED REGION ID(AEMatchboxDS.programmer_methods) ENABLED START -----#
    def IOMethod(self):
        
        while True:
            
            
            try:
                self.Matchbox = AEMatchbox.AEMatchbox()
                self.Matchbox.StartSerial(self.defaultBaud,self.defaultPortID,self.defaultTimeout) 
                self.set_state(PyTango.DevState.ON)
                faultread = 0
                
                while(True):
                        
                    if self.queueAutomatic:
                        
                        self.queueAutomatic = False
                        self.Matchbox.SetControlMode(1)
                        self.set_state(PyTango.DevState.ON)
                        print('automode')
                        
                    if self.queueLoad:
                        
                        self.queueLoad = False
                        self.Matchbox.SetLoadCapPosition(self.queueData)
                        
                        
                    if self.queueLocal:
                        
                        self.queueLocal = False
                        self.Matchbox.SetControlMode(0)
                        self.set_state(PyTango.DevState.MOVING)
                        print('localmode')
                        
                    if self.queueRemote:
                        
                        self.queueRemote = False
                        self.Matchbox.SetControlMode(2)
                        print('remotemode')
                        
                    if self.queueSend:
                        
                        self.queueSend = False
                        
                    if self.queueTune:
                        
                        self.queueTune = False
                        self.Matchbox.SetTuneCapPosition(self.queueData)

                    
                    
                        
                    time.sleep(.05)
                    self.capacitorDict = self.Matchbox.GetCapPosition()
                    self.attr_LoadCapacitorPosition_read = self.capacitorDict['load']
                    self.attr_TuneCapacitorPosition_read = self.capacitorDict['tune']
                    self.loadDict = self.Matchbox.GetSensorData()
                    self.attr_loadOhms_read = float(self.loadDict['ohm'])
                    self.attr_loadX_read = float(self.loadDict['x'])
                    self.attr_loadGamma_read = float(self.loadDict['gamma'])
                    self.attr_loadWatts_read = float(self.loadDict['watts'])

                    faultread = faultread + 1

                    if faultread > 20:
                        self.lastStatusReading= datetime.now()
                        time.sleep(.05)
                        faults = self.Matchbox.GetFaults()
                        self.attr_faultCode_read = (":".join("{:02x}".format(int(c)) for c in faults))
                        time.sleep(.05)
                        statuses = self.Matchbox.GetStatus()
                        self.attr_statusCode_read = (":".join("{:02x}".format(int(c)) for c in statuses))
                        time.sleep(.05)
                        faultread = 0
            except:
                traceback.print_exc()
                print('Serial device failure at ' + str(datetime.now()))
                self.set_state(PyTango.DevState.ALARM)
                self.attr_LoadCapacitorPosition_read = -1
                self.attr_TuneCapacitorPosition_read = -1
                self.attr_loadOhms_read = -1
                self.attr_loadX_read = -1
                self.attr_loadGamma_read = -1
                self.attr_loadWatts_read = -1
                
                time.sleep(5)       
      
    #----- PROTECTED REGION END -----#	//	AEMatchboxDS.programmer_methods

class AEMatchboxDSClass(PyTango.DeviceClass):
    # -------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(AEMatchboxDS.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	AEMatchboxDS.global_class_variables


    #    Class Properties
    class_property_list = {
        }


    #    Device Properties
    device_property_list = {
        'defaultAddress':
            [PyTango.DevShort, 
            "Defines the AEbus address.  Typically 2 for most devices.",
            [2]],
        'defaultBaud':
            [PyTango.DevLong, 
            "sets the baud rate used for communication.  Note that this device server is designed only for 19200 baud and other baud rates should be used with caution.",
            [19200]],
        'defaultPortID':
            [PyTango.DevString, 
            "Defines the port used for serial communication.\n\nExample: `/dev/tty_USB0`",
            [] ],
        'defaultTimeout':
            [PyTango.DevFloat, 
            "The timeout period for the pyserial code.  Should not have a large effect as all reads and writes have been refactored to be bytewise and not dependent on waiting for a EOL character.",
            [0.2]],
        }


    #    Command definitions
    cmd_list = {
        'Automatic':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'GetMode':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevString, "none"]],
        'Local':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Remote':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Send':
            [[PyTango.DevString, "none"],
            [PyTango.DevString, "none"]],
        }


    #    Attribute definitions
    attr_list = {
        'LoadCapacitorPosition':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "Load Capacitor Position",
                'unit': "%",
                'standard unit': "%",
                'display unit': "%",
                'format': "%3.2f",
                'max value': "100",
                'min value': "0",
                'description': "Capacitor position from 0 to 100% to the nearest hundredth.",
                'period': "500",
                'rel_change': ".01",
            } ],
        'faultCode':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "The encoded fault word coming from the matchbox.",
                'description': "Provides the unpacked bytewise fault word for later consumption.",
            } ],
        'statusCode':
            [[PyTango.DevString,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "The encoded status word coming from the matchbox.",
                'description': "Provides the unpacked bytewise status word for later consumption.",
            } ],
        'TuneCapacitorPosition':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'label': "Tune Capacitor Position",
                'unit': "%",
                'standard unit': "%",
                'display unit': "%",
                'format': "%3.2f",
                'max value': "100",
                'min value': "0",
                'description': "Capacitor position from 0 to 100% to the nearest hundredth.",
                'period': "500",
                'rel_change': ".01",
            } ],
        'loadOhms':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "Ohms",
                'description': "Load parameter returned from the matchbox sensor.",
                'period': "500",
                'rel_change': ".1",
            } ],
        'loadX':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "X",
                'description': "Load parameter returned from the matchbox sensor.",
                'period': "500",
                'rel_change': ".1",
            } ],
        'loadGamma':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "gamma",
                'format': "%f",
                'description': "Load parameter returned from the matchbox sensor.",
                'period': "500",
                'rel_change': ".1",
            } ],
        'loadWatts':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ],
            {
                'label': "Watts",
                'unit': "W",
                'format': "%f",
                'description': "Load parameter returned from the matchbox sensor.",
                'period': "500",
                'rel_change': ".1",
            } ],
        }


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(AEMatchboxDSClass, AEMatchboxDS, 'AEMatchboxDS')
        #----- PROTECTED REGION ID(AEMatchboxDS.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	AEMatchboxDS.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print ('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print ('-------> An unforeseen exception occured....', e)

if __name__ == '__main__':
    main()
