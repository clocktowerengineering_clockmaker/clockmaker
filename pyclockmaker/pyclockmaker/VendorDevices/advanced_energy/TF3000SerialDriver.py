import time
from datetime import datetime
import serial
import re
import traceback


class TF3000SerialDriver:


    # defines a TF3000 RS485 object.

    # parameters and action types
    terminator = "\r\n".encode()

    default_port = "/dev/ttyUSB1"
    default_baud = 4800
    default_timeout = .25

    # data types
    boolean_old_0 = 0
    u_integer_1 = 1
    u_real_2 = 2
    string_6_4 = 4
    boolean_new_6 = 6
    u_short_int_7 = 7
    u_expo_new_10 = 10
    string_16_11 = 11


    #parameters

    #control commands
    currentSet = ['SI ', string_6_4]
    voltageSet = ['SV ', string_6_4]
    controlMode = ['REMS ', string_6_4]
    powerWord = ['POWER ',string_6_4]

    #status requests
    Current = ['RI?', u_short_int_7]
    Voltage = ['RV?', u_short_int_7]
    Status0 = ['STUS 0', string_6_4]
    Status1 = ['STUS 1', string_6_4]
    

    def __init__(self,serialPort=default_port,baud=default_baud,timeout=default_timeout):

        print(str(datetime.now()) + ": New PS Starting!")
        self.StartSerial(serialPort,baud,timeout)

    
    def StartSerial(self,port=default_port,baud=default_baud,timeout=default_timeout):

        self.supplyserial = serial.Serial()
        self.supplyserial.port = port
        self.supplyserial.baudrate = baud
        self.supplyserial.timeout = timeout
        self.supplyserial.open()
        time.sleep(.01)
        self.supplyserial.read_all()

        return self.supplyserial



    def StopSerial(self):

        self.supplyserial.close()

    def SetSerialCommandMode(self):

        # sets the command mode into serial.
        self.short_int_cmd(self.controlMode, 1)
        self.supplyserial.read_all()


    def SetLocalCommandMode(self):

        # sets the command mode into local (analog control)
        self.short_int_cmd(self.controlMode, 0)
        self.supplyserial.read_all()


    def GetCommandMode(self):

        # gets the command mode
        self.supplyserial.read_all()

        return self.DataRequest(["REMS 2",self.boolean_new_6])

    def SetRemotePowerOn(self):

        self.short_int_cmd(self.powerWord, 1)
        self.supplyserial.read_all()



    def SetRemotePowerOff(self):

        self.short_int_cmd(self.powerWord, 0)
        self.supplyserial.read_all()


    def GetRemotePower(self):
        self.supplyserial.read_all()
        return self.DataRequest(["POWER 2",self.boolean_new_6])

    def DataRequest(self,parameter):

        fulltelegram = parameter[0].encode() + self.terminator
        self.supplyserial.write(fulltelegram)
        #print(fulltelegram)

        #time.sleep(2)
        
        try:
            response = self.supplyserial.read_until('\r\n=>\r\n'.encode())
            # print(response)
            readback = response.decode()
        except:
            traceback.print_exc()
            return None
        

        if parameter[1] == self.boolean_new_6 or parameter[1] == self.boolean_old_0:
            #print(readback)
            #return(readback)
            if readback[0] == "1" or readback[0] == "3":

                return True
            
            elif readback[0] == "0" or readback[0] == "2":

                return False

            else:

                raise Exception 


        if parameter[1] == self.string_16_11:

            # return readback[-20:-4]
            pass

        if parameter[1] == self.string_6_4:
            try: 
                bin_readback = str(bin(int(readback)))[2:]
                # print(bin_readback)
                padded = bin_readback.zfill(8)
                # print(padded)

                return padded
            except:
                return None

        if parameter[1] == self.u_expo_new_10:

            # not implemented. Implement when sensors available

            pass

        if parameter[1] == self.u_real_2:

            # return int(readback[-10:-4])*.01
            pass

        if parameter[1] == self.u_integer_1:

            # return int(readback[-10:-4])
            pass

        if parameter[1] == self.u_short_int_7:
            try:
                #print(str(datetime.now())+": " + str(float(''.join(re.findall('\.|\d', readback)))))
                return float(''.join(re.findall('\.|\d', readback)))
            except:
                traceback.print_exc()
                return -1.0
    
    def short_int_cmd(self, parameter, val):
        fulltelegram = parameter[0].encode() + str(val).encode() + self.terminator
        print(fulltelegram)
        self.supplyserial.write(fulltelegram)
 
    def current(self):
        return self.DataRequest(self.Current) 
    
    def voltage(self):
        return self.DataRequest(self.Voltage)
    
    def current_set(self, val):
        if val:
            self.short_int_cmd(self.currentSet, val)
        pass
    
    def voltage_set(self, val):
        if val:
            self.short_int_cmd(self.voltageSet, val)
        pass
    
    def SupplyOnReadback(self):

        byte1 = self.DataRequest(self.Status0)
        byte2 = self.DataRequest(self.Status1)

        byte_count = 0
        digit_index = 0
        try:
            for byte in [byte1, byte2]:
                byte_count += 1
                digit_index = 0
                for digit in byte:
                    if int(digit) == 1:
                        return self.status_error_code(byte_count, digit_index)
                    digit_index += 1

            return (True, 'No Errors Present')
        except:
            traceback.print_exc()
            return (None, None)
    
    def status_error_code(self, byte_num, index):
        error = ''
        status_bool = False
        if byte_num == 1:
            if index == 7:
                error = 'OVP Shutdown'
            elif index == 6:
                error = 'OLP Shutdown'
            elif index == 5:
                error = 'OTP Shutdown'
            elif index == 4:
                error = 'Fan Failure'
            elif index == 3:
                error = 'AUX or SMPS Fail'
            elif index == 2:
                error = 'HI-TEMP Alarm'
                status_bool = True
            elif index == 1:
                error = 'AC Input Power Down'
            else:
                error = 'AC Input Failure'
        else:
            if index == 7:
                error = 'Inhibit by VCI / ACI or ENB'
            elif index == 6:
                error = 'Inhibit by Software Command'
            elif index == 3:
                error = 'Power'
            elif index == 0:
                error = 'Remote'
            else:
                error = 'Unknown'
                status_bool = None
        return (status_bool, error)
            


# s = TF3000SerialDriver()

# print("running")
# print(s.current())


