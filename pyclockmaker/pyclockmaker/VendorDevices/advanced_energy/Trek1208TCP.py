import time
from datetime import datetime
import socket
import os
import traceback
import re
import random


class Trek1208TCP:
    
    terminator = "\r\n"
    LocalPort = random.randint(21000,35000)
    SupplyPort = 3000
    SupplyAddress = "192.168.8.190"
    timeout = 1
    buffer = b''
    XModeVoltage = 0.0
    YModeVoltage = 0.0
    
    # Init object
    def __init__(self):
        pass
        
    
    # Open serial object
    def StartTCP(self,interface="enp1s0"):
        
        self.Trek1208TCP = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        #TODO: do we really need this?
        
        # TODO: fix this ipv4 = self.get_ip_address(interface)
        print(f'binding to', self.LocalPort)
        self.Trek1208TCP.bind(('192.168.8.29', int(self.LocalPort)))
        self.Trek1208TCP.settimeout(self.timeout)
        self.Trek1208TCP.connect((self.SupplyAddress,self.SupplyPort))
        


        return self.Trek1208TCP
    
    def get_ip_address(self,ifname):
        # This is a pretty baller way to get the IP of an interface.
        # Taken from https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python
        

        return os.popen('ip addr show ' + ifname + ' | grep "\<inet\>" | awk \'{ print $2 }\' | awk -F "/" \'{ print $1 }\'').read().strip()

    def get_line(self):
        # this is a neat little way to handle packet spread (which is insane on this fuckin thing)
        # https://stackoverflow.com/questions/67825653/how-can-i-properly-receive-data-with-a-tcp-python-socket-until-a-delimiter-is-fo
        while b'\r\n' not in self.buffer:
            data = self.Trek1208TCP.recv(1024)
            if not data: # socket closed
                return None
            self.buffer += data
        line,sep,self.buffer = self.buffer.partition(b'\r\n')
        return line.decode()
    # Close serial object
    def StopTCP(self):

        self.Trek1208TCP.close()

    
    # Command structure\r\n
    def TrekCommand(self,command):
        
        commandString = str(command) + self.terminator 
        return self.DirectWrite(commandString)
        
    def DirectWrite(self,commandString):
        #print(commandString)
        

        self.Trek1208TCP.send(commandString.encode())
        try:
            response = self.get_line()
            #print(response)
            return response
    
        except:
            traceback.print_exc()
            return []
        
    
    # Set commands

    def SetHVOn(self):
        self.TrekCommand("HV:ON")

    def SetHVOff(self):

        self.TrekCommand("HV:OFF")

    def SetClampValues(self,v1,v2,delay_time):
        
        self.TrekCommand("CLAMP:"+str(v1)+","+str(v2)+","+str(int(delay_time*10)*.1))

    def SetDCModeHVAmplitude(self,voltage):

        self.TrekCommand("AMPLITUDE:DC:"+str(int(voltage)))
        return voltage

    def SetACModeHVAmplitude(self,voltage):

        self.TrekCommand("AMPLITUDE:AC:"+str(int(voltage)))

        return voltage

    def SetXModeHVAmplitude(self,voltage):
        self.XModeVoltage = voltage
        self.TrekCommand("AMPLITUDE:X:"+str(int(voltage)))
        return voltage

    def SetYModeHVAmplitude(self,voltage):
        self.YModeVoltage = voltage
        self.TrekCommand("AMPLITUDE:Y:"+str(int(voltage)))
        
        return voltage

    def SetDCACClampDelay(self,delay_time=0):

        self.TrekCommand("CLAMP_DELAY:"+str(int(delay_time*10)*.1))

    def SetClampStart(self):

        self.TrekCommand("CLAMP:START")

    def SetDeclampStart(self):

        self.TrekCommand("DECLAMP")

    def SetACFrequency(self,frequency=100):

        self.TrekCommand("FREQUENCY:"+str(int(frequency*100)*.10))

    def SetDCPolarity(self,polarity = "+"):

        self.TrekCommand("POLARITY:"+str(polarity))

    def SetDCPolarityPlus(self):

        self.SetDCPolarity(polarity="+")

    def SetDCPolarityMinus(self):

        self.SetDCPolarity(polarity="-")

    def SetMode(self,mode="DC"):

        self.TrekCommand("MODE:"+str(mode))

    def SetACMode(self):

        self.SetMode(mode="AC")
   
    def SetDCMode(self):

        self.SetMode(mode="DC")

    def SetWaferClampSense(self,mode="EXTERNAL"):

         self.TrekCommand("WAFER_SENSE:"+str(mode))

    def SetWaferClampSenseInternal(self):

        self.SetWaferClampSense(mode="INTERNAL")

    def SetWaferClampSenseExternal(self):

        self.SetWaferClampSense(mode="EXTERNAL")


    def Reset(self):

        self.TrekCommand("RESET")

    def SetReset(self):

        self.Reset()

    def SetExternalControlSource(self,externalinput="ENABLE"):

         self.TrekCommand("EXTERNAL_INPUT:"+str(externalinput))

    def SetExternalControlSourceActive(self):

        self.SetExternalControlSource(externalinput="ENABLE")

    
    def SetExternalControlSourceInactive(self):

        self.SetExternalControlSource(externalinput="DISABLE")


    # Gets
    def GetHV(self):

        readback = self.TrekCommand("HV:?")
        
        if readback=="HV:ON":

            return True

        else:

            return False

    def GetDCModeHVAmplitude(self):

        readback = self.TrekCommand("AMPLITUDE:DC:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback

    def GetACModeHVAmplitude(self):

        readback = self.TrekCommand("AMPLITUDE:AC:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetXModeHVAmplitude(self):

        readback = self.TrekCommand("AMPLITUDE:AC:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
        
        return readback

    def GetACFrequency(self):

        readback = self.TrekCommand("FREQUENCY:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback


    def GetMode(self):

        readback = self.TrekCommand("MODE:?")
    
        return readback


    def GetILimitLow(self):

        readback = self.TrekCommand("I_LIMIT_LOW?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetILimitHigh(self):

        readback = self.TrekCommand("I_LIMIT_HIGH?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetWCThreshold(self):

        readback = self.TrekCommand("WC_THRESHOLD:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetWIPThreshold(self):

        readback = self.TrekCommand("WIP_THRESHOLD:?")
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetNoWaferThreshold(self):

        readback = self.TrekCommand("NO_WAFER:?")
        readback = readback.replace('0-','')
        readback = re.sub("[^-0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetClampMonitorX(self):

        readback = self.TrekCommand("CLAMP_MONITOR_X:")
        readback = readback.replace('0-','')
        readback = re.sub("[^0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetClampMonitorY(self):

        readback = self.TrekCommand("CLAMP_MONITOR_Y:")
        readback = readback.replace('0-','')
        readback = re.sub("[^0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetVoltageMonitorX(self):

        readback = self.TrekCommand("V_MONITOR:X")
        readback2 = readback.replace('0-','0')
        readback2 = re.sub("[^0-9. ]", "", readback2)
        readback2 = float(str.split(readback2)[0])*100
        
        if ((self.XModeVoltage - 25) < readback2) and ((self.XModeVoltage + 25 )> readback2):

            return readback2
        
        else:

            readback = readback.replace('0-','')
            readback = re.sub("[^0-9. ]", "", readback)
            readback = float(str.split(readback)[0])*100
    
            return readback
    
    def GetVoltageMonitorY(self):

        readback = self.TrekCommand("V_MONITOR:Y")
        readback2 = readback.replace('0-','')
        readback2 = re.sub("[^0-9. ]", "", readback2)
        readback2 = float(str.split(readback2)[0])*100

        if ((self.YModeVoltage - 5) < readback2) and ((self.YModeVoltage + 5 )> readback2):

            return readback2
        
        else:

            readback = readback.replace('0-','0')
            readback = re.sub("[^0-9. ]", "", readback)
            readback = float(str.split(readback)[0])*100
    
            return readback
    
        return readback
    
    def GetCurrentMonitorX(self):

        readback = self.TrekCommand("I_MONITOR:X")
        readback = readback.replace('0-','')
        readback = re.sub("[^0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback
    
    def GetCurrentMonitorY(self):

        readback = self.TrekCommand("I_MONITOR:Y")
        readback = readback.replace('0-','')
        readback = re.sub("[^0-9. ]", "", readback)
        readback = float(str.split(readback)[0])
    
        return readback