from pyclockmaker.VendorDevices.advanced_energy.Pinnacle import Pinnacle

class PinnaclePlus(Pinnacle):

    configdictionary = dict()
    param_read_faults = 223
    param_read_warnings = 223
    param_pulse_type = 65
    param_set_pulse_frequency = 92
    param_read_pulse_frequency = 146
    param_set_reverse_time = 93
    param_read_reverse_time=147
    
    minimum_frequency_index = 0
    maximum_frequency_index = 70
    frequency_index_step = 5
    minimum_reverse_time = .4 # microsec
    
    minimum_frequency = minimum_frequency_index*frequency_index_step
    maximum_frequency = maximum_frequency_index*frequency_index_step

    maxreversetable = [
    10,
    10,
    10,
    10,
    10,
    10,
    10,
    10,
    9.9,
    9,
    8.1,
    7.4,
    6.9,
    6.4,
    6.0,
    5.6,
    5.2,
    4.9,
    4.7,
    4.5,
    4.2,
    4,
    3.9,
    3.7,
    3.6,
    3.4,
    3.3,
    3.2,
    3.1,
    2.9,
    2.9,
    2.8,
    2.7,
    2.6,
    2.5,
    2.4,
    2.4,
    2.3,
    2.3,
    2.2,
    2.2,
    2.1,
    2,
    2,
    2,
    1.9,
    1.9,
    1.8,
    1.8,
    1.6,
    1.5,
    1.5,
    1.5,
    1.4,
    1.4,
    1.4,
    1.4,
    1.3,
    1.3,
    1.3,
    1.3,
    1.3,
    1.2,
    1.2,
    1.2,
    1.2,
    1.2,
    1.1,
    1.1,
    1.1
    ]


    maxdutycycletable = [
    5,
    10,
    15,
    20,
    25,
    30,
    35,
    40,
    45,
    44.5,
    44.5,
    44.4,
    44.8,
    44.8,
    44.9,
    44.8,
    44.3,
    44.1,
    44.5,
    45,
    44.2,
    44,
    44.8,
    44.3,
    45,
    44.2,
    44.6,
    44.8,
    44.9,
    43.6,
    45,
    44.8,
    44.6,
    44.1,
    43.9,
    43.2,
    44,
    43.8,
    44.7,
    44,
    44.9,
    44.2,
    43,
    44,
    44.9,
    43.7,
    43.7,
    43.4,
    43.9,
    40,
    38.5,
    39,
    40,
    37.8,
    38.4,
    39.4,
    40,
    37.7,
    38.2,
    38.8,
    39.4,
    40,
    37.5,
    38.1,
    38.7,
    39.3,
    40,
    37.3,
    37.9,
    38.6
    ]

    dutycyclelength = len(maxdutycycletable)

    def ReadFaults(self):

        sendresponse = self.Send(self.param_read_faults,self.long_to_bytes(3,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated


    def ReadWarnings(self):

        sendresponse = self.Send(self.param_read_warnings,self.long_to_bytes(4,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def EnableFixedPulsingMode(self):

        sendresponse = self.Send(self.param_pulse_type,[0x1],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        print("fixed mode" + str(translated))
        return translated

    def EnableCurrentThresholdPulsingMode(self):

        sendresponse = self.Send(self.param_pulse_type,[0],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        print("threshold mode current" + str(translated))
        return translated
    
    def DisablePulsingMode(self):
        sendresponse = self.Send(self.param_set_pulse_frequency,self.long_to_bytes(0,1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        
        return translated
        pass

    def SetPulseFrequency(self,kHzFreq):

        # bound between min and max
        kHzFreq  = int(self.BoundInput(kHzFreq,self.minimum_frequency,self.maximum_frequency))
        
        frequencyindex = self.CalculateFrequencyIndex(kHzFreq)

        sendresponse = self.Send(self.param_set_pulse_frequency,self.long_to_bytes(int(frequencyindex),1),self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        

        return translated

    def ReadPulseFrequency(self):

        sendresponse = self.Send(self.param_read_pulse_frequency,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        print(translated)

        return int(translated * self.frequency_index_step)

    def ReadReverseTime(self):

        sendresponse = self.Send(self.param_read_reverse_time,[],self.defaultAddress)
        translated = self.hex_to_long(sendresponse.hex())
        return translated

    def SetReverseTime(self,reverseTimeMicroseconds,kHzFreq):

        kHzFreq = self.BoundInput(kHzFreq,0,self.dutycyclelength)
        index = self.CalculateFrequencyIndex(kHzFreq)
        reverseTimeHundredNanoseconds = int(self.BoundInput(reverseTimeMicroseconds,.4,self.maxreversetable[index])*10)
         
        self.Send(self.param_set_reverse_time,self.long_to_bytes(reverseTimeHundredNanoseconds,1),self.defaultAddress)
    
    
         

    def CalculateFrequencyIndex(self,kHzFreq):
        
        # divide by frequency step and cast to integer
        print("FrequencyIndex = " + str(int(kHzFreq/self.frequency_index_step)))
        return int(kHzFreq/self.frequency_index_step)

    def CalculateMaximumDutyCycle(self,kHzFreq):

        frequencyindex = self.CalculateFrequencyIndex(kHzFreq)

        return self.maxdutycycletable[frequencyindex]
        
    def CalculateMinimumDutyCycle(self,kHzFreq):

        # minimum duty cycle is minimum reverse time/period in percent

        return float(self.minimum_reverse_time * kHzFreq/10)

