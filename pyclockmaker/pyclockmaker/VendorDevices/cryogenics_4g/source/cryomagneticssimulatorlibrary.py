'''
CryomagneticsSimulatorLibrary defines the cryomagnetics 4G supply simulation.
It is primarily concerned around interfacing via the ethernet socket interface. 
Thus it requires socket to be imported and set up.
'''

from datetime import datetime
import traceback
import socket
import time
import sys

class SuperconductingMagnet():

    Quenched = False
    ResistanceWhenQuenched = 2
    ResistanceWhenSuperconducting = 0.001
    Resistance = ResistanceWhenSuperconducting
    PersistentHeater = False
    CurrentInMagnet = 0
    MagnetState = ''

    def __init__(self):

        self.MagnetState = 'Init'
        

    def GetVoltage(self):

        return self.CurrentInMagnet*self.Resistance

    def SetCurrent(self,value):

        self.CurrentInMagnet = value

    def GetCurrent(self):

        return self.CurrentInMagnet

    def GetPersistentHeater(self):

        return self.PersistentHeater

    def EnablePersistentHeater(self):

        self.PersistentHeater = True

    def DisablePersistentHeater(self):

        self.PersistentHeater = False
        
        #TODO: handle time for heatup/cooldown


class Cryomagnetics4GSimulator():

    writeterminationcharacter = bytes('\r\n',"ascii")
    readterminationcharacter = bytes('\r',"ascii")
    configuration = 'ethernet'
    IPAddress = '192.168.0.130'
    IPPort = 4444
    SerialPort = '/dev/tty_Cryomagnetics4G'
    GPIBAddress = 3
    maxcurrent = 100
    heaterdelay = 30
    querycharacter = '?'
    debug = True
    localIPPort = 1214
    commandDelay = .01
    PersistentCurrentSetting = 0
    ModuleSelected = 1
    ErrorResponse = False # always treating as false for now
    LLIMCurrent = 0.0
    ULIMCurrent = 0.0
    OperatingMode = 'LOCAL'
    NAME = 'SIMULATOR'
    RangeLimit = [25,50,75,101]
    SweepRate = [1,1,1,1,25]
    SweepMode = 'PAUSED'
    SweepFastMode = False
    VoltageLimit = 10
    LastSweepTime = datetime.now()
    SupplyCurrent = 0.0
    SupplyVoltage = 0.0
    

    def __init__(self):

        print(str(datetime.now())+ ": Initializing Cryomagnetics4G Magnet Supply Simulator.")
        self.Magnet = SuperconductingMagnet()
        

        if self.configuration == 'ethernet':
            print("Initializing ethernet.")
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.bind(('localhost', self.IPPort))
            self.sock.listen(1)
            while True:
                self.conn, self.addr = self.sock.accept()
                
    
                try:
                    print('Connected by '+ str( self.addr))
                    
                    while True:
                        print("Waiting for data.")
                        data = self.conn.recv(1024)
                        
                        if not data: 
                            print("Data error.")
                            raise Exception

                        else:
                            
                            self.HandleInput(data)
                            #conn.sendall(response)
                            
                except:
                    self.sock.close()
                    traceback.print_exc()

        
            
    def get_ip_address(self):
        # This is a pretty baller way to get the IP of an interface.
        # Taken from https://stackoverflow.com/questions/24196932/how-can-i-get-the-ip-address-from-nic-in-python
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(("8.8.8.8", 80))
        return s.getsockname()[0]

    def BoundInput(self,value,low_value=0, high_value=0):

        if value>high_value:
            
            return high_value
        
        elif value<low_value:
            
            return low_value

        else:
            return value

    def IntegerCasting(self,value,length):

        # casts to int and truncates length of integer.
        # example - a 3 digit allowed integer must be between 999 and 0.
        # thus, 0<= value <10^3.
        # generalized 0=< value < 10^n for length n.

        intermediate = int(value)

        if intermediate <0:
            
            return 0

        elif intermediate >= 10**(length):

            return int((10**length) -1)

        else:

            return intermediate

    def ThrowError(self):

        print(str(datetime.now())+ ": Error")
        if self.ErrorResponse:

            self.WriteString('ERROR')

    def GPIBWrite(self,value):
        pass

    def RS232Write(self,value):
        pass

    def EthernetWrite(self,value):

        # build the write string
        
        toSend = bytearray()
        
        bytearraystringvalue = bytes(value,"ascii")
        
        toSend.extend(bytearraystringvalue)
        
        toSend.extend(self.writeterminationcharacter)  
        #print(str(toSend))
        self.conn.sendall(toSend)
        
        time.sleep(self.commandDelay)
        

    def WriteString(self,value):

    ## Write the string and read back until the correct newline character is found.

        if self.configuration=='GPIB':

            return self.GPIBWrite(value)

        elif self.configuration=="RS232":

            return self.RS232Write(value)

        else:

            return self.EthernetWrite(value)



    def SelectModule(self,value=1):
        
        '''
        CHAN 
        Set the module for subsequent remote commands 
        Availability:                   Remote Mode                  
        Command Syntax:CHAN [module number] Example:                       CHAN                       2                       
        Default Parameter:       none       
        Parameter Range:  1 to 2 Description: The CHAN command  selects  the  module  for  subsequent  remote  commands.   
        A  command error is returned if only one module is installed. 
        Related Commands:   CHAN? 
        '''
        if value==1 or value==2:
            self.ModuleSelected = value
        else:
            self.ThrowError()

    def QueryModule(self):

        '''
        CHAN?
        Query the currently selected module  
        Availability:                   Always                   
        Command Syntax:       CHAN?       
        Response:<currently selected module> 
        Response Example: 2 
        Description:    The CHAN?  query  returns  the  power  module  currently  selected  for  remote  commands.  
        It returns 1 or 2.  A command error is returned if only one module is installed. 
        Related Commands:    CHAN
        '''
        #print("before write")
        what = self.WriteString(str(self.ModuleSelected))
        #print("after write")
        return what
        
    def SetErrorResponseMode(self,value):
       
        '''
        ERROR
        Set error response mode for USB interface 
        Availability:                   Remote                   Mode                   
        Command Syntax:ERROR <Error Mode> 
        Example:                       ERROR                       1                       
        Parameter Range:0 or 1    (0 - disable error reporting, 1 - enable error reporting) 
        Description:  The ERROR command enables or disables error messages when the USB interface is used.  
        It is much easier to handle errors under program control when using the USB interface if error messages are disabled, 
        but it is desirable to enable error messages if a terminal program is used to interactively control and query the 4G. 
        Related Commands:  ERROR? 
        '''
        if value==0 or value == 1:
            self.ErrorResponse = bool(value)
        else:
            self.ThrowError()
        

    def QueryErrorResponseMode(self):

        '''
        ERROR?
        Query error response mode 
        Availability:                   Always                   
        Command Syntax:       ERROR?                  
        Response:                    <Error                    Mode>                    
        Response Example: 0  
        Response Range:  0 or 1 
        Description:  The ERROR? query returns the selected error reporting mode. 
        Related Commands:   ERROR 
        '''
        return self.WriteString(str(int(self.ErrorResponse)))

    def SetMagnetPersistentCurrent(self,value,shim=False,fieldmode=False):
        '''
        IMAG
        Sets the magnet current (or magnetic field strength).   
        Availability:                   Remote                   Mode                   
        Command Syntax:       IMAG       [value]       
        IMAG <shim_id> <value>   (shim mode only) 
        Example:                       IMAG                       47.1123                       
        IMAG X2 -13.4571  (shim mode only) 
        Default Parameter:       0.0       
        Parameter  Range:+/-Maximum  Magnet  Current
        Description: The IMAG command sets the magnet current shown on the display.  The supply must be  in  standby  or  a  command  error  will  be  returned.    
        The  value  must  be  supplied  in  the  selected  units  - amperes or field (kG).  
        If Shim Mode is enabled, the persistent mode current displayed for the named shim is set if the shim parameter is provided. 
        Related Commands:    IMAG?
        '''


        #TODO: Add shim mode functionality

        if shim and fieldmode:

            pass

        elif shim and not fieldmode:

            pass

        elif fieldmode and not shim:

            pass

        else:

            #TODO: handle field/current mode correctly.

            boundinputvalue = self.BoundInput(float(value),0,self.maxcurrent)
            self.SupplyCurrent = boundinputvalue

            if bool(self.Magnet.GetPersistentHeater()):

                self.Magnet.SetCurrent(self.SupplyCurrent)


    def QueryMagnetCurrent(self,shimmode=False,):

        '''
        IMAG?
        Query magnet current  (or magnetic field strength) 
        Availability:                   Always                  
        Command Syntax:       IMAG?       
        Response:<Magnet Current> <Units> 
        Response Example: 87.9350 A 
        Description:    The IMAG?  query  returns  the  magnet  current  (or  magnetic  field  strength)  in  the  present units.   
        If the persistent switch heater is ON the magnet current returned will be the same as the power supply output current.  
        If the persistent switch heater is off, the magnet current will be the value of the power supply output current when the persistent switch heater was last turned off.  
        The magnet current  will  be  set  to  zero  if  the  power  supply  detects  a  quench.    
        If  in  SHIM  mode,  the  IMAG? query reports the present current of the shim selected by the SHIM command in Amps.  
        If the optional Shim ID is provided while in shim mode, the present current of the specified shim will be reported. 
        Related Commands:   UNITS, UNITS?
        '''

        return self.WriteString(str(self.Magnet.GetCurrent())+ " A")
        


    def QueryOutputCurrent(self):

        '''

        IOUT?
        Query power supply output current
        Availability:   Always                   
        Command Syntax:       IOUT?       
        Response:<Output Current> <Units> Response Example: 87.935 A
        Description:  The IOUT? query returns the power supply output current (or magnetic field strength) in the present units.  
        Related Commands:   UNITS, UNITS?

        '''
        
        return self.WriteString(str(self.SupplyCurrent)+ " A")
        

        


    def SetCurrentSweepLowerLimit(self,value):


        '''
        LLIM
        Set current sweep lower limit Availability:                   Remote                   Mode                   
        Command Syntax:       LLIM       [Limit]
        Example:                       LLIM                       20.1250                       
        Default Parameter:       0.0       
        Parameter  Range:+/-Maximum  Magnet  Current
        Description: The LLIM  command  sets  the  current  limit  used  when  the  next  SWEEP  DOWN  command is issued.  The value must be supplied in the selected units - amperes or field (kG).  An error will be returned if this value is greater than the upper sweep limit. 
        Related Commands:    LLIM?, ULIM, ULIM?, SWEEP, SWEEP?, UNITS, UNITS? 
        '''

        #TODO: handle field/current mode correctly. This naively assumes the incoming value is in the same unit system as the selected mode.

        boundinputvalue = self.BoundInput(value,-self.maxcurrent,self.maxcurrent)

        self.LLIMCurrent = float(boundinputvalue)
        

    def QueryCurrentSweepLowerLimit(self):

        '''

        LLIM?
        Query current sweep lower limit 
        Availability:                   Always  
        Command Syntax:       LLIM?       
        Response:                    <Limit>                    <Units>                    
        Response Example: 20.1250 A Response Range:  +/-Maximum Magnet 
        Current Description:  The LLIM? query returns the current limit used with the SWEEP DOWN command.  It is issued in the selected units - amperes or field (kG). 
        Related Commands:    LLIM, ULIM, ULIM?, SWEEP, SWEEP?, UNITS, UNITS? 

        '''
        return self.WriteString(str(self.LLIMCurrent) + " A")


    def SetLocalMode(self):

        '''
        LOCAL 
        Return control to front panel 
        Availability:Always (USB and Ethernet Only) 
        Command Syntax:       LOCAL       
        Description:    The LOCAL command  returns  control  the  front  panel  keypad  after  remote  control  has been selected by the REMOTE or RWLOCK commands. 
        Related Commands:   REMOTE, RWLOCK 
        '''

        self.OperatingMode = 'LOCAL'
        

    def QueryOperatingMode(self):

        '''
        MODE?
        Query selected operating mode
        Availability:                   Always 
        Command Syntax:       MODE?       
        Response:                    <Operating                    Mode>                    
        Response Example: Manual Response 
        Range:  Shim or Manual Description:  The MODE? command returns the present operating mode.   
        Related Commands:  MODE 

        '''

        return self.WriteString(str(self.OperatingMode))

        

    def SetName(self,value):


        '''
        NAME 
        Sets the name of the coil (magnet) on the display. 
        Availability:                   Remote                   Mode                   
        Command Syntax: NAME <name string> 
        Example:NAME GUN COIL 
        Default Parameter:       None       
        Parameter Range:  0 to 16 characters Description: The NAME  command  sets  the  name  of  the  currently  selected  module  for  display.    Upper and lower case is accepted, however the string is converted to upper case.
        Related Commands:    NAME?
        '''
        if len(value)<17:
            self.NAME = value
        else:
            self.ThrowError()

        

    def QueryName(self):

        '''
        NAME?
        Query the name of the currently selected coil (magnet)
        Availability:                   Always
        Command Syntax:       NAME?       
        Response:0 to 16 characters 
        Description:  The NAME? query returns the name of the currently selected module. 
        Related Commands:    NAME
        '''
        return self.WriteString(str(self.NAME))


    def ControlPersistentSwitchHeater(self,value):

        '''
        PSHTR 
        Control persistent switch heater
        Availability:                   Remote                   Mode                   
        Command Syntax: PSHTR <State> 
        Example:                       PSHTR                       ON                       
        Default Parameter:       None       
        Parameter Range:  On or Off 
        Description: The PSHTR  command  turns  the  persistent  switch  heater  on  or  off.    Note  that  the  switch heater current can only be set in the Magnet Menu using the keypad.  This command should normally  be  used  only  when  the  supply  output  is  stable  and  matched  to  the  magnet  current.    If  in  Shim Mode, the heater for the selected shim is controlled instead of the main switch heater.
        Related Commands:    PSHTR?
        '''


        if ord(value[-1])==13:

            value = value[:-1]


        if value=='ON':
            self.Magnet.EnablePersistentHeater()
        elif value=='OFF':
            self.Magnet.DisablePersistentHeater()
        else:
            raise Exception
        

    def QueryPersistentSwitchHeaterState(self):

        '''
        PSHTR?
        Query persistent switch heater state 
        Availability:                   Always                   
        Command Syntax:       PSHTR?       
        Response:0 or 1 
        Description:    The PSHTR? query  returns  1  if  the  switch  heater  is  ON  or  0  if  the  switch  heater  is  OFF.  If in Shim Mode, the status of the switch heater for the selected shim is returned. 
        Related Commands:    PSHTR QRESET Reset Quench Condition

        '''
        return self.WriteString(str(int(self.Magnet.GetPersistentHeater())))


    def ResetQuenchCondition(self):
        
        '''
        QRESET 
        Reset Quench Condition
        CAvailability:                   Remote                   Mode                   
        Command Syntax: QRESET 
        Description: The QRESET  command  resets  a  power  supply  quench  condition  and  returns  the  supply to STANDBY.
        Related Commands:    None
        '''
        #nothing to do here right now
        pass
        

    def SetRangeLimit(self,rangeselect,value):
        '''
        RANGE
        Set range limit for sweep rate boundary 
        Availability:                   Remote                   
        Command Syntax:RANGE <Select> <Limit> 
        Example:RANGE 0 25.0 
        Default Parameter:       None       
        Parameter Ranges:
        Range Selection:   0 to 4 
        Limit:   0 to Max Supply Current
        '''
        rangeselect = int(rangeselect)
        if rangeselect>4 or rangeselect<0:
            raise Exception
        if value>self.maxcurrent or value<0:
            raise Exception

        self.RangeLimit[rangeselect]=value

    def QueryRangeLimit(self,rangeselect):

        '''
        RANGE?
        Query range limit for sweep rate boundary 
        Availability:                   Always                   
        Command Syntax:       RANGE?       <Select>       
        Example:                       RANGE?                       1                                              
        Parameter Range: 0 to 4 
        Response:                    <Limit>                    
        Response Example: 75.000 
        Response Range: 0 to Max Magnet Current 
        Description:    The RANGE? query  returns  the  upper  limit  for  a  charge  rate  range  in  amps.    See  RANGE for further details. 
        Related Commands:   RANGE, RATE, RATE? 
        '''
        rangeselect = int(rangeselect)
        return self.WriteString(str(float(self.RangeLimit[rangeselect])))
        

    def SetSweepRate(self,rangeselect,value):

        '''
        RATE
        Set sweep rate for selected sweep range 
        Availability:                   Remote                   Command 
        Syntax:RATE <Range> <Sweep Rate> 
        Example:RATE 0 0.250 
        Default Parameter:       None       
        Parameter Ranges:
        Range Selection:  0 to 5 
        Limit:     0 to Max Magnet Current 
        Description: The RATE command  sets  the  charge  rate  in  amps/second  for  a  selected  range.    
        A  range parameter of 0, 1, 2, 3, and 4 will select Range 1, 2, 3, 4, or 5 sweep rates as displayed in the Rates Menu.  
        A range parameter of 5 selects the Fast mode sweep rate. 
        Related Commands:   RANGE, RANGE?, RATE?
        '''
        rangeselect = int(rangeselect)
        if rangeselect>5 or rangeselect<0:
            raise Exception
        if value>self.maxcurrent or value<0:
            raise Exception

        self.SweepRate[rangeselect]=value

    def QuerySweepRate(self, rangeselect):


        '''
        RATE?
        Query range limit for sweep rate boundary 
        Availability:                   Always                   
        Command Syntax:       RATE?       <Range>       
        Example:                       RATE?                       1                                              
        Parameter Range: 0 to 5 
        Response:                    <Rate>                    
        Response Example: 0.125 
        Response Range: 0 to Max Magnet Current 
        Description: The RATE? command queries the charge rate in amps/second for a selected range.  
        A  range  parameter  of  0  to  4  will  select  Range  1  through  5  sweep  rates  as  displayed  in  the  Rates  Menu.  
        A range parameter of 5 queries the Fast mode sweep rate. 
        Related Commands:   RANGE, RANGE?, RATE
        '''
        rangeselect = int(rangeselect)
        return self.WriteString(str(float(self.SweepRate[rangeselect])))

        

    def SetRemoteMode(self):

        '''
        REMOTE
        Select remote operation
        Availability: Operate (USB Only) 
        Command Syntax:       REMOTE       
        Description:  The REMOTE command takes control of the 4G via the remote interface.  All buttons on the front panel are disabled except the Local button.  
        This command will be rejected if the menu system is being accessed via the front panel or if LOCAL has been selected via the Local button on the  front  panel.    
        Pressing  the  Local  button  again  when  the  menu  is  not  selected  will  allow  this  command to be executed.  
        This command is only necessary for USB operation since the IEEE-488 interface provides for bus level control of the Remote and Lock controls. 
        Related Commands:   LOCAL, RWLOCK
        '''

        self.OperatingMode ='REMOTE'
        

    def SetRemoteWithLockMode(self):

        '''
        RWLOCK
        Select remote operation
        Availability:Operate (USB Only) 
        Command Syntax:       RWLOCK      
        Description:  The REMOTE command takes control of the 4G via the remote interface.  All buttons on the front panel are disabled except the Local button.  
        This command will be rejected if the menu system is being accessed via the front panel or if LOCAL has been selected via the Local button on the  front  panel.    
        Pressing  the  Local  button  again  when  the  menu  is  not  selected  will  allow  this  command to be executed.  
        This command is only necessary for USB operation since the IEEE-488 interface provides for bus level control of the Remote and Lock controls. 
        Related Commands:   LOCAL, RWLOCK 
        '''
        
        self.OperatingMode = 'RWLOCK'

    def SelectShim(self,value):


        # SHIM
        pass

    def QueryShimSelected(self):


        response = self.WriteString('SHIM?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def SetShimCurrentLimit(self,value):

        
        # SLIM
        pass

    def QueryShimCurrentLimit(self):

        response = self.WriteString('SLIM?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def StartOutputCurrentSweep(self,value):

        '''
        SWEEP
        Start output current sweep 
        Availability:                   Remote                   Mode                   
        Command Syntax: SWEEP <Sweep Mode> [fast or slow] 
        Examples:                     SWEEP                     UP                     
        SWEEP UP FAST 
        Default Parameter: None 
        Parameter Range:  UP, DOWN, PAUSE, or ZERO 
        Shim Mode Only: LIMIT
        Description:    The SWEEP command  causes  the  power  supply  to  sweep  the  output  current  from  the  present  current  to  the  specified  limit  at  the  applicable  charge  rate  set  by  the  range  and  rate  commands. If  the  FAST  parameter  is  given,  the  fast  mode  rate  will  be  used  instead  of  a  rate  selected from the output current range.  SLOW is required to change from fast sweep.  SWEEP UP sweeps  to  the  Upper  limit,  SWEEP  DOWN  sweeps  to  the  Lower  limit,  and  SWEEP  ZERO  discharges the supply.  If in Shim Mode, SWEEP LIMIT sweeps to the shim target current. 
        Related Commands:   LLIM, LLIM?, SLIM, SLIM?, SWEEP?, ULIM, ULIM?, UNITS, UNITS?
        '''
        if ord(value[-1])==13:

            value = value[:-1]

        # handle parsing
        if len(value.split(' ',3))==2:

            if value.split(' ',3)[1] == 'FAST':
                
                self.SweepFastMode = True

            elif value.split(' ',3)[1] == 'SLOW': 

                self.SweepFastMode = False
            
            else:

                self.ThrowError()

        if value.split(' ',3)[0] == 'UP':

            self.SweepMode = 'UP'
            print("UP!!!!")

        elif value.split(' ',3)[0] == 'DOWN':

            self.SweepMode = 'DOWN'

        elif value.split(' ',3)[0] == 'PAUSE':
            
            self.SweepMode = 'PAUSE'
            print("PAUSEDDDDDD")
        
        elif value.split(' ',3)[0] == 'ZERO':
            
            self.SweepMode = 'ZERO'

        else:

            self.ThrowError()

    def QuerySweepMode(self):

        '''
        SWEEP?
        Query sweep mode 
        Availability:                   Always                   
        Command Syntax:       SWEEP?       
        Response:                    <Mode>                    [fast]                    
        Response Example: sweep up fast Response  Range:    sweep  up,  sweep  down,  sweep paused, or zeroing 
        Description:    The SWEEP? query  returns  the  present  sweep  mode.    If  sweep  is  not  active  then  'sweep paused' is returned. 
        Related Commands:   LLIM, LLIM?, SLIM, SLIM?, SWEEP, ULIM, ULIM?, UNITS, UNITS?
        '''

        if self.SweepFastMode:

            return self.WriteString(str(self.SweepMode) + " FAST")

        
        else:

            return self.WriteString(str(self.SweepMode))

    def SetCurrentSweepUpperLimit(self,value):
        '''
        ULIM
        Set current sweep upper limit 
        Availability:                   Remote                   Mode                   
        Command Syntax:       ULIM       [Limit]       
        Example:                       ULIM                       65.327                       
        Default Parameter:       0.0       Parameter  Range:+/-Maximum  Supply  Current
        Description: The ULIM command sets the current limit used when the next SWEEP UP command is issued.  The value must be supplied in the selected units - amperes or field (kG).  An error will be returned if this value is less than the lower sweep limit. 
        Related Commands:   LLIM, LLIM?, SWEEP, SWEEP?, ULIM?, UNITS, UNITS?
        '''

        self.ULIMCurrent = float(value)
        

    def QueryCurrentSweepUpperLimit(self):

        '''
        ULIM?
        Query current sweep upper limit 
        Availability:                   Always                   
        Command Syntax:       ULIM?       
        Response:                    <Limit>                    <Units>                    
        Response Example: 65.327 A 
        Response Range:  +/-Maximum              Supply              
        Current Description:  The ULIM? query returns the current limit used for the SWEEP UP command.  It is issued in the selected units - amperes or field (kG). 
        Related Commands:   LLIM, LLIM?, SWEEP, SWEEP?, ULIM, UNITS, UNITS?
        '''


        return self.WriteString(str(self.ULIMCurrent)+" A")

        

    def SetUnits(self,value='A'):
        '''
        UNITS                            
        Select                            units                            
        Availability:                   Remote                   Mode                   
        Command Syntax:UNITS <Unit Selection> 
        Example:                       UNITS                       A                                                                     
        Parameter Range:  A, G Description:  The UNITS command sets the units to be used for all input and display operations.  Units may be set to Amps or Gauss.  The unit will autorange to display Gauss, Kilogauss or Tesla. 
        Related Commands:  IMAG?, IOUT?, LLIM, LLIM?, ULIM, ULIM?, UNITS?
        '''
        
        # DOES NOTHING
        pass
        

    def QueryUnits(self):


        '''
        UNITS?
        Query selected units 
        vailability:                   Always                   
        Command Syntax:       UNITS?       
        Response:                    <Selected                    Units>                    
        Response Example: G Response Range: A, G 
        Description:  The UNITS? command returns the units used for all input and display operations.  
        Related Commands:  IMAG?, IOUT?, LLIM, LLIM?, ULIM, ULIM?, UNITS 
        '''

        # ALWAYS RESPONDS CURRENT
        return self.WriteString("A")
        

    def SetVoltageLimit(self,value):


        '''
        VLIM 
        Set voltage limit 
        Availability:                   Remote                   Mode                  
        Command Syntax:VLIM <Voltage Limit> Example:                       VLIM                       5.0
        Parameter Range:  0.0 to 10.0 
        Description:    The VLIM  command  sets  the  power  supply  output  voltage  limit  to  the  voltage  provided.  
        Related Commands: VLIM?, VMAG?, VOUT? 
        '''
        self.VoltageLimit = float(value)
        

    def QueryVoltageLimit(self):

        '''
        VLIM?
        Query voltage limit Availability:                   Always                  
        Command Syntax:       VLIM?       
        Response:                    <Voltage                    Limit>                    
        Response Example: 4.75 V 
        Response Range: 0 to 10.00 
        Description:  The VLIM? command returns the power supply output voltage limit.  
        Related Commands: VLIM, VMAG?, VOUT?
        '''

        return self.WriteString(str(self.VoltageLimit)+ " V")

    def QueryMagnetVoltage(self):


        '''
        VMAG?
        Query magnet voltage 
        Availability:                   Always                   
        Command Syntax:       VMAG?       
        Response:                    <Magnet                    Voltage>                    
        Response Example: 4.75 V Response Range: -10.00 to +10.00 
        Description:  The VMAG? command returns the present magnet voltage.  
        Related Commands: VLIM, VLIM?, VOUT?
        '''

        return self.WriteString(str(self.Magnet.GetVoltage())+" V")

    def QueryOutputVoltage(self):

        '''
        VOUT?Query output voltage 
        Availability:                   Always                   
        Command Syntax:       VOUT?       
        Response:                    <Output                    Voltage>                    
        Response Example: 4.75 V Response Range: -12.80 to +12.80 
        Description:  The VOUT? command returns the present power supply output voltage.  
        Related Commands:  VLIM, VLIM?, VMAG? 
        '''

        if self.Magnet.GetPersistentHeater():
            return self.WriteString(str(self.Magnet.GetVoltage())+" V")
        else:
            return self.WriteString("0 V")

# The following appears to not be accessible via Ethernet and are included only for completion.

    def ClearStatusCommand(self):


        #*CLS
        pass

    def CommandStandardEventStatusEnable(self,value):


        #*ESE
        pass

    def QueryStandardEventStatusEnable(self):

        response = self.WriteString('*ESE?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryStandardEventStatusRegister(self):

        response = self.WriteString('*ESR?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryIdentification(self):

        response = self.WriteString('*IDR?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandOperationComplete(self):


        #*OPC
        pass

    def QueryOperationComplete(self):

        response = self.WriteString('*OPC?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandReset(self):

        response = self.WriteString('*RST')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandServiceRequestEnable(self):


        #*SRE
        pass

    def QueryServiceRequestEnable(self):

        response = self.WriteString('*SRE?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QueryReadStatusByte(self):

        response = self.WriteString('*STB?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def QuerySelfTest(self):

        response = self.WriteString('*TST?')

        # parse response
        # TODO: check handling on a bad readback
        return response.split(' ',1)[0]

    def CommandWaitToContinue(self):


        #*WAI
        pass

        


    def HandleInput(self,receivedbytearray):

        # Look for termination characted at end of string. If not, return NOTHING.
        receiveddata = receivedbytearray.decode()
        if receiveddata[-1] != self.readterminationcharacter.decode():

            print('No termination char.')
            return ''
            
        else:

            print(receiveddata)
            #strip readline handling.
            if ord(receiveddata[-1])==13:

                receiveddata = receiveddata[:-1]
            # grab the first word.
            command = receiveddata.split(' ',3)[0]

            if ord(command[-1])==13:

                command = command[:-1]



            #for i in range(len(command)):
#
            #   print(str(command[i])+": "+str(ord(command[i])))

            self.HandleSweep()
            #big ol statemachine time.
            try:
                
                if command == "CHAN":

                    self.SelectModule(int(receiveddata.split(' ',3)[1]))

                elif command == "CHAN?":
                    self.QueryModule()

                elif command == "ERROR":

                    self.SetErrorResponseMode(int(receiveddata.split(' ',3)[1]))
                
                elif command == "ERROR?":

                    self.QueryErrorResponseMode()

                elif command == "IMAG":

                    self.SetMagnetPersistentCurrent(float(receiveddata.split(' ',3)[1]))
                
                elif command == "IMAG?":

                    self.QueryMagnetCurrent()

                elif command == "IOUT?":

                    self.QueryOutputCurrent()

                elif command == "LLIM":
                    
                    self.SetCurrentSweepLowerLimit(float(receiveddata.split(' ',3)[1]))

                elif command == "LLIM?":

                    self.QueryCurrentSweepLowerLimit()

                elif command == "LOCAL":

                    self.SetLocalMode()

                elif command == "MODE?":

                    self.QueryOperatingMode()

                elif command == "NAME":

                    self.SetName(receiveddata.split(' ',3)[1])
                
                elif command == "NAME?":

                    self.QueryName()

                elif command == "PSHTR":

                    self.ControlPersistentSwitchHeater(str(receiveddata.split(' ',3)[1]))

                elif command == "PSHTR?":

                    self.QueryPersistentSwitchHeaterState()

                elif command == "QRESET":

                    self.ResetQuenchCondition()

                elif command == "RANGE":

                    self.SetRangeLimit(int(receiveddata.split(' ',3)[1]),float(receiveddata.split(' ',3)[2]))
                
                elif command == "RANGE?":

                    self.QueryRangeLimit(int(receiveddata.split(' ',3)[1]))

                elif command == "RATE":

                    self.SetSweepRate(int(receiveddata.split(' ',3)[1]),float(receiveddata.split(' ',3)[2]))

                elif command == "RATE?":

                    self.QuerySweepRate(int(receiveddata.split(' ',3)[1]))

                elif command == "REMOTE":

                    self.SetRemoteMode()

                elif command == "RWLOCK":

                    self.SetRemoteWithLockMode()

                elif command == "SWEEP":

                    self.StartOutputCurrentSweep(str(receiveddata.split(' ',1)[1]))

                elif command == "SWEEP?":

                    self.QuerySweepMode()

                elif command == "ULIM":

                    self.SetCurrentSweepUpperLimit(float(receiveddata.split(' ',1)[1]))

                elif command == "ULIM?":

                    self.QueryCurrentSweepUpperLimit()

                elif command == "UNITS":

                    self.SetUnits()

                elif command == "UNITS?":

                    self.QueryUnits()

                elif command == "VLIM":

                    self.SetVoltageLimit(receiveddata.split(' ',1)[1])

                elif command == "VLIM?":

                    self.QueryVoltageLimit()

                elif command == "VMAG?":

                    self.QueryMagnetVoltage()

                elif command == "VOUT?":

                    self.QueryOutputVoltage()

                else:
                    print('NoMatch.')
                    self.ThrowError()

            except:
                traceback.print_exc()
                self.ThrowError()


    def HandleSweep(self):

        # handles sweep conditions.

        currenttime = datetime.now()
        time_delta = currenttime - self.LastSweepTime

        # determine whether we are actually sweeping, and if so, what we're sweepint.

        if self.Magnet.GetPersistentHeater():
            print(self.SweepMode)
            # handle up-down-pause
            if self.SweepMode == "PAUSED":

                pass

            elif self.SweepMode == "UP":

                
                if self.SweepFastMode:
                    newcurrent = time_delta.total_seconds()*self.SweepRate[4]+self.Magnet.GetCurrent()
                
            
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.Magnet.GetCurrent())
                    
                    # set new current
                    newcurrent = float(time_delta.total_seconds()*self.SweepRate[rangeindex])+self.Magnet.GetCurrent()
                    print(newcurrent)

                # check for current limit and end sweep if required
                print(newcurrent)
                if newcurrent > self.ULIMCurrent or newcurrent > self.maxcurrent:

                   
                    self.Magnet.SetCurrent(self.ULIMCurrent)
                    self.SupplyCurrent = self.Magnet.GetCurrent()
                    self.SweepMode = "PAUSED"

                else: 

                    self.Magnet.SetCurrent(newcurrent)
                    self.SupplyCurrent = self.Magnet.GetCurrent()
            
            elif self.SweepMode == "DOWN":

                if self.SweepFastMode:
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[4]+self.Magnet.GetCurrent())
                
            
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.Magnet.GetCurrent())
                    
                    # set new current
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[rangeindex])+self.Magnet.GetCurrent()

                # check for current limit and end sweep if required

                if newcurrent < self.LLIMCurrent:

                    self.Magnet.SetCurrent(self.LLIMCurrent)
                    self.SupplyCurrent = self.Magnet.GetCurrent()
                    self.SweepMode = "PAUSED"

                else: 

                    self.Magnet.SetCurrent(newcurrent)
                    self.SupplyCurrent = self.Magnet.GetCurrent()

            elif self.SweepMode == "ZERO":

                if self.SweepFastMode:
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[4]+self.Magnet.GetCurrent())
                
            
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.Magnet.GetCurrent())
                    
                    # set new current
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[rangeindex])+self.Magnet.GetCurrent()

                # check for current limit and end sweep if required

                if newcurrent < 0:

                    self.Magnet.SetCurrent(0)
                    self.SupplyCurrent = self.Magnet.GetCurrent()
                    self.SweepMode = "PAUSED"

                else: 

                    self.Magnet.SetCurrent(newcurrent)
                    self.SupplyCurrent = self.Magnet.GetCurrent()




            self.LastSweepTime = currenttime

        else:
        
            print(self.SweepMode)
            # handle up-down-pause
            if self.SweepMode == "PAUSED":

                pass

            elif self.SweepMode == "UP":

                
                if self.SweepFastMode:
                    newcurrent = time_delta.total_seconds()*self.SweepRate[4]+self.SupplyCurrent
                
            
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.SupplyCurrent)
                    
                    # set new current
                    newcurrent = float(time_delta.total_seconds()*self.SweepRate[rangeindex])+self.SupplyCurrent
                    print(newcurrent)

                # check for current limit and end sweep if required
                print(newcurrent)
                if newcurrent > self.ULIMCurrent or newcurrent > self.maxcurrent:

                
                    self.SupplyCurrent(self.ULIMCurrent)
                    self.SweepMode = "PAUSED"

                else: 
                    self.SupplyCurrent = newcurrent
            
            elif self.SweepMode == "DOWN":

                if self.SweepFastMode:
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[4]+self.SupplyCurrent)
                
            
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.SupplyCurrent)
                    
                    # set new current
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[rangeindex])+self.SupplyCurrent

                # check for current limit and end sweep if required

                if newcurrent < self.LLIMCurrent:

                    self.SupplyCurrent = self.LLIMCurrent
                    self.SweepMode = "PAUSED"

                else: 

                    self.SupplyCurrent = newcurrent

            elif self.SweepMode == "ZERO":

                if self.SweepFastMode:
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[4])+self.SupplyCurrent
                else:

                    # find the index of the range that is larger than the current
                    rangeindex = next(x for x, val in enumerate(self.RangeLimit)
                                            if val > self.SupplyCurrent)
                    
                    # set new current
                    newcurrent = float(-1*time_delta.total_seconds()*self.SweepRate[rangeindex])+self.SupplyCurrent

                # check for current limit and end sweep if required

                if newcurrent < 0:

                    self.SupplyCurrent = 0
                    self.SweepMode = "PAUSED"

                else: 

                    self.SupplyCurrent = newcurrent




            self.LastSweepTime = currenttime

def main():

    print("Starting!")
    Cryomagnetics4GSimulator()

if __name__ == '__main__':

    main()