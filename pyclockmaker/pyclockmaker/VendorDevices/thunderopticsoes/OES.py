import cv2
import numpy
from datetime import datetime

class OES:
    
    # defines
    
    cameraID = 1
    xwidth = 1920
    ywidth = 1080
    ROIx = [37,1919]
    ROIy = [310,440]
    background = []
    codec = 0x47504A4D  # MJPG
    
    def __init__(self,cameraID):
    
        print("Initializing camera at address: " + str(cameraID))
        self.OES = cv2.VideoCapture(cameraID)
        print(self.OES.isOpened())

        print(cameraID)
        
        #self.OES.set(cv2.CAP_PROP_FPS, 30.0)
        self.OES.set(cv2.CAP_PROP_FOURCC, self.codec)
        self.OES.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
        self.OES.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
        self.OES.set(3,self.xwidth)
        self.OES.set(4,self.ywidth)
        self.GatherBackground()
        print("Initialized!!!")
       
    def GetFrame(self):
        #print("Getting frame!")
        return_value, image = self.OES.read()
        
        return image

    def GatherBackground(self):

        temp = self.GetFrame()

        self.background = []
        self.background = self.CalcIntensity(temp)
                
    def CalcIntensity(self,image):
        outputintensity = []
        xout = 0
        for xout in range(self.ROIx[0]-1,self.ROIx[1]-1):
            
            # let's talk a little about the algorithm here.
            # the image object is a 1080 x 1920 x 3 array, Y/X/RGB.
            # the goal here is to calculate the relative intensities per X vertical line.
            # the overall intensity of a given pixel is sqrt(R^2 + G^2 + B^2).
            # doing it that way will unfortunately require 1 sqrt and 3 squares per pixel, which is computationally shitty.
            # since we're doing relative intensities, it's perfectly ok to 
            # sum all Rs, Gs, Bs per vertical slice, then root/square those.
            # furthermore! that's the geometric average, but it's also perfectly cromulent to just ADD these together and say "yeah here's the amount" and scale it by some value later. that's probably faster, but for now we'll just square and sqrt.
           
            rtemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,0].sum()**2
            gtemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,1].sum()**2
            btemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,2].sum()**2
            outputintensity.append((rtemp+gtemp+btemp)**.5)
            
        return outputintensity
        
    def CalcCompensatedIntensity(self,image):
        outputintensity = []
        xout = 0
        compensate = 100000
        for xout in range(self.ROIx[0]-1,self.ROIx[1]-1):
            
            
            rtemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,0].sum()**2
            gtemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,1].sum()**2
            btemp=image[self.ROIy[0]:self.ROIy[1]][:,xout,2].sum()**2
            outputintensity.append((rtemp+gtemp+btemp)**.5)


            # TODO: what the fuck was compensate trying to do? shift by lowest?
            '''
            for y in range(self.ROIy[0]-1,self.ROIy[1]-1):
                outputintensity[xout]+=((image[y][x][0]**2+image[y][x][1]**2+image[y][x][2]**2)**.5)**2
            outputintensity[xout] = outputintensity[xout]**.5
            
            if outputintensity[xout]< compensate:
                compensate = outputintensity[xout]
                xout = xout + 1
            
        for x in range(len(outputintensity)):
            outputintensity[x] = outputintensity[x]-compensate
        '''
        return outputintensity
 
    def CalcBackgroundRemovedIntensity(self,image):
    
        outputintensity = self.CalcIntensity(image)
        compensatedintensity = numpy.asarray(outputintensity) - numpy.asarray(self.background)
        return compensatedintensity
