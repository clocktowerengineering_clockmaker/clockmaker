from datetime import datetime
from distutils import command
import serial
import traceback


class ShimadzuTurboRS232:

    terminator = '\r'
    defaultBaud = 9600
    defaultTimeout = .1
    defaultPortID = '/dev/tty_turbo'
    
    # Defines here
    command_header = "MJ01"

    #RESPONSES - list is set up to take code, text, and response value
    responses = [
        ["MJ01LL90", "In Local Mode", "Local"],
        ["MJ01LR96", "In Remote Mode", "Remote"],
        ["MJ01LC87", "In RS232 Mode", "RS232"],
        ["MJ01LL90", "In Local Mode", "Local"],
        ["MJ01LR96", "Changed to Offline Remote Mode", "Remote"],
        ["MJ01RA8B", "Start Accelerating", "Accelerating"],
        ["MJ01RVA0", "Command Not Accepted", "Error"],
        ["MJ01RB8C", "Start Decelerating", "Decelerating"],
        ["MJ01RZA4", "Buzzer Off", "Buzzer"],
        ["MJ01RF","Failure Occurrence","FailureCode"],
        ["MJ01RC8D", "Failure Cleared!", "FailureCleared"],

    ]
    failure_responses = ["MJ01F50F5", "Start Decelerating", "Decelerating"]

    # Init object
    def __init__(self,serialPort=defaultPortID,baud=defaultBaud,timeout=defaultTimeout):

        self.StartSerial(serialPort,baud,timeout)
        

    # Open serial object 
    def StartSerial(self,serialPort=defaultPortID,baud=defaultBaud,timeout=defaultTimeout):

        self.pumpSerial = serial.Serial()
        self.pumpSerial.parity = "N"
        self.pumpSerial.stopbits = 1
        self.pumpSerial.timeout = timeout
        self.pumpSerial.baudrate = baud 
        self.pumpSerial.port = serialPort
        self.pumpSerial.open()

        #print self.pumpSerial.name
        return self.pumpSerial

    # Close serial object 
    def StopSerial(self):

        self.pumpSerial.close()


    def ChecksumCalc(self,word):

        #checksum gets calced by adding all chars in bytearray together, then generating a char itself that is the mod 256 of that sum
        byte_version_of_word = bytearray(word.encode())
        tempsum = 0
        for i in range(len(byte_version_of_word)):
            tempsum=tempsum + byte_version_of_word[i]
        
        # add it to the end
        word = word + str.upper(str(hex(tempsum%256)[2:]))

        # return string
        return word


    # response parser


    

    # CRC Command


    def WriteSerial(self,value):

        # writes a serial command and reads from the receive buffer. does not decode. if timeout,, excepts, closes the serial port, reopens the serial port, and tries again.
        while(True):
            try:
                self.pumpSerial.write(str(str(value)+self.terminator).encode())
                response = self.pumpSerial.read_until(self.terminator.encode()  )
                #print(response)

                return response
            except:
                print(str(datetime.now())+': timeout. Restarting comms....')
                self.pumpSerial.close()
                self.pumpSerial.open()
    
    # Pump in RS232 Mode
                
    def SetPumpModeSerial(self):
        try:
            command_word = "LN"
            to_transmit = self.ChecksumCalc(self.command_header+command_word)
            #print(to_transmit)
            response =  self.WriteSerial(to_transmit)           
            responsetext = response.decode().split()[0]
            if responsetext == "MJ01LC87":
                return True, responsetext
            
            else:
                return False, responsetext

        except:
            print(str(datetime.now()) + ": Couldn't set pump into serial mode.")
            traceback.print_exc()   
        

    # Start Pump

    def StartPump(self):

        command_word = "RT"
        to_transmit = self.ChecksumCalc(self.command_header+command_word)
        response =  self.WriteSerial(to_transmit)           
        responsetext = response.decode().split()[0]
        print(str(datetime.now()) + ": " + responsetext)
        if responsetext == "MJ01RA8B":
            return True, responsetext
        
        else:
            return False, responsetext
        

    # Stop Pump

    def StopPump(self):

        command_word = "RP"
        to_transmit = self.ChecksumCalc(self.command_header+command_word)
        response =  self.WriteSerial(to_transmit)           
        responsetext = response.decode().split()[0]

        print(str(datetime.now()) + ": " + responsetext)
        if responsetext == "MJ01RB8C":
            return True, responsetext
        
        else:
            return False, responsetext


    def ResetPump(self):

        command_word = "RP"
        to_transmit = self.ChecksumCalc(self.command_header+command_word)
        response =  self.WriteSerial(to_transmit)           
        responsetext = response.decode().split()[0]
        print(str(datetime.now()) + ": " + responsetext)
        if responsetext == "MJ01RC8D$":
            return True, responsetext
        
        elif responsetext[0:5]=="MJ01FS":
            # failure, return the failure #
            return False, int(responsetext[6:8])
        
        else:
            return False, responsetext


    # Read Pump Status

    def PumpStatus(self):
        try:
            command_word = "CS"
            to_transmit = self.ChecksumCalc(self.command_header+command_word)
            #print(to_transmit)
            #to_transmit = "MJ01CS8E"
            response =  self.WriteSerial(to_transmit)    
            responsetext = response.decode().split()[0] 
            #print(responsetext)    
            if responsetext=="MJ01NA00E7":

                return True, "Accelerating"
            if responsetext=="MJ01NB00E8":

                return True, "Decelerating"
            if responsetext=="MJ01NN00F4":

                return True, "Normal Rotation"
            if responsetext=="MJ01NS00F9":

                return True, "Stop"
            if responsetext=="MJ01FS1C05":

                return False, "Failure Stop"
            if responsetext=="MJ01FF32E9":

                return False, "Failure Idle"
            if responsetext=="MJ01FR15F6":

                return False, "Failure Regeneration"
            if responsetext=="MJ01FB60E6$":

                return False, "Failure Deceleration"
            



        except:
            print("something be hosed with pumpstatus.")
            traceback.print_exc()

    # read alarm details

    def PumpAlarm(self):

        try:
            command_word = "CF01"
            to_transmit = self.ChecksumCalc(self.command_header+command_word)
            
            response =  self.WriteSerial(to_transmit)           
            responsetext = response.decode().split()[0]
            return True, responsetext
        
        except:
            print("something be hosed with alarmstatus.")
            traceback.print_exc()

    # read speed

    def PumpSpeed(self):

        try:
            command_word = "PR03"
            to_transmit = self.ChecksumCalc(self.command_header+command_word)
            
            response =  self.WriteSerial(to_transmit)           
            responsetext = response.decode().split()[0]

            if responsetext[0:8]=="MJ01PA03":
                # failure, return the failure #
                return True, int(responsetext[8:12])

            else:
                return False, responsetext
        
        except:
            print("something be hosed with alarmstatus.")
            traceback.print_exc()
