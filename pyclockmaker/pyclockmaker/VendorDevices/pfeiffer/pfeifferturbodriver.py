import time
from datetime import datetime
import serial
import traceback



class pfeifferturbodriver:


    # defines a Pfeiffer RS485 object.

    # parameters and action types
    pump_address = '001'
    request = '00'
    command = '10'
    terminator = "\r".encode()

    default_port = "/dev/tty_Turbo"
    default_baud = 9600
    default_timeout = .1

    # data types
    boolean_old_0 = 0
    u_integer_1 = 1
    u_real_2 = 2
    string_6_4 = 4
    boolean_new_6 = 6
    u_short_int_7 = 7
    u_expo_new_10 = 10
    string_16_11 = 11


    #parameters

    #control commands
    pressure = ['340', u_short_int_7]
    ErrorAckn = ['009',boolean_old_0]
    PumpingStatn = ['010',boolean_old_0]
    MotorPump = ['023',boolean_old_0]   
    Brake = ['013',boolean_old_0]
    SpdSetMode = ['027',u_short_int_7]
    GasMode = ['027',u_short_int_7]
    CtrlVaiInt = ['060',u_short_int_7]


    #status requests
    OpFluidDef = ['301',boolean_old_0]
    SpdSwPtAtt = ['302',boolean_old_0]
    ErrorCode = ['303',string_6_4]
    OvTempElec = ['304',boolean_old_0]
    OvTempPump = ['305',boolean_old_0]
    SetSpdAtt = ['306',boolean_old_0]
    PumpAccel = ['307',boolean_old_0]
    SetRotSpd = ['308',u_integer_1]
    ActualSpd = ['309',u_integer_1]
    DrvCurrent = ['310',u_real_2]
    

    def __init__(self,serialPort=default_port,baud=default_baud,timeout=default_timeout):

        print(str(datetime.now()) + ": New Turbo Starting!")
        self.StartSerial(serialPort,baud,timeout)

    
    def StartSerial(self,port=default_port,baud=default_baud,timeout=default_timeout):

        self.turboserial = serial.Serial()
        self.turboserial.port = port
        self.turboserial.baudrate = baud
        self.turboserial.timeout = timeout
        self.turboserial.open()
        time.sleep(.01)
        self.turboserial.read_all()

        return self.turboserial



    def StopSerial(self):

        self.turboserial.close()


    def DataRequest(self,parameter):

        datatelegram = str(self.pump_address).encode()+"00".encode()+str(parameter[0]).encode()+"02=?".encode()
        checksum = self.ReturnChecksum(datatelegram)
        fulltelegram = datatelegram + checksum + self.terminator
        #print(fulltelegram)
        self.turboserial.write(fulltelegram)
        response = self.turboserial.read_until(self.terminator)
        #print(response)
        readchecksum = self.ReturnChecksum(response[:-4])
        #print(readchecksum)
        #print(response[-4:-1])
        # check that checksum actually is correct
        if readchecksum != response[-4:-1]:

            print("something is hosedaaaaa")
            print(readchecksum+ 'does not equal ' +(response[-4:-1]))
            print("response= " + response)

        else:

            # switch time
            readback = response.decode()

            if parameter[1] == self.boolean_new_6 or parameter[1] == self.boolean_old_0:

                
                if readback[-5] == "1":

                    return True
                
                elif readback[-5] == "0":

                    return False

                else:

                    Exception 
                        
                pass


            if parameter[1] == self.string_16_11:

                return readback[-20:-4]

            if parameter[1] == self.string_6_4:

                return readback[-10:-4]

            if parameter[1] == self.u_expo_new_10:

                # not implemented. Implement when sensors available.

                pass

            if parameter[1] == self.u_real_2:

                return int(readback[-10:-4])*.01

            if parameter[1] == self.u_integer_1:

                return int(readback[-10:-4])

            if parameter[1] == self.u_short_int_7:

                return int(readback[-7:-4])



    def ReturnChecksum(self,byteword):

        return str(sum(byteword) % 256).zfill(3).encode()
    

    def OnBooleanCommand(self,parameter):

        datatelegram = str(self.pump_address).encode()+"10".encode()+str(parameter[0]).encode()+"06111111".encode()
        checksum = self.ReturnChecksum(datatelegram)
        fulltelegram = datatelegram + checksum + self.terminator
        #print(fulltelegram)
        self.turboserial.write(fulltelegram)
        response = self.turboserial.read_until(self.terminator)
        #print(response)

    def OffBooleanCommand(self,parameter):

        datatelegram = str(self.pump_address).encode()+"10".encode()+str(parameter[0]).encode()+"06000000".encode()
        checksum = self.ReturnChecksum(datatelegram)
        fulltelegram = datatelegram + checksum + self.terminator
        #print(fulltelegram)
        self.turboserial.write(fulltelegram)
        response = self.turboserial.read_until(self.terminator)
        #print(response)

    def StartPump(self):

        #send twice because we don't totally know what the order of ops is

        self.OnBooleanCommand(self.MotorPump)
        self.OnBooleanCommand(self.PumpingStatn)
        self.OnBooleanCommand(self.MotorPump)
        self.OnBooleanCommand(self.PumpingStatn)

    def StopPump(self):

        self.OffBooleanCommand(self.MotorPump)
        self.OffBooleanCommand(self.PumpingStatn)

    def PumpOnReadback(self):

        return self.DataRequest(self.MotorPump)

    def PumpSpeed(self):

        return self.DataRequest(self.ActualSpd)    
    
    def PumpAlarm(self):
        #TODO: implement alarm
        return [True,True,2]

    def HandleAlarm(self):

        #TODO: SEND ALARM CLEAR

        pass


    def PumpAccelerating(self):


        return self.DataRequest(self.PumpAccel)

    def PumpSpeedAttained(self):
        
        return self.DataRequest(self.SetSpdAtt)