{
  pkgs ? (import (import ../nix/sources.nix {}).nixpkgs {}),
  poetry2nix ? (import (import ../nix/sources.nix {}).poetry2nix {pkgs = pkgs;}),
}: let

  # NOTE: POETRY2NIX SHOULD BE FIXED VIA FLAKE MODULE TO NAME EXECUTABLES RIGHT
  inherit (pkgs.qt5) qtbase;
in
  poetry2nix.mkPoetryApplication {
    projectDir = ./.;
    preferWheels = true;
    # TODO: move this, only needed for diagnostics.
    # extraPackages = (pythonPackages: [ pythonPackages.tkinter ]);
    overrides = [
      poetry2nix.defaultPoetryOverrides
      (final: prev: {
        # We ignore the poetry lock file for pyside2 and pyqt5,
        # and substitute whatever the working version is from pkgs.
        inherit (pkgs.python311Packages) pyside2 pyqt5;
      })
    ];
    # huh? what's this? look in ./make-wrapper.sh to understand...
    preFixup = builtins.readFile ./make-wrapper.sh;
    postFixup = ''
      mkdir -p "$out/lib"
      # PyQt seems to reliably search in $out
      # for a platform plugin, so that's where we'll link it.
      # This is an ugly hack but I (daniel) already spent ~8h
      # trying to disentangle this so fuck it.
      # Ideally we would be able to let pyqt know by other means
      # where to find the platform plugins, but this is not an ideal world.
      ln -s \
        "${qtbase}/lib/qt-${qtbase.version}" \
        -t "$out/lib/"
    '';
    shellHook = ''
      export QT_QPA_PLATFORM_PLUGIN_PATH="${qtbase}/lib/qt-${qtbase.version}/plugins"
    '';
  }
