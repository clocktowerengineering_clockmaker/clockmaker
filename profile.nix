let
  tango-controls = import ./tango-controls {};
  pyclockmaker = import ./pyclockmaker {};
in
  builtins.attrValues tango-controls  ++ [ pyclockmaker ]
