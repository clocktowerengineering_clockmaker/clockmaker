{
  poetry2nixApplication,
  origSrc ? poetry2nixApplication.src.origSrc, # string of path of source files.
  pyprojectTOML ? builtins.fromTOML (builtins.readFile (origSrc + "/pyproject.toml")),
  scripts ? pyprojectTOML.tool.poetry.scripts or {},
}: let
  sources = import ../nix/sources.nix {};
  nixpkgs = import sources.nixpkgs {};
  inherit (nixpkgs) lib makeWrapper stdenv;
  inherit (lib.strings) concatStringsSep splitString;
  scriptList = builtins.attrValues (builtins.mapAttrs (
      name: spec: let
        spec_split = splitString ":" spec;
      in {
        inherit name;
        module = builtins.elemAt spec_split 0;
        function = builtins.elemAt spec_split 1;
      }
    )
    scripts);

  workingDir = builtins.toPath origSrc;
  python = poetry2nixApplication.python;

  # Stolen/adapted from poetry2nix/shell-scripts.nix
  scriptToMakeWrapperCmd = (
    {
      name,
      module,
      function,
    }: ''
      cat << EOF >> $out/bin/${name}
      #!${python.interpreter}
      import sys
      import re

      sys.path.insert(0, "${workingDir}")

      from ${module} import ${function}

      if __name__ == '__main__':
          sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', "", sys.argv[0])
          sys.exit(${function}())
      EOF
      ls $out/bin
      chmod +x $out/bin/${name}
    ''
  );

  # These shell scripts
  scriptPkg = stdenv.mkDerivation {
    name = "${poetry2nixApplication.pname}-shell-scripts";
    meta = {
      description = "Shell scripts for ${poetry2nixApplication.pname} that invoke code directly in ${origSrc} rather than the packaged source.";
    };
    nativeBuildInputs = [makeWrapper];
    buildInputs = [python];
    phases = ["installPhase"];
    installPhase =
      ''
        mkdir -p $out/bin
      ''
      + concatStringsSep "\n" (builtins.map scriptToMakeWrapperCmd scriptList);
  };
in {
  name = "${poetry2nixApplication.pname}-shell";
  packages = [nixpkgs.pyright scriptPkg];
  inputsFrom = [poetry2nixApplication];
  shellHook = ''
    export PYTHONPATH="${workingDir}:$PYTHONPATH"
  '';
}
