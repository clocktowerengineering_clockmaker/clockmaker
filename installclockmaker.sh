#!/usr/bin/env bash

set -e
set -o pipefail

if [ "$USER" = "root" ]; then
  printf "%s\n" "Running as root increases chance of footgun and as such is not recommended."
  exit 1
fi

if [ -d "/run/current-system" ]; then
  printf "%s\n" "Don't run this from nixos! See docs (or yell at Daniel to write docs) for how to get clockmaker running in Nixos."
  exit 1
fi

# Gotta specify "--extra-experimental-features" to bootstrap.
_DEFAULT_NIX_PROFILE="/nix/var/nix/profiles/default"
export _NIXRUN="$_DEFAULT_NIX_PROFILE/bin/nix --extra-experimental-features nix-command --extra-experimental-features flakes run"

# TMPDIR=$(mktemp --directory "/tmp/create_tango_database-$NOW.XXXXXX")
# NIX_TANGO_DATABASE="$TMPDIR/nix-TangoDatabase"
THIS_FILE_DIR=$(dirname $0)
# User runs the services. Not necessarily the same user used when developing.
CLOCKMAKER_USER="clockmakeruser"
CLOCKMAKER_USER_PASSWORD="saR6b/MLHmoOc"

echo "Creating $CLOCKMAKER_USER on the system if one doesn't already exist."
sudo useradd -m -p "$CLOCKMAKER_USER_PASSWORD" $CLOCKMAKER_USER || echo "Found user; continuing"
echo "Adding user to dialout group" || echo "User already in dialout group"
sudo adduser clockmakeruser dialout || echo  "User already in dialout group"

CLOCKMAKER_USER_MYSQL_PASSWORD="clockmakeruser"
MYSQL_HOST="localhost"

USER_PASSWD_ENTRY=$(getent passwd "$CLOCKMAKER_USER")
CLOCKMAKER_USER_UID=$(printf  %s $USER_PASSWD_ENTRY | cut -d : -f 3)
CLOCKMAKER_USER_GID=$(printf  %s $USER_PASSWD_ENTRY | cut -d : -f 4)
CLOCKMAKER_USER_HOME=$(printf %s $USER_PASSWD_ENTRY | cut -d : -f 6)

(
  umask 0077
  printf "%s\n", "Updating nix.conf (infra/mongrel/make_nix_conf_usable.py)."
  # TODO: switch $USER prefixing to tempfiles.
  sha256sum --tag < /etc/nix/nix.conf > /tmp/$USER.nix.conf.sum
  if [[ ! ( -f "/etc/nix/nix.conf" ) ]]; then
    printf "%s\n" "Couldn't find /etc/nix/nix.conf. Is Nix installed? See README for instructions."
    exit 1
  fi
  cat /etc/nix/nix.conf \
    | $_NIXRUN nixpkgs#python3 -- "$THIS_FILE_DIR"/infra/mongrel/make_nix_conf_usable.py \
    > /tmp/$USER.new_nix.conf


  # avoid a daemon reload unless we need it.
  (sha256sum --check /tmp/$USER.nix.conf.sum < /tmp/$USER.new_nix.conf &> /dev/null) || _check_fail=1
  if [[ -v _check_fail ]]; then
    sudo cp /tmp/$USER.new_nix.conf /etc/nix/nix.conf
    sudo systemctl reload-or-restart nix-daemon.service
  fi
)

function find_cache() {
  which tailscale &> /dev/null || _check_fail=1
  if [ -v _check_fail ]; then
    printf "%s\n" "WARNING: Could not find tailscale. This script will likely break without a nix cache with a built version of the software."
    return
  fi
  DNS_NAMES=$(
    umask 0077
    STATUS_JSON=$(mktemp /tmp/tailcalestatus.XXXXXXXXXXXXX)
    tailscale status --json \
      | $_NIXRUN nixpkgs#jq -- -r '.Peer[] | select((.Online==true) and (.Tags[]? == "tag:nixcache")) | .DNSName' - \
  )

  for CACHE_OPTION in $DNS_NAMES; do
    if $_NIXRUN nixpkgs#curl -- --silent --fail --max-time 5 https://$CACHE_OPTION/nix-cache-info > /dev/null; then
      export CACHE_URL=https://$CACHE_OPTION
      break
    fi
  done
}
find_cache

if [ -v CACHE_URL ]; then
  printf "Found cache to use for building: %s\n" "$CACHE_URL"
  export EXTRA_SUBSTITUTERS_FLAGS="--option extra-substituters $CACHE_URL"
else
  printf "$s\n" "WARNING: DID NOT FIND NIXCACHE. Make sure you're logged in on tailscale. If you are, yell at Daniel 'cause it might be his fault."
fi

CLOCKMAKER_PROFILE="/nix/var/nix/profiles/clockmaker"

if [ -z $TANGO_HOST ]; then
  TANGO_HOST="127.0.0.1:10000"
  echo "Using preset TANGO_HOST=$TANGO_HOST"
else
  echo "Using existing TANGO_HOST=$TANGO_HOST"
fi

ORBENDPOINT="giop:tcp:$TANGO_HOST"

CLOCKMAKER_BASH_ENV=\
"
export TANGO_HOST="$TANGO_HOST"
export HDB_TYPE="mysql"
export HDB_MYSQL_HOST=\"$MYSQL_HOST\"
export HDB_NAME=hdbpp
export HDB_USER=\"$CLOCKMAKER_USER\"
export HDB_PASSWORD=\"$CLOCKMAKER_USER_MYSQL_PASSWORD\"
"

printf "%s\n" "$CLOCKMAKER_BASH_ENV" \
  | sudo tee /etc/profile.d/clockmaker.sh \
  > /dev/null


echo "Building clockmaker with nix. This might be heavy on RAM."
echo "  Hint: come back in about an hour since installation requires sudo"
#
# --keep-going for if the user steps away for coffee, all but the broken package(s) should be built upon their return.
#

# allow user to restart the machine just once at the end by avoiding PATH lookup of nix-build.
$_DEFAULT_NIX_PROFILE/bin/nix-build \
  $EXTRA_SUBSTITUTERS_FLAGS \
  "$THIS_FILE_DIR"/profile.nix \
  --no-out-link \
  --keep-failed \
  --keep-going


echo "Setting default profile of $CLOCKMAKER_USER to $CLOCKMAKER_PROFILE"
sudo -u $CLOCKMAKER_USER -- \
  $_DEFAULT_NIX_PROFILE/bin/nix-env \
  --switch-profile "$CLOCKMAKER_PROFILE"

# TODO (daniel): decide whether to make this a default.
# echo "Setting default profile of $USER to $CLOCKMAKER_PROFILE"
# $_DEFAULT_NIX_PROFILE/bin/nix-env --switch-profile "$CLOCKMAKER_PROFILE"

echo "Installing clockmaker to $CLOCKMAKER_PROFILE with nix."
# BE CAREFUL; THIS SHOULD NOT TOUCH DEFAULT PROFILE
sudo -- \
  $_DEFAULT_NIX_PROFILE/bin/nix-env \
  --profile "$CLOCKMAKER_PROFILE" \
  --remove-all \
  --install \
  --file $THIS_FILE_DIR/profile.nix


echo "Installing mariadb (aka mysql) on the local machine"
# libboost-all-dev  (commenting this out cause idk what it's for)
sudo apt install -y mariadb-server mariadb-client

if [ -n "$WITH_DIRENV" ]; then
  sudo apt install -y direnv
  if grep direnv "$HOME/.bashrc"; then
    echo "Direnv might already be in your bashrc"
    echo "If this is not the case, make sure 'eval \"\$(direnv hook bash)\"' is in your .bashrc"
    echo 'Optionally, make sure that "export DIRENV_LOG_FORMAT=" is also in your bashrc before the hook above. This quiets direnv.'
  else
    printf "%s\n" "export DIRENV_LOG_FORMAT=" >> "$HOME/.bashrc"
    printf "%s\n" 'eval "$(direnv hook bash)"' >> "$HOME/.bashrc"
  fi
  echo "Enabling direnv for this directory."
  direnv allow "$THIS_FILE_DIR/.envrc"
  echo "Unlazily-loading direnv"
  direnv exec $PWD echo "Loaded Direnv"
fi

echo "Installing mariadb (aka mysql) on the local machine"


# $CLOCKMAKER_USER needs a mysql password for Tango since Tango attaches via TCP/localhost.
# $USER doesn't, since the `mysql <hdbpp|tango>` command attaches via unix socket.
# No idea why @'$MYSQL_HOST' is necessary tho.
CREATE_USER_SQL=\
"CREATE USER IF NOT EXISTS '$CLOCKMAKER_USER'@'$MYSQL_HOST' IDENTIFIED BY 'clockmakeruser';
GRANT ALL PRIVILEGES ON *.* TO '$CLOCKMAKER_USER'@'$MYSQL_HOST';
GRANT FILE ON *.* TO '$CLOCKMAKER_USER'@'$MYSQL_HOST';
CREATE USER IF NOT EXISTS '$USER'@'$MYSQL_HOST';
GRANT ALL PRIVILEGES ON *.* TO '$USER'@'$MYSQL_HOST';
GRANT FILE ON *.* TO '$USER'@'$MYSQL_HOST';
FLUSH PRIVILEGES;
"
echo "Creating $CLOCKMAKER_USER and $USER in mysql if not already exists."
printf %s "$CREATE_USER_SQL" | sudo mysql -u root

CREATE_USER_MY_CNF=\
"[client]
user=$CLOCKMAKER_USER
password=$CLOCKMAKER_USER_MYSQL_PASSWORD
"

if [ -e "$CLOCKMAKER_USER_HOME/.my.cnf" ]; then
  echo "Skipping installation of $CLOCKMAKER_USER_HOME/.my.cnf; file exists."
else
  echo "Installing $CLOCKMAKER_USER_HOME/.my.cnf"
  (
    umask 077 # files created in 600 mode. reference: `help umask` in bash.
    sudo touch "$CLOCKMAKER_USER_HOME/.my.cnf"
    # Notice we secure the file before putting the password into it.
    sudo chown $CLOCKMAKER_USER_UID:$CLOCKMAKER_USER_GID "$CLOCKMAKER_USER_HOME/.my.cnf"
    printf %s "$CREATE_USER_MY_CNF" | sudo tee "$CLOCKMAKER_USER_HOME/.my.cnf" > /dev/null
  )
fi


if mysql tango < /dev/null; then
  echo "Skipping migration for tango database; already exists."
else
  echo "Running create_db.sql to create Tango configuration database with mysql"
  (
    cd "$CLOCKMAKER_PROFILE/share/tango/db"
    # need to cd to the directory cause create_db.sql references other sql files
    # in the same directory.
    sudo -u "$CLOCKMAKER_USER" mysql < create_db.sql
  )
fi

# TODO: parameterize the dbname for libhdbpp
if mysqladmin create hdbpp; then
  echo "Applying hdbpp mysql migration"
  (
    # libhdb++mysql is standalone but might not be in the future
    # so we just future proof ourselves by cd'ing anyway.
    cd "$CLOCKMAKER_PROFILE/share/libhdb++mysql"
    sudo -u "$CLOCKMAKER_USER" mysql -D hdbpp < create_hdb++_mysql.sql
  )
else
  echo "Skipping migration for hdbpp database; already exists."
fi

# start adding services.

echo "Installing {tangodatabase,starter}.service (replaces existing)"
TANGO_SERVICE=\
"[Unit]
Description=TangoDatabase
After=network-online.target
Requires=network-online.target

[Service]
User=$CLOCKMAKER_USER
Environment=TANGO_HOST=$TANGO_HOST
ExecStart=$CLOCKMAKER_PROFILE/bin/Databaseds 2 -ORBendPoint $ORBENDPOINT

[Install]
WantedBy=multi-user.target
"

STARTER_SERVICE=\
"[Unit]
Description = Tango Starter
After=tango.service
Requires=tango.service

[Service]
User=$CLOCKMAKER_USER
Environment=TANGO_HOST=$TANGO_HOST
ExecStart=$CLOCKMAKER_PROFILE/bin/Starter %H

[Install]
WantedBy=multi-user.target
"

{
  printf %s "$TANGO_SERVICE"   | sudo tee /etc/systemd/system/tango.service
  printf %s "$STARTER_SERVICE" | sudo tee /etc/systemd/system/starter.service
} > /dev/null
sudo chmod +x /etc/systemd/system/{tango,starter}.service
sudo systemctl --system daemon-reload
sudo systemctl enable tango starter
sudo systemctl start tango starter

printf %s "Installing desktop files to $HOME/.local/share/applications/clockmaker"
mkdir -p "$HOME/.local/share/applications/clockmaker"
desktop-file-install \
  --rebuild-mime-info-cache \
  --dir "$HOME/.local/share/applications/clockmaker" \
  $THIS_FILE_DIR/desktop/applications/*.desktop


echo 'If there were no errors, reboot with "sudo reboot -f"'
