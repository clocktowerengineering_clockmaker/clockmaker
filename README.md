# Contributing

We are henceforth using trunk-based development to manage this repository.
TODO: write more.
For now, just be aware that you should NEVER push directly to the master or main branches.

# Background Info
Clockmaker, consisting only of the contents of this repository and client-specific SOW repositories, is a group of utilities and device servers built upon Tango Controls.
*NOTE:* Nixified clockmaker hopes to maintain all clockmaker-relevant code in this one central repository, with no submodules. So far, I (Daniel) have integrated:
- devicepalette and its submodules, including Mainspring, in `./pyclockmaker`.
- hdb-install, which only existed as a group of folders of tango-controls projects to make installing the archiver easier.
- all other tango-controls that we were using, which are no longer housed in cloned repositories from upstream.

If you're missing any utilities, or see any issues, please document them as child issues in [CLMK-119](https://clocktowerengineering.atlasion.net/browse/CLMK-119)

## Tango Around the clock
Clockmaker is built upon Tango Controls, a "free open source device-oriented controls toolkit". Tango-controls is a system where (ran out of steam. To talk: device != device server, there’s a tango host, explain what Starter and Tango Database are, what the services are, etc).

## (Lin|N)ix is user-friendly. It's just picky about who its friends are.

Clockmaker uses Nix to ensure that all clients and users can easily reproduce builds of clockmaker and its dependencies.
"Nix" is a package management system with three parts:
1. Nixlang, a lazy, functional programming language that describes how to compile software packages,
2. Nixpkgs, a collection of packages that the Nix community has already described using Nixlang,
3. NixOS, a Linux-based operating system entirely based on Nix.

Clockmaker only uses the first two parts: Nixlang, which Daniel has used to package Tango Controls, and nixpkgs, which he pinned to the last stable version (24.05 as of the time of writing).
We opted to use Linux Mint with Nix as the platform that we ship to clients, rather than NixOS, because NixOS doesn’t offer many "safety hatches" to allow users to break out of its system---for instance, as part of its compilation process Nix patches binaries to reference their libraries by exact path (/nix/store/xxxxxxxxxxxxxxxxx-libname.so).
For a shop like ours, which regularly runs into proprietary and poorly-maintained vendor software, having a traditional Debian-based system as a fallback in case we can't package something correctly in Nix is essential.

# Installation

These instructions guide you through a mongrel install of clockmaker.
"Mongrel" means you're running the Nix package manager as a secondary package manager in addition to the one provided by the distribution (`apt`).
Work towards a properly (hopefully) NixOS-ified version of clockmaker is being done on branch `feat/nixify`.
The olde setup is on `master`.

## Set up a machine with Debian or Mint
The first step to setting up a clockmaker instance is to find a machine to deploy it on.
This can either be a virtualmachine (Like Virtualbox) or baremetal machine.
Daniel runs clockmaker on Debian Unstable, but so far we’re only deploying it on Linux Mint machines.
Instructions to get Linux Mint are beyond the scope of this document, but [here](https://www.linuxmint.com/download.php) is a link to download the ISO, which you can then load onto a USB or into Virtualbox.

## Set up Tailscale!

The install script for clockmaker needs you to be logged into tailscale to auto-detect our company-wide nix cache.
The nix cache will speed up certain operations (if the compilation has already been done on the nix cache)
and stores copies of often-changing internet files, such as the jars on Apache Maven, which cause a lot of headache when they're silently updated but we still know them by their old sha's.

If the below command doesn't work, see `https://tailscale.com/kb/1031/install-linux`
```bash
curl -fsSL https://tailscale.com/install.sh | sh
```

If the target machine isn't logged in on tailscale, you can login using
```bash
sudo tailscale login
```

Check that everything's working by seeing if you can find clocktower's nixcache in the tailscale status:
```bash
tailscale status
tailscale status | grep nixcache  # Should return more than one line.
```

If you're setting up a machine for production use, you must tag it correctly.
Only network admins can tag machines, so tell Daniel the machine's hostname once it's on Tailscale.

Also, to enable easy remote access, try running the below
```bash
# THIS CAN TAKE THE MACHINE OFF THE NETWORK;
# MAKE SURE SOMEONE IS PHYSICALLY NEAR THE MACHINE SO THEY CAN TROUBLESHOOT.
sudo tailscale up --ssh=true
```

For extra credit, take a skim through Clocktower's Confluence page on Tailscale for more details.

## Install Nix

*If* you’re on a fresh install, you can safely run:
```bash
sh <(curl -L https://nixos.org/nix/install) --daemon --yes
```
This will install Nix on your computer without additional intervention.

If you're installing Nix on a personal computer, or want to know exactly what Nix does to your system, I instead suggest running the interactive Nix installer.
If asked, choose the *multi-user install* and *allow sudo*:
```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```
## Clone this repository

```bash
sudo apt install git  # if you don't have git yet
git clone --recurse-submodules \
    git@bitbucket.org:clocktowerengineering_clockmaker/clockmaker.git
```

Make sure you check out the correct branch. Ye olde install is on the `master` branch, but the current nixified install is available on `main`, so you should use `main`.

## Install clockmaker!

The clockmaker install script does a couple of small things to make use of Clockmaker, Mysql, and other utilities as frictionless as possible.
If you're working on a personal machine, I recommend reading through the install script and adapting it to suit your workflow.
If you're installing Clockmaker on a client machine, you should run the install script directly:
```bash
cd # CLOCKMAKER REPOSITORY
WITH_DIRENV=t ./installclockmaker.sh
```

Make sure to run the installation script with `$WITH_DIRENV` set (as above) which enables the script to set up direnv for the user running the script.
I left the direnv installation as a parameter because it messes with your `.bashrc` so if you have an existing setup you might prefer to install and configure `direnv` manually.

The script will refuse to run as root to minimize the chance it catastrophically breaks something. It will, as it runs, ask for a root password once or twice for the purposes of sudo'ing key parts of the install, but the use of sudo is minimized.

After installing Clockmaker, please restart the computer. If you don't want to, you can instead log out and log back, which should be enough to have bash pick up the path.

## Set up Starter

`Starter` is the name of a "Device Server". 
It is responsible for starting and stopping the other device servers (those that talk to actual peripherals such as Galil Rio's, for example). To set up Starter, you _must_ do it through Astor, rather than through Jive. Other device servers should be set up through Jive (see our Confluence page) but to make sure Astor shows the Starter. 

1. Open Astor (should be in the start menu after installation, or run `/nix/var/nix/profiles/clockmaker/bin/astor`). 
2. Click "Command" from the top menu bar, then "Add New Host" in the top bar.
3. Enter the machine's host name in "Host name". This is usually something like `clockmaker-blahblah` or `clocktower--blahblah`. If you're not sure, run the `hostname` command in the terminal.
4. Under "Device Servers PATH", enter the profiles containing the device servers, one per line. On a vanilla install not meant for a customer, it should just be one line: `/nix/var/nix/profiles/clockmaker/bin`. For custom customer device servers, refer to the instructions in the respective repository.
5. Click "Create".
6. You should now see a new heading in Astor named "Miscellaneous", which you should then expand to see the new starter server (named after the computer's hostname). 
7. Restart the `starter` service so it picks up the new Starter device: `sudo systemctl restart starter`. After about 10 seconds, the status orb color in Astor should go from red to green.
8. Double-clicking on the hostname should reveal a new control panel with 3 buttons ("Start New", "Start All", and "Stop all") telling you about the servers Starter currently controls. After an installation, it'll show 0 controlled servers.

TODO (daniel): Add photos to this section.

# What just happened?
This section describes the behavior of the installation script and and structure of the clockmaker install.

## Dude, where's my (astor|jive|...)?

`nix`, `nix-env`, `nix-build`, and other nix programs come from the default Nix profile, which is always at `/nix/var/nix/profiles/default`. You should at no point modify this profile, and Clockmaker does not interact with it.

All tango-controls executables (`astor`, `jive`, etc) are managed through Nix and are located in a clockmaker-specific profile the installer creates: `/nix/var/nix/profiles/clockmaker`.
You'll notice, however, that running `jive` in the terminal as a non-`clockmakeruser` usre, gives a command not found error.
This is an intentional choice I (Daniel) made to help distinguish between the "system" version of Clockmaker, created by the `installclockmaker.sh`, and the packages as they're being developed.
If you installed `direnv`, though, navigating to the clockmaker repository should automatically activate a `nix-shell` which adds `jive`, `astor`, and other tango binaries to your PATH to allow you to develop.

When you're ready to commit your changes to the system such that the `Starter` device server picks them up, run `./installclockmaker.sh` again.

## Goodies and Breaking Changes
While I (Daniel) tried to be as true to the original installation format as possible, there are some changes I have made. If I'm missing anything, add them as a comment to the issue linked above, or open a new issue and assign it to me:

### SQL Config
The installation script now creates a sql config file in `/home/clockmakeruser/.my.cnf` which contains the Tango Database configuration data. This minimizes the amount of plaintext passwords hanging around the system (ex. keeping them out of the service files).
  This also means that when the `clockmakeruser` uses `mysql`, they don't need to specify a password for the tango database as this is already specified in their `~/.my.cnf` file.

### Tango Host and `/etc/hosts`
Notice that the installation procedure no longer requires editing `/etc/hosts`.
We originally edited it to make the `TANGO_HOST=localhost:10000` work correctly across everything Tango-related, but as far as I can tell the original problem was that the Tango database service resolved `localhost` to the ipv6 loopback `::1`, rather than the ipv4 loopback, `127.0.0.1`.
Somehow, editing `/etc/hosts` made the various Tango tools also resolve `localhost` to `::1` rather than the ipv4 loopback.
Whatever the original cause, we now avoid using `localhost` in clockmaker installations, instead preferring the explicit loopback address `TANGO_HOST=127.0.0.1:1000`.

### Starter Service
 The Starter service now sources device servers from the `clockmaker` Nix profile, rather than whatever may be in your user path.

### Development Workflow
While developing or diagnosing a device server, you should be working in the `nix-shell` or using `direnv`.
You can manually activate `nix-shell` from the root git directory, although you should strongly consider `direnv` which (imo) integrates better with the terminal.
Note that the Nix shell is derived from `./shell.nix` rather than `./profile.nix`, which means there are some differences between the environment of the `clockmakeruser` and your environment as a dev.
Mostly, these are quality of life improvements.
For example, in the nix shell, pyclockmaker scripts (like `mainspring`) are provided as wrapper scripts pointing _directly to the python code in the repo directory_
rather than a package in `/nix/`, which means changes you make to Python files are immediately available when you run the corresponding script.
Additionally, `./pyclockmaker` is now automatically added to the PYTHONPATH in the nix shell, meaning that you can run Python files importing from `pyclockmaker` anywhere in the filesystem.

### Python Importing
All imports of `pyclockmaker` packages should now be absolute imports starting from the top level "pyclockmaker" package.
For example, in `./pyclockmaker/pyclockmaker/VendorDevices/copleydeviceserver/AdvancedGenericCopley.py`, the import
```python
import CopleyRS232
```
became
```python
from pyclockmaker.VendorDevices.copleydeviceserver import CopleyRS232
```

### No more wrapper scripts for Starter
Device servers and other Python-based executables can now be specified in `./pyclockmaker/pyproject.toml` under the `[tools.poetry.scripts]` heading. This eliminates the need to write wrapper scripts, as they are programatically generated by the Nix setup.

### Monorepo
We are now nearly-monorepo, meaning that going forward everything clockmaker related should be in this repository, without submodules. "Nearly" because SOW-proprietary code continues to live in its own repository, though I (Daniel) will soon work on establishing an orderly process to coordinate SOW's and this central repository.

### SQL Migrations
Magic sql migrations are gone, replaced with programatically-applied patches to upstream migration scripts. In `./tango-controls/libhdbpp-mysql`  you can see I programatically apply a patch to fix the "date has no default" error (details in `./tango-controls/libhdbpp-mysql/default.nix`).

*IMPORTANT*: That there may be other patches I missed. If such errors exist, they'll bubble up soon enough / as part of the Cylinder2 testing.

### `/etc/profile`
The installation script no longer blindly shoves `export TANGO_HOST=` at the end of /etc/profile. Instead, it pushes an entire file at once to `/etc/profile.d/clockmaker.sh`, which means that if you run the install script multiple times it won't clog /etc/profile.

### `hdb-viewer` -> `jhdbviewer`
In previous clockmaker installations, the hdb viewer executable was called `hdb-viewer`.
I (Daniel) renamed it to `jhdbviewer` to align with the Tango docs.


# Tango Configuration
*Note*: This section is currently incomplete and assumes baseline knowledge about how Tango works. I'll fill this in in more detail as time goes on, but for now crossreference this with the Atlassian guide.

A typical install of clockmaker has 3 users:
1. root, which has no special function in clockmaker
2. "clockmakeruser", which is the user that has access to MYSQL and runs the Tango Database and Starter Systemd services.
  - (right now the installation script grants clockmakeruser access to the complete install
3. The standard user the developer/maintainer logs in as, usually "clockmaker".

For the hdbpp archiver, here's the magic words for LibConfiguration property:

```bash
libname=/nix/var/nix/profiles/clockmaker/lib/libhdb++mysql.so
host=localhost
dbname=hdbpp
user=clockmakeruser
password=clockmakeruser
port=3306
json_array=0
```

For Starter, use `/nix/var/nix/profiles/clockmaker/bin` for `StartDSPath` configuration.

## Superpowers
If you've broken the clockmaker profile with bad code, you can now safely rollback the work without changing the code in dev:
```bash
sudo /nix/var/nix/profiles/default/bin/nix-env \
  --profile /nix/var/nix/profiles/clockmaker \
  --rollback
```

## `HdbManager`, `jhdbviewer`, `hdbpp-configurator`, and you!
jhdbviewer and hdbpp-configurator will refuse to start unless the `HdbManager` environment variable is set. 
Your invocation should look something like:
```bash
HdbManager=daniel-debian-desktop/hdb++cm-srv/mycm jhdbviewer
HdbManager=daniel-debian-desktop/hdb++cm-srv/mycm hdbpp-configurator
```
This workaround shouldn't be necessary and is being addressed in CLMK-153.

## Tango database dumping
To dump the Tango database, 2 things need to be true: The target directory must be writable by the `mysql` user, and the database user must have the `FILE` permission, which isn't included in a typical `GRANT ALL PRIVILEGES` invocation. The `FILE` permission is included in `installclockmaker.sh`, so you can run that and it'll solve it for you. Dump the db to csv's using the script at the root of this with the following incantation:

```bash
./dumpdb.sh hdbpp /tmp/dump
```


# TODO
- I haven't yet decided whether I want to (or how I would) manage the desktop shortcuts, so those aren't installed in the install script yet, but you can find them in clockmaker/desktop/applications unharmed.
- Typing out `nix-shell --run ...` is painful while developing. Instead, I'm going to add bash aliases to the nix shell to override the script packages. Coming soon.
- I just realized that mainspring, as it's currently set up, executes the wrong python file so you get an empty grayed out form. This is an easy fix, I just need to do it cleanly.
