{ pkgs ? (import (import ../nix/sources.nix {}).nixpkgs) {}}: let
  manifest = [
    ./Astor
    ./JTango
    ./TangoAccessControl
    ./TangoDatabase
    ./TangoTest
    ./atk
    ./atk-panel
    ./cppTango
    ./hdbpp-cm
    ./hdbpp-configurator
    ./hdbpp-es
    ./hdbpp-viewer
    ./jive
    ./jdraw
    ./libhdbpp
    ./libhdbpp-mysql
    ./pogo
    ./starter
    ./tango-idl
  ];
  tango-controls = builtins.listToAttrs (
    map (
      pkgLoc: let
        pkg = pkgs.lib.callPackageWith (pkgs // {inherit tango-controls;}) (import pkgLoc) {};
      in {
        name = pkg.pname or pkg.name;
        value = pkg;
      }
    )
    manifest
  );
in
  tango-controls
