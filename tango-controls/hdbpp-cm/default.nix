{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  omniorb,
  pkg-config,
  stdenv,
  tango-controls,
}: let
  inherit (tango-controls) cppTango;

  pname = "hdbpp-cm";
  version = "2.2.0";
in
  stdenv.mkDerivation (finalAttrs: {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/hdbpp/${pname}/-/archive/${version}/${pname}-${version}.zip";
      hash = "sha256-JAapeD+8LCPksJsF6F3SFYQ/+NNowDINY4rSZ7GtcGk=";
    };

    nativeBuildInputs = [
      cmake
      pkg-config
    ];
    buildInputs = [
      cppTango
      cppzmq
      omniorb
    ];

    meta = {
      description = "Interface library for the HDB++ archiving system.";
      homepage = "https://gitlab.com/tango-controls/hdbpp/${pname}";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
