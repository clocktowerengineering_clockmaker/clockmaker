{
  cmake,
  fetchzip,
  lib,
  stdenv,
}:
stdenv.mkDerivation (finalAttrs: {
  pname = "tango-idl";
  version = "6.0.2";

  src = fetchzip {
    url = "https://gitlab.com/tango-controls/tango-idl/-/archive/6.0.2/tango-idl-6.0.2.zip";
    hash = "sha256-S47XWNJrcXp2AFyxYaO6OcxVeFk7bryCgjfaPZ/aWuo=";
  };

  nativeBuildInputs = [cmake];
  buildInputs = [];

  patches = [./join_path_pkgconfig.patch];

  meta = {
    description = "This is the Tango CORBA IDL file.";
    homepage = "https://gitlab.com/tango-controls/tango-idl";
    license = lib.licenses.lgpl3Only;
    maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
  };
})
