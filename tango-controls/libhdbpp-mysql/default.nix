{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  libmysqlclient,
  mariadb,
  omniorb,
  pkg-config,
  stdenv,
  tango-controls,
  zeromq,
}: let
  inherit (tango-controls) cppTango libhdbpp;

  pname = "libhdbpp-mysql";
  version = "7.1.0";
in
  stdenv.mkDerivation (finalAttrs: {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/hdbpp/libhdbpp-mysql/-/archive/11185fbffff7a39b6ddfe9864aca3e366820a1e0/libhdbpp-mysql-11185fbffff7a39b6ddfe9864aca3e366820a1e0.zip";
      hash = "sha256-Ucs3GcrAmohWGJEYKNaUElfWyQ6lAVMV+ZaO/y7OKUY=";
    };

    patches = [./fix-include.patch];

    nativeBuildInputs = [
      cmake
      pkg-config
      cppzmq
      libmysqlclient
    ];
    buildInputs = [
      cppTango
      libhdbpp
      zeromq
      mariadb
      omniorb
    ];

    meta = {
      description = "Interface library for the HDB++ archiving system.";
      homepage = "https://gitlab.com/tango-controls/hdbpp/${pname}";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
