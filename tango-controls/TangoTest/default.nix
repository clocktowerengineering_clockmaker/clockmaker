{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  libmysqlclient,
  mariadb,
  omniorb,
  stdenv,
  tango-controls,
  zeromq,
}: let
  inherit (tango-controls) cppTango;
  # Revision pegged to the git submodule that TangoTest uses.
  TangoCMakeModules-src = fetchzip {
    url = "https://gitlab.com/tango-controls/TangoCMakeModules/-/archive/fc8df31610a360f3e2914825bd46fca57cc672cc/TangoCMakeModules-fc8df31610a360f3e2914825bd46fca57cc672cc.zip";
    hash = "sha256-fEwUyO2tPFmGkkc1A0+auPPChzJ1IODeodHgqbPO+TM=";
  };
in
  stdenv.mkDerivation (finalAttrs: {
    pname = "TangoTest";
    version = "version-unknown";

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/TangoTest/-/archive/6c6de480a5e4f3b3eb6fd552cacce54f6f1d3af4/TangoTest-6c6de480a5e4f3b3eb6fd552cacce54f6f1d3af4.tar.gz";

      hash = "sha256-A7SDkM/Npl5Waf7LTfRbo0FVa4odU/8n0Wm5PKQeFwY=";
    };

    # Manually add submodule
    prePatch = ''
      rmdir ./TangoCMakeModules
      ln -s ${TangoCMakeModules-src}/ ./TangoCMakeModules
    '';

    nativeBuildInputs = [
      cmake
      cppzmq
    ];
    buildInputs = [
      cppTango
      mariadb
      omniorb
      zeromq
    ];

    meta = {
      description = "Tango Database Server";
      homepage = "https://gitlab.com/tango-controls/TangoDatabase";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
