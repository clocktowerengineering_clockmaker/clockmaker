{
  jre,
  makeWrapper,
  stdenv,
  lib,
  tango-controls,
}: let
  inherit (tango-controls) atk atk-panel JTango;
  pname = "synopticappli";

  classpath =
    builtins.concatStringsSep ":"
    [
      "${atk}/share/java/ATKCore.jar"
      "${atk}/share/java/ATKWidget.jar"
      "${atk-panel}/share/java/ATKPanel.jar"
      "${JTango}/share/java/JTango.jar"
    ];
in
  stdenv.mkDerivation (finalAttrs: {
    inherit pname;
    inherit (atk) version;

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre JTango];

    dontUnpack = true;
    # Mark didn't like how jive looks with the system theme,
    # so we use Java's builtin theme here to make sure he doesn't complain about jdraw/synoptic either.
    installPhase = ''
      makeWrapper ${jre}/bin/java $out/bin/synopticappli \
        --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
        --add-flags "--class-path \"${classpath}\"" \
        --add-flags "fr.esrf.tangoatk.widget.jdraw.SimpleSynopticAppli"
    '';

    meta = {
      homepage = "https://gitlab.com/tango-controls/atk";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
      mainProgram = pname;
    };
  })
