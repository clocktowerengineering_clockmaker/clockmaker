{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
}: let
  pname = "JTango";
  version = "9.7.3";
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/${pname}/-/archive/${version}/${pname}-${version}.zip";
      hash = "sha256-KmzWgr6DDxgthpnOwC76IaSfcD/r4BU8WJivKkllJEY=";
    };
    mvnHash = "sha256-B/PR9/wSPklADVNo6ruh7EKz4Rh22DJKuRn8vc3vmXo=";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre];

    # JTango hasn't passed its test suite in a while...
    mvnFetchExtraArgs = {
      doCheck = false;
    };
    doCheck = false;
    mvnParameters = "-DskipTests  -e";
    mvnDepsParameters = "-DskipTests  -e";

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java

      # there's also an integration-tests jar but i'm ignoring it
      install common/target/JTangoCommons-${version}-SNAPSHOT.jar \
         -T $out/share/java/JTangoCommons-${version}.jar
      install dao/target/TangORB-${version}-SNAPSHOT.jar \
         -T $out/share/java/TangORB-${version}.jar
      install client/target/JTangoClientLang-${version}-SNAPSHOT.jar \
         -T $out/share/java/JTangoClientLang-${version}.jar
      install assembly/target/JTango-${version}-SNAPSHOT.jar \
           -T $out/share/java/JTango-${version}.jar
      install server/target/JTangoServer-${version}-SNAPSHOT.jar \
         -T $out/share/java/JTangoServer-${version}.jar
      install server/target/JTangoServer-${version}-SNAPSHOT-jar-with-dependencies.jar \
         -T $out/share/java/JTangoServer-${version}-jar-with-dependencies.jar

      for JARNAME in JTangoCommons JTangoClientLang JTango TangORB JTangoServer; do
        ln -sf $out/share/java/$JARNAME-${version}.jar $out/share/java/$JARNAME.jar
      done

      ln -sf $out/share/java/JTangoServer-${version}-jar-with-dependencies.jar $out/share/java/JTangoServer-jar-with-dependencies.jar

      makeWrapper ${jre}/bin/java $out/bin/jtangoserver \
          --add-flags "-jar $out/share/java/JTangoServer-jar-with-dependencies.jar"

      runHook postInstall
    '';

    meta = {
      description = "Tango kernel Java implementation";
      homepage = "https://gitlab.com/tango-controls/JTango";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
