{
  fetchzip,
  jre,
  jython,
  lib,
  makeWrapper,
  maven,
  tango-controls,
}: let
  inherit (lib) licenses;
  inherit (tango-controls) JTango atk jive;

  pname = "hdbpp-viewer";
  version = "1.27.0";

  viewer-classpath = builtins.concatStringsSep ":" [
    "$out/share/java/_HDBViewer_deps/jcalendar.jar"
    "$out/share/java/_HDBViewer_deps/HDBExtract.jar"
    "$out/share/java/HDBViewer.jar"
    "${JTango}/share/java/JTango.jar"
    "${atk}/share/java/ATKCore.jar"
    "${atk}/share/java/ATKWidget.jar"
    "${jive}/share/java/Jive.jar"
    "${jython}/jython.jar"
  ];
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/hdbpp/${pname}/-/archive/${version}/${pname}-${version}.zip";

      hash = "sha256-JQoMaTvhK/9ASniQqtQzYN5JbZ+TDEX6bHyoA2cb0mE=";
    };

    mvnHash = "sha256-DhRMGNIYyCRjClG+zYeJmjj+6NY1POc1DkNHF51eiAc=";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre JTango atk jive jython];

    patches = [./maven_compiler_plugin_configuration.patch];

    # mvnFetchExtraArgs = {
    #   doCheck = false;
    # };
    # doCheck = false;
    # mvnParameters = "-DskipTests  -e";
    # mvnDepsParameters = "-DskipTests  -e";

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java
      mkdir -p $out/share/java/_HDBViewer_deps

      install ./.m2/org/tango-controls/hdb/HDBExtract/1.31.1/HDBExtract-1.31.1.jar \
         -T $out/share/java/_HDBViewer_deps/HDBExtract.jar

      install ./.m2/com/toedter/jcalendar/1.4/jcalendar-1.4.jar \
         -T $out/share/java/_HDBViewer_deps/jcalendar.jar

      install target/HDBViewer-1.27-SNAPSHOT.jar \
         -T $out/share/java/HDBViewer-1.27.jar

      ln -sf $out/share/java/HDBViewer-1.27.jar $out/share/java/HDBViewer.jar

      makeWrapper ${jre}/bin/java $out/bin/jhdbviewer \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
          --add-flags "--class-path \"${viewer-classpath}\"" \
          --add-flags "HDBViewer.MainPanel"

      runHook postInstall
    '';

    meta = {
      description = "Java HDB++ viewer";
      homepage = "https://gitlab.com/tango-controls/JTango";
      license = licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
