{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  libmysqlclient,
  mariadb,
  omniorb,
  stdenv,
  tango-controls,
  zeromq,
}: let
  inherit (tango-controls) cppTango;
  pname = "TangoAccessControl";
  version = "2.20";
in
  stdenv.mkDerivation (finalAttrs: {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/TangoAccessControl/-/archive/TangoAccessControl-Release-2.20/TangoAccessControl-TangoAccessControl-Release-2.20.zip";

      hash = "sha256-5LrF18o3CWveX4zoUhKB0tgIIVDXnhxL2UcYoL33r4g=";
    };

    nativeBuildInputs = [
      cmake
      cppzmq
      libmysqlclient
    ];
    buildInputs = [
      mariadb
      omniorb
      zeromq
      cppTango
    ];

    cmakeFlags = ["-S ../TangoAccessControl"];

    meta = {
      description = "Access control device server for Tango.";
      homepage = "https://gitlab.com/tango-controls/${pname}";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
