{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
  tango-controls,
}: let
  inherit (tango-controls) JTango atk jive;

  pname = "hdbpp-configurator";
  version = "3.27.0";

  # hdbpp-configurator only works right
  # with the specific jars that it downloads
  # (as opposed to the versions of these jars that we compile).
  # It doesn't download JTango,
  # so we source as many dependencies as possible
  # and cross our fingers for JTango.
  exfiltrated-deps = [
    ".m2/org/tango-controls/atk/ATKCore/9.3.31/ATKCore-9.3.31.jar"
    ".m2/org/tango-controls/atk/ATKWidget/9.3.31/ATKWidget-9.3.31.jar"
    ".m2/org/tango-controls/Jive/7.37/Jive-7.37.jar"
  ];
  deps-path = "$out/share/java/_hdb_configurator_deps";
  output-deps =
    map (dep-jar: deps-path + "/" + (builtins.baseNameOf dep-jar))
    exfiltrated-deps;

  cp-exfiltrated-deps =
    "cp "
    + (builtins.concatStringsSep " " exfiltrated-deps)
    + " \""
    + deps-path
    + ''"'';

  hdbpp-configurator-classpath-list =
    ["$out/share/java/HDBConfigurator.jar" "${JTango}/share/java/JTango.jar"]
    ++ output-deps;

  hdbpp-configurator-classpath =
    builtins.concatStringsSep ":" hdbpp-configurator-classpath-list;
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/hdbpp/${pname}/-/archive/${version}/${pname}-${version}.zip";
      hash = "sha256-HNOh+yEY8SSYXbXoHerwX9qYhY9x1GQPFRYOqF9Bljg=";
    };
    mvnHash = "sha256-S6UevSnMz3TBhTbUfy5hhmrzneMQlwkjoca5UsOK/1M=";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre JTango atk jive];

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java

      install ./target/HDBConfigurator-3.28-SNAPSHOT.jar \
       $out/share/java/HDBConfigurator-3.28.jar

      ln -s $out/share/java/HDBConfigurator-3.28.jar \
            $out/share/java/HDBConfigurator.jar \

      mkdir -p ${deps-path}
      ${cp-exfiltrated-deps}


      makeWrapper ${jre}/bin/java $out/bin/hdbpp-configurator \
          --add-flags "--class-path \"${hdbpp-configurator-classpath}\"" \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
          --add-flags org.tango.hdb_configurator.configurator.HdbConfigurator

      makeWrapper ${jre}/bin/java $out/bin/hdbpp-configurator-diagnostics \
          --add-flags "--class-path \"${hdbpp-configurator-classpath}\""  \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
          --add-flags org.tango.hdb_configurator.diagnostics.HdbDiagnostics

      makeWrapper ${jre}/bin/java $out/bin/hdbpp-configurator-server-info \
          --add-flags "--class-path \"${hdbpp-configurator-classpath}\"" \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
          --add-flags org.tango.hdb_configurator.diagnostics.ServerInfoTable

      runHook postInstall
    '';

    meta = {
      description = "Java GUI Configurator for TANGO HDB++";
      homepage = "https://gitlab.com/tango-controls/hdbpp/hdbpp-configurator";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
