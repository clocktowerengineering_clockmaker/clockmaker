{
  fetchurl,
  jre,
  lib,
  makeWrapper,
  stdenv,
}: let
  # Phoning this one in and just downloading hte public binaries directly.
  astor-jar-with-dependencies = (
    fetchurl {
      url = "https://repo1.maven.org/maven2/org/tango-controls/Astor/7.5.7/Astor-7.5.7-jar-with-dependencies.jar";
      hash = "sha256-zrR2f+UaS+YCYzDt7RIz0UwbSaurbVIDCpmHXcmV1UQ=";
    }
  );
  astor-jar = (
    fetchurl {
      url = "https://repo1.maven.org/maven2/org/tango-controls/Astor/7.5.7/Astor-7.5.7.jar";
      hash = "sha256-dLNgcQQSvNzaotSAk/64HqZvGv6h5PjMmdrxH/1hngE=";
    }
  );
in
  stdenv.mkDerivation (finalAttrs: {
    pname = "Astor";
    version = "7.5.7";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre];

    dontUnpack = true;

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java


      install ${astor-jar-with-dependencies} \
         -T $out/share/java/Astor-7.5.7-jar-with-dependencies.jar
      ln -sf $out/share/java/Astor-7.5.7-jar-with-dependencies.jar $out/share/java/Astor-jar-with-dependencies.jar

      install ${astor-jar} -T $out/share/java/Astor-7.5.7.jar
      ln -sf $out/share/java/Astor-7.5.7.jar $out/share/java/Astor.jar

      # Astor doesn't look right with gtk theme,
      # so we use Java's builtin theme.
      makeWrapper ${jre}/bin/java $out/bin/astor \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
          --add-flags "-jar $out/share/java/Astor-jar-with-dependencies.jar"

      runHook postInstall

    '';

    meta = {
      description = "Astor is a graphical Tango control system administration tool.";
      homepage = "https://gitlab.com/tango-controls/Astor";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
