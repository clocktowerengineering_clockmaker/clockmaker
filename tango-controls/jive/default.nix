{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
}: let
  pname = "jive";
  version = "7.41";
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/jive/-/archive/${version}/jive-${version}.zip";
      hash = "sha256-PX9oFftgDeDejzdk5UrUrhYs6/EcFsZt69aAryE4d2U=";
    };
    mvnHash = "sha256-QlOEyNx+qf6ikdP0YJZIefsts20hdtcQMsL1bc15Q2U=";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre];

    patches = [./maven_compiler_plugin_configuration.patch];

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java

      install ./target/Jive-${version}.jar \
         -T $out/share/java/Jive-${version}.jar

      install ./target/Jive-${version}-jar-with-dependencies.jar \
         -T $out/share/java/Jive-${version}-jar-with-dependencies.jar

      ln -sf $out/share/java/Jive-${version}.jar $out/share/java/Jive.jar

      # Mark doesn't like how jive looks with the system theme,
      # so we use Java's builtin theme.

      makeWrapper ${jre}/bin/java $out/bin/jive \
          --add-flags "-Dswing.defaultlaf=javax.swing.plaf.metal.MetalLookAndFeel" \
         --add-flags "-jar $out/share/java/Jive-${version}-jar-with-dependencies.jar"

      runHook postInstall
    '';

    meta = {
      description = "Jive is a standalone JAVA application designed to browse and edit the static TANGO database.";
      homepage = "https://gitlab.com/tango-controls/jive";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
