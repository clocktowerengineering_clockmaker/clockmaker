{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
}: let
  pname = "pogo";
  version = "9.9.2";
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/${pname}/-/archive/${version}/${pname}-${version}.zip";
      hash = "sha256-x0qeYgji3TTkfxQYnzCrMr7+UNiLn3bLDBuUcuK3+K8=";
    };

    mvnHash = "sha256-rs8OE6wHXeuKosM100c3VkX/siQTyss7W1ozrIYEVEo=";

    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre];

    # # JTango hasn't passed its test suite in a while...
    # doCheck = false;

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java

      # seems they forgot to update the version no.
      install ./org.tango.pogo.gui/target/Pogo-9.9.1-SNAPSHOT.jar \
         -T $out/share/java/Pogo-${version}.jar

      ln -sf $out/share/java/Pogo-${version}.jar $out/share/java/Pogo.jar

      makeWrapper ${jre}/bin/java $out/bin/pogo \
          --add-flags "-jar $out/share/java/Pogo-${version}.jar"

      runHook postInstall
    '';

    meta = {
      description = "A GUI aplication for generating Tango projects.";
      homepage = "https://gitlab.com/tango-controls/pogo";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
