{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
  tango-controls,
}: let
  inherit (tango-controls) JTango;

  pname = "atk";
  version = "9.4.8";
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/atk/-/archive/9.4.8/atk-9.4.8.zip";
      hash = "sha256-p03o1HiJ+eKsG3OK494O8Nn59sHWX606nvFB7u5yiDE=";
    };
    mvnHash = "sha256-xIRhogUT8Yu0rVQKjeUivQ93eoKY858t5jzLWUCA9vA=";
    nativeBuildInputs = [makeWrapper];
    # JTango necessary to execute JDrawEditorFrame
    buildInputs = [jre JTango];

    # atk repo has a useless configure that breaks compilation.
    dontConfigure = true;
    mvnFetchExtraArgs = {dontConfigure = true;};

    # adapted from Makefile in atk source dir
    installPhase = ''
       runHook preInstall

       mkdir -p $out/share/java

      install {core/target,$out/share/java}/ATKCore-${version}.jar
      install {widget/target,$out/share/java}/ATKWidget-${version}.jar
       ln -sf $out/share/java/ATKCore-${version}.jar $out/share/java/ATKCore.jar
       ln -sf $out/share/java/ATKWidget-${version}.jar $out/share/java/ATKWidget.jar
      install -d {core,widget}/target/classes/* $out/share/java

      cp -r {core,widget}/target/classes/fr $out/share/java

      runHook postInstall
    '';

    meta = {
      description = "atk atk atk";
      homepage = "https://gitlab.com/tango-controls/atk";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
