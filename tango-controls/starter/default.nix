{
  cmake,
  cppzmq, # libzmq
  fetchzip,
  lib,
  omniorb,
  stdenv,
  tango-controls,
}: let
  inherit
    (tango-controls)
    cppTango
    ;
in
  stdenv.mkDerivation (finalAttrs: {
    pname = "starter";
    version = "8.4";

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/starter/-/releases/Starter-8.4/downloads/starter-with-submodules-Starter-8.4.tar.gz";
      hash = "sha256-E1eaYzTEX/t8nrA21umWGWQWZn68OEc5IrpdjVYJTxQ=";
    };

    nativeBuildInputs = [cmake];
    buildInputs = [
      cppTango
      cppzmq
      omniorb
    ];

    meta = {
      description = "Tango Device server that controls Tango components";
      homepage = "https://gitlab.com/tango-controls/starter";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
