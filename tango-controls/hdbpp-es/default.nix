{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  omniorb,
  pkg-config,
  stdenv,
  tango-controls,
}: let
  inherit (tango-controls) cppTango libhdbpp;

  pname = "hdbpp-es";
  version = "2.2.0";
in
  stdenv.mkDerivation (finalAttrs: {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/hdbpp/${pname}/-/archive/${version}/${pname}-${version}.zip";
      hash = "sha256-iOPuMY7beuEz2xTu0/47mKtZ5u0fCfNzu5D70zFaXd8=";
    };

    nativeBuildInputs = [
      cmake
      pkg-config
    ];
    buildInputs = [
      cppTango
      cppzmq
      omniorb
      libhdbpp
    ];

    meta = {
      description = "Tango device server for the HDB++ Event Subscriber";
      homepage = "https://gitlab.com/tango-controls/hdbpp/${pname}";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
