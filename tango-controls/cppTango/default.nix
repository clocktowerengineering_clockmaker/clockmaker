{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  libjpeg, # libjpeg-turbo
  omniorb,
  stdenv,
  tango-controls,
  zeromq,
}: let
  inherit (tango-controls) tango-idl;
in
  stdenv.mkDerivation (finalAttrs: {
    pname = "cppTango";
    version = "9.5.0";

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/cppTango/-/archive/9.5.0/cppTango-9.5.0.zip";
      hash = "sha256-IBZgwsopPzqahBVXqlN7g38e7qD7IoJx3S0yHFwtm5k=";
    };

    nativeBuildInputs = [
      cmake
    ];
    buildInputs = [
      omniorb
      zeromq
      cppzmq
      libjpeg
      tango-idl
    ];

    patches = [./join_path_pkgconfig.patch];

    meta = {
      description = "Distributed Control System - C++ library";
      homepage = "https://gitlab.com/tango-controls/cppTango";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
