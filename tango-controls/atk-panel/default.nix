{
  fetchzip,
  jre,
  lib,
  makeWrapper,
  maven,
  tango-controls,
}: let
  inherit (tango-controls) atk;

  pname = "atk-panel";
  version = "6.0";
in
  maven.buildMavenPackage {
    inherit pname version;

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/${pname}/-/archive/${version}/atk-${version}.zip";
      hash = "sha256-KkPbi4+Sk0gF9/td3TjxGR3qRM/sOgpAu637qg+0OGI=";
    };
    mvnHash = "sha256-zwEaOxIuOcsOG+GoYr29UO9IK0b/inrgaZMgOrgi5xs=";
    nativeBuildInputs = [makeWrapper];
    buildInputs = [jre];

    # # atk repo has a useless configure that breaks compilation.
    # dontConfigure = true;
    # mvnFetchExtraArgs = { dontConfigure = true; };

    # TODO: enable tests
    # doCheck = true;
    # checkPhase = ''
    #   mvn compile jar:test-jar
    #   java --class-path "${atk}/share/java/ATKCore.jar:classes" -jar ./target/ATKPanel-${version}-tests.jar
    #   exit 1
    # '';

    installPhase = ''
      runHook preInstall

      mkdir -p $out/share/java

      install target/*.jar -t $out/share/java
      ln -s -T $out/share/java/ATKPanel-${version}.jar $out/share/java/ATKPanel.jar

      runHook postInstall
    '';

    meta = {
      description = "atk atk atk";
      homepage = "https://gitlab.com/tango-controls/atk";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  }
