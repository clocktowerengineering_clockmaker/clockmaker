{
  cmake,
  cppzmq,
  fetchzip,
  lib,
  libmysqlclient,
  mariadb,
  omniorb,
  stdenv,
  tango-controls,
  zeromq,
}: let
  inherit (tango-controls) cppTango;

  TangoCMakeModules-src = fetchzip {
    url = "https://gitlab.com/tango-controls/TangoCMakeModules/-/archive/4c798de9a0f1142fd3175d06f7507023a6884774/TangoCMakeModules-4c798de9a0f1142fd3175d06f7507023a6884774.zip";
    hash = "sha256-fEwUyO2tPFmGkkc1A0+auPPChzJ1IODeodHgqbPO+TM=";
  };
in
  stdenv.mkDerivation (finalAttrs: {
    pname = "TangoDatabase";
    version = "6.0.2";

    src = fetchzip {
      url = "https://gitlab.com/tango-controls/TangoDatabase/-/archive/18ca6e91de147e4c4ddc88f3a84f26773635eddc/TangoDatabase-18ca6e91de147e4c4ddc88f3a84f26773635eddc.zip";
      hash = "sha256-fn0Kc7svSAu2T1Kh3y3oMyGoLz9lKd5ZEovd6QP174E=";
    };

    # Manually add submodule
    prePatch = ''
      rmdir ./TangoCMakeModules
      ln -s ${TangoCMakeModules-src}/ ./TangoCMakeModules
    '';

    # For some strange reason Tango upstream has been extremely resistant to fix this.
    # https://gitlab.com/topodelapradera/TangoDatabase/-/commit/d0f59db056e06ee2d34292e00c9776bc7ba07d03
    # https://gitlab.com/tango-controls/TangoDatabase/-/merge_requests/82

    patches = [./d0f59db056e06ee2d34292e00c9776bc7ba07d03.patch];

    nativeBuildInputs = [
      cmake
      cppzmq
      libmysqlclient
    ];
    buildInputs = [
      cppTango
      mariadb
      omniorb
      zeromq
    ];

    meta = {
      description = "Tango Database Server";
      homepage = "https://gitlab.com/tango-controls/TangoDatabase";
      license = lib.licenses.lgpl3Only;
      maintainers = []; # TODO (daniel.indictor@clocktowerengineering.com): do they mean the creators or?
    };
  })
