let
  nixpkgs = ((import (import ../nix/sources.nix {}).nixpkgs) {}).pkgsCross.aarch64-multiplatform;

  manifest = [
    ./Astor
    ./JTango
    ./TangoAccessControl
    ./TangoDatabase
    ./atk
    ./cppTango
    ./hdbpp-cm
    ./hdbpp-configurator
    ./hdbpp-es
    ./hdbpp-viewer
    ./jive
    ./libhdbpp
    ./libhdbpp-mysql
    ./pogo
    ./starter
    ./tango-idl
  ];
  tango-controls = builtins.listToAttrs (
    map (
      pkgLoc: let
        pkg = nixpkgs.lib.callPackageWith (nixpkgs // {inherit tango-controls;}) (import pkgLoc) {};
      in {
        name = pkg.pname;
        value = pkg;
      }
    )
    manifest
  );
in
  tango-controls
